package com.knock.knocked.utilities;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;
import androidx.core.content.ContextCompat;

import com.knock.knocked.R;

public class RectAngle extends View {

    public Paint topLeftRect, bottomLeftRect, topRightRect, midRightRect, bottomRightRect,eyePaint2;
    public Canvas myCanvas;
    public int width = 400;
    public int mouthRadius = 240,mouthy=800,leftEyeX=350,rightEyeX=695,eyey=500;
    public int leftsideLeftEyeY = 300,rightsideRightEyeY=300,leftEyeBottomY=600,progress=0;
    public int leftEyeRightTopY = 370,curveRadius=-40,leftEyeRightTopY2=370;

    public RectAngle(Context context) {
        super(context);
        initPaints();
    }

    public RectAngle(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RectAngle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaints();

    }

    void initPaints() {
        topLeftRect = new Paint();
        bottomLeftRect = new Paint();
        topRightRect = new Paint();
        midRightRect = new Paint();
        bottomRightRect = new Paint();
        topLeftRect.setStyle(Paint.Style.FILL);
        topLeftRect.setStrokeWidth(10);
        topLeftRect.setColor(Color.parseColor("#202020"));
        bottomLeftRect.setStyle(Paint.Style.FILL);
        bottomLeftRect.setColor(Color.parseColor("#FFFF00"));
        topRightRect.setStyle(Paint.Style.STROKE);
        topRightRect.setColor(Color.parseColor("#202020"));
        topRightRect.setStrokeWidth(10);
        midRightRect.setStyle(Paint.Style.FILL);
        midRightRect.setColor(Color.parseColor("#808080"));
        bottomRightRect.setStyle(Paint.Style.FILL);
        bottomRightRect.setColor(Color.parseColor("#CCCC00"));


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        myCanvas = canvas;
        myFun(myCanvas, topLeftRect, 200, 200, width);


    }




    public void myFun(Canvas canvas, Paint paint, int x, int y, int width) {


        if(progress <90) {

            //left eye
            drawCurvedArrow(290, leftsideLeftEyeY, 490, leftEyeRightTopY, curveRadius, R.color.black, 4, 0);
            drawCurvedArrow(290, leftsideLeftEyeY, 340, leftEyeBottomY, -40, R.color.black, 4, 0);
            drawCurvedArrow(490, leftEyeRightTopY2, 340, leftEyeBottomY, 40, R.color.black, 4, 0);

            //right eye
            drawCurvedArrow(590, leftEyeRightTopY, 790, rightsideRightEyeY, curveRadius, R.color.black, 4, 0);
            drawCurvedArrow(590, leftEyeRightTopY, 740, leftEyeBottomY, -40, R.color.black, 4, 0);
            drawCurvedArrow(790, rightsideRightEyeY, 740, leftEyeBottomY, 40, R.color.black, 4, 0);
        }

        else
        {
            //left eye
            Paint eyePaint = new Paint();
            eyePaint.setStyle(Paint.Style.FILL);
            eyePaint.setColor(Color.parseColor("#fafafa"));
            eyePaint.setStrokeWidth(5);
            eyePaint.setColor(Color.parseColor("#fafafa"));

             eyePaint2 = new Paint();
            eyePaint2.setStyle(Paint.Style.STROKE);
            eyePaint2.setColor(Color.parseColor("#202020"));
            eyePaint2.setStrokeWidth(5);
            eyePaint2.setColor(Color.parseColor("#202020"));

            myCanvas.drawCircle(leftEyeX,eyey,90,eyePaint);
            myCanvas.drawCircle(leftEyeX,eyey,90,eyePaint2);

            //right eye
            myCanvas.drawCircle(rightEyeX,eyey,90,eyePaint);
            myCanvas.drawCircle(rightEyeX,eyey,90,eyePaint2);

        }

        //circle of left eye
        Paint paint1 = new Paint();
        paint1.setStrokeWidth(5);
        myCanvas.drawCircle(leftEyeX,eyey,12,paint1);

        //circle of right eye
        myCanvas.drawCircle(rightEyeX,eyey,12,paint1);


        //mouth
        drawCurvedArrow(330,800,760,mouthy,mouthRadius, R.color.black,4,0);

    }



    public void drawCurvedArrow(int x1, int y1, int x2, int y2, int curveRadius, int color, int lineWidth,float me) {

        Paint paint  = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(lineWidth);
        paint.setColor(ContextCompat.getColor(getContext(), color));

        Paint paint2  = new Paint();
        paint2.setAntiAlias(true);
        paint2.setStyle(Paint.Style.FILL);
        paint2.setStrokeWidth(lineWidth);
        paint2.setColor(Color.parseColor("#fafafa"));

        final Path path = new Path();
        int midX            = x1 + ((x2 - x1) / 2);
        int midY            = y1 + ((y2 - y1) / 2);
        float xDiff         = midX - x1;
        float yDiff         = midY - y1;
        double angle        = (Math.atan2(yDiff, xDiff) * (180 / Math.PI)) - 90;
        double angleRadians = Math.toRadians(angle);
        float pointX        = (float) (midX + curveRadius * Math.cos(angleRadians));
        float pointY        = (float) (midY + curveRadius * Math.sin(angleRadians));


        path.moveTo(x1, y1);
        path.cubicTo(x1,y1,pointX, pointY, x2, y2);
        myCanvas.rotate(me);
        myCanvas.drawPath(path, paint);

    }


}