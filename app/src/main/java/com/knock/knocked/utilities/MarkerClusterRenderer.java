package com.knock.knocked.utilities;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.knock.knocked.R;

public class MarkerClusterRenderer extends DefaultClusterRenderer<Person> {

    public MarkerClusterRenderer(Context context, GoogleMap map,
                                 ClusterManager<Person> clusterManager) {
        super(context, map, clusterManager);
    }

    @Override
    protected void onBeforeClusterItemRendered(Person item, MarkerOptions markerOptions) {
        // use this to make your change to the marker option
        // for the marker before it gets render on the map
        markerOptions.icon(BitmapDescriptorFactory.
                fromResource(R.mipmap.location));
    }
}