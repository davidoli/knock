package com.knock.knocked.utilities;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class MyView2 extends View {

    Paint mPaint;
    int i=0;
    int myStart,myEnd;
    private int startX = 250;
    private int startY = 250;

    private int endX = 250;
    private int endY = 250;


    public MyView2(Context context) {
        super(context);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);

        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.BLUE);
        mPaint.setStrokeWidth(5);

        this.myStart = 250;
        this.myEnd = 250;



    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        canvas.drawLine(startX, startY, endX, endY, mPaint);

        if (endX != 400 && endY != 400) { // set end points
            endY++;
            endX++;

            postInvalidateDelayed(15); // set time here
        }

    }


    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }


}