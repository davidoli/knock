package com.knock.knocked.listeners;

import android.view.View;
import com.knock.knocked.models.UserFilterModel;

public interface UserFilterRecyclerViewListener {
    void onClick(View view, int position, UserFilterModel model, String type);
}
