package com.knock.knocked.listeners;

import android.view.View;

import com.knock.knocked.models.ChatListModel;
import com.knock.knocked.models.WorkingHourModel;

public interface WorkingHoursRecyclerViewListener {
    void onClick(View view, int position, WorkingHourModel model, String type);
}
