package com.knock.knocked.listeners;

import android.view.View;
import android.widget.LinearLayout;
import com.knock.knocked.models.ReviewModel;

public interface ReviewListRecyclerListener {
    void onClick(View view, int position, ReviewModel model, String type);
}
