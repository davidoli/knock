package com.knock.knocked.listeners;

import android.view.View;

import com.knock.knocked.models.ExpertModel;

public interface ExpertRecyclerViewListener {
    void onClick(View view, int position, ExpertModel model, String type);

}
