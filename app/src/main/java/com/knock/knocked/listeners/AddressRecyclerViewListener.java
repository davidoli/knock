package com.knock.knocked.listeners;

import android.view.View;

import com.knock.knocked.models.AddressModel;

public interface AddressRecyclerViewListener {
    void onClick(View view, int position, AddressModel model, String type);
}
