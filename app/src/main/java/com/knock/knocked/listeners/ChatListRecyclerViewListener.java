package com.knock.knocked.listeners;

import android.view.View;

import com.knock.knocked.models.ChatListModel;
import com.knock.knocked.models.ExpertModel;

public interface ChatListRecyclerViewListener {
    void onClick(View view, int position, ChatListModel model, String type);
}
