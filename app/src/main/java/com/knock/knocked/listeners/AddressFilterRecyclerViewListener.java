package com.knock.knocked.listeners;

import android.view.View;
import android.widget.LinearLayout;

import com.knock.knocked.models.AddressFilterModel;
import com.knock.knocked.models.ProfessionModel;

public interface AddressFilterRecyclerViewListener {
    void onClick(View view, int position, AddressFilterModel model, String type);
}
