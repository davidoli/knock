package com.knock.knocked.listeners;

import android.view.View;

import com.knock.knocked.models.CalendarVisitModel;
import com.knock.knocked.models.WorkingHourModel;

public interface VisitRecyclerViewListener {
    void onClick(View view, int position, CalendarVisitModel model, String type);
}
