package com.knock.knocked.listeners;

import android.view.View;
import android.widget.LinearLayout;

import com.knock.knocked.models.ExpertModel;
import com.knock.knocked.models.ProfessionModel;

public interface ProfessionsRecyclerViewListener {
    void onClick(View view, int position, ProfessionModel model, String type, LinearLayout layout);
}
