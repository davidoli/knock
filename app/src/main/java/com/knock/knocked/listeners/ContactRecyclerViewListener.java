package com.knock.knocked.listeners;

import android.view.View;

import com.knock.knocked.models.ContactModel;


public interface ContactRecyclerViewListener {
    void onClick(View view, int position, ContactModel model, String type);
}
