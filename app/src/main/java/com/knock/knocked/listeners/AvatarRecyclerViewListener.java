package com.knock.knocked.listeners;

import android.view.View;

import com.knock.knocked.models.AvatarModel;
import com.knock.knocked.models.ChatListModel;

import java.io.IOException;

public interface AvatarRecyclerViewListener {
    void onClick(View view, int position, AvatarModel model, String type) throws IOException;
}
