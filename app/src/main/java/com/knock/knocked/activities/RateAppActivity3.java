package com.knock.knocked.activities;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.knock.knocked.R;
import com.knock.knocked.rest.RQualityRate;
import com.knock.knocked.rest.RRecommend;
import com.knock.knocked.rest.RetrofitHandler;
import com.knock.knocked.rest.models.QualityRateData;
import com.knock.knocked.rest.models.RecommendInData;
import com.knock.knocked.utilities.RectAngle;

import okhttp3.ResponseBody;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RateAppActivity3 extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout linearLayout;
    /**
     * seek bar to rate the provider
     */
    private SeekBar seekBar;
    /**
     * Text view showing the current rate
     * of the provider
     */
    private TextView tvRankStatus;
    private ImageView leftEye, rightEye, ivCancel,ivRecommend,ivNotRecommend,ivLike;
    private int qualityRate,id,myId;
    private String comment;
    private EditText etDes;
    private Button btnRate;
    private Dialog dialog;
    private RelativeLayout lLike,lDislike,rlLogin;
    private boolean isLiked;
    private LottieAnimationView lottieAnimationView;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate3);
        initView();
    }

    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     */

    private void initView() {
        id = getIntent().getIntExtra("id",0);
        myId = getIntent().getIntExtra("user_id",0);


        linearLayout = findViewById(R.id.layout);
        rlLogin = findViewById(R.id.rl_login);
        ivLike = findViewById(R.id.iv_like);
        seekBar = findViewById(R.id.seek_bar);
        leftEye = findViewById(R.id.left_eye);
        rightEye = findViewById(R.id.right_eye);
        tvRankStatus = findViewById(R.id.tv_3);
        ivCancel = findViewById(R.id.iv_cancel);

        rlLogin.setOnClickListener(this);
        ivCancel.setOnClickListener(this);


        final RectAngle rectAngleView = findViewById(R.id.rect);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                linearLayout.setBackgroundColor(Color.rgb(252 - (progress), 190 + (progress / 2), 233 - (progress / 10)));

                statusTextChange(progress);
                emotionVoiewChange(progress, rectAngleView);
                rectAngleView.invalidate();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.iv_cancel:
                super.onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                RateAppActivity3.this.finish();
                break;

            case R.id.rl_login:
                qualityRate();
                break;

        }

    }


    /**
     * Description: This is the method
     * which set the text of
     * rating status
     */

    private void statusTextChange(int progress) {
        if (progress < 40)
            tvRankStatus.setText("Hideous");
        else if (progress >= 40 && progress < 80)
            tvRankStatus.setText("Ok ");
        else
            tvRankStatus.setText("Good");
    }



    /**
     * Description: This is the method
     * which set the changes of
     * emotion view rating
     */


    private void emotionVoiewChange(int progress, RectAngle rectAngleView) {
        if (progress == 0)
            qualityRate = 0;
        else if (progress < 40)
            qualityRate = 1;
        else if (progress < 90)
            qualityRate = 2;
        else
            qualityRate = 3;
        if (progress == 0) {
            rectAngleView.mouthRadius = 240;
            rectAngleView.mouthy = 800;
            rectAngleView.eyey = 500;
            rectAngleView.rightEyeX = 695;
            rectAngleView.leftEyeX = 350;
            rectAngleView.leftsideLeftEyeY = 300;
            rectAngleView.rightsideRightEyeY = 300;
            rectAngleView.leftEyeBottomY = 600;
            rectAngleView.leftEyeRightTopY = 370;
            rectAngleView.leftEyeRightTopY2 = 370;
            rectAngleView.curveRadius = -40;
            rectAngleView.progress = progress;



        } else if (progress >= 50) {
            rectAngleView.mouthRadius = 240 - (progress * 6);
            rectAngleView.mouthy = 800 + (progress / 2) + 30;
            rectAngleView.eyey = 500 - (progress / 2) - 10;
            rectAngleView.progress = progress;
            rectAngleView.curveRadius = -40 + progress;
            rectAngleView.leftEyeRightTopY = (370 + progress) - ((progress - 50) * 2);
            rectAngleView.leftEyeRightTopY2 = (370 + progress) - ((progress - 50) * 2);

        } else {
            rectAngleView.mouthRadius = 240 - (progress * 6);
            rectAngleView.mouthy = 800 + (progress / 2) + 30;
            rectAngleView.leftEyeX = 350 + (progress / 2);
            rectAngleView.rightEyeX = 695 + (progress / 3);
            rectAngleView.eyey = 500 - (progress / 2) - 10;
            rectAngleView.leftsideLeftEyeY = 300 + (progress * 2) + 30;
            rectAngleView.rightsideRightEyeY = 300 + (progress * 2) + 30;
            rectAngleView.leftEyeBottomY = 600 - (progress * 2);
            rectAngleView.progress = progress;
            rectAngleView.curveRadius = -40 + progress;
            rectAngleView.leftEyeRightTopY = 370 + progress;
            rectAngleView.leftEyeRightTopY2 = 370 + progress;

        }
    }


    private void qualityRate()
    {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.done_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        QualityRateData data = new QualityRateData();
        data.setOwner(myId);
        data.setProvider(id);
        data.setQuality(qualityRate);
        RQualityRate.Fire(data, new RetrofitHandler.apiResponseHandler<ResponseBody>() {
            @Override
            public void onSuccess(ResponseBody responseBody) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(RateAppActivity3.this, RateAppActivity2.class);
                        intent.putExtra("id",id);
                        intent.putExtra("user_id",myId);
                        startActivity(intent);
                        RateAppActivity3.this.finish();
                        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                    }
                },1800);
            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }


}