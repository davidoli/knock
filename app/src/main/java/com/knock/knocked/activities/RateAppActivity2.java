package com.knock.knocked.activities;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.knock.knocked.R;
import com.knock.knocked.rest.RQualityRate;
import com.knock.knocked.rest.RRecommend;
import com.knock.knocked.rest.RReview;
import com.knock.knocked.rest.RetrofitHandler;
import com.knock.knocked.rest.models.QualityRateData;
import com.knock.knocked.rest.models.RecommendInData;
import com.knock.knocked.rest.models.ReviewInData;
import com.knock.knocked.utilities.RectAngle;

import okhttp3.ResponseBody;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RateAppActivity2 extends BaseActivity implements View.OnClickListener {
    private RelativeLayout linearLayout;
    /**
     * seek bar to rate the provider
     */
    private SeekBar seekBar;
    /**
     * Text view showing the current rate
     * of the provider
     */
    private TextView tvRankStatus;
    private ImageView leftEye, rightEye, ivCancel, ivRecommend, ivNotRecommend, ivLike;
    private int qualityRate, id, myId;
    private String comment;
    private EditText etDes;
    private Button btnRate;
    private Dialog dialog;
    private RelativeLayout lLike, lDislike, rlLogin;
    private boolean isLiked;
    private LottieAnimationView lottieAnimationView;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate2);
        initView();
    }

    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     */

    private void initView() {
        id = getIntent().getIntExtra("id", 0);
        myId = getIntent().getIntExtra("user_id", 0);


        linearLayout = findViewById(R.id.layout);
        rlLogin = findViewById(R.id.rl_login);
        ivLike = findViewById(R.id.iv_like);
        seekBar = findViewById(R.id.seek_bar);
        leftEye = findViewById(R.id.left_eye);
        rightEye = findViewById(R.id.right_eye);
        tvRankStatus = findViewById(R.id.tv_3);
        ivCancel = findViewById(R.id.iv_cancel);
        etDes = findViewById(R.id.et_des);
        lottieAnimationView = findViewById(R.id.lottie_like);

        lottieAnimationView.setOnClickListener(this);
        ivLike.setOnClickListener(this);
        rlLogin.setOnClickListener(this);
        ivCancel.setOnClickListener(this);


    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_cancel:
                super.onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                RateAppActivity2.this.finish();
                break;
            case R.id.lottie_like:
                isLiked = true;
                lottieAnimationView.playAnimation();
                break;
            case R.id.iv_like:
                lottieAnimationView.playAnimation();
                break;
            case R.id.rl_login:
                Recommend();
                break;


        }

    }


    private void Recommend() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.done_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        RecommendInData data = new RecommendInData();
        data.setLike(isLiked);
        data.setOwner(myId);
        data.setProvider(id);
        RRecommend.Fire(data, new RetrofitHandler.apiResponseHandler<ResponseBody>() {
            @Override
            public void onSuccess(ResponseBody responseBody) {
                if (etDes.getText().toString().trim().length() == 0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            RateAppActivity2.super.onBackPressed();
                            RateAppActivity2.this.finish();
                        }
                    }, 1800);
                } else
                    comment();


            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });

    }


    private void comment() {
        ReviewInData data = new ReviewInData();
        data.setOwner(myId);
        data.setProvider(id);
        data.setReview(etDes.getText().toString().trim());
        RReview.Fire(data, new RetrofitHandler.apiResponseHandler<ResponseBody>() {
            @Override
            public void onSuccess(ResponseBody responseBody) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        RateAppActivity2.super.onBackPressed();
                        RateAppActivity2.this.finish();
                    }
                }, 2100);


            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });

    }

}