package com.knock.knocked.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.knock.knocked.R;
import com.knock.knocked.models.MyInfoData;
import com.knock.knocked.models.ProviderInfoInData;
import com.knock.knocked.rest.RCompleteInfo;
import com.knock.knocked.rest.RMyInfo;
import com.knock.knocked.rest.RetrofitHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CompletingInfoActivity extends BaseActivity implements View.OnClickListener {
    private RelativeLayout rlConfirm, rlLayout;
    private ImageView btnMap1, btnMap2, btnMap3, ivBack;
    private TextInputLayout tlInsta, tlWeb;
    private LinearLayout tlLocation1, tlLocaion2, tlLocation3;
    private TextInputEditText etInsta, etWeb;
    private TextView etLocation1, etLocation2, etLocation3;
    private Switch aSwitch;
    private int inviteNums, catId, userId;
    private String des;
    private Dialog dialog;
    private TextView tvConfirm;
    private double lat1, lon1, lat2, lon2, lat3, lon3;
    private String web, insta;
    private boolean isFreelancer;
    private SharedPreferences settings;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completing_info);
        initView();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     * then get the list of professions and
     * experts
     */

    private void initView() {


        rlConfirm = findViewById(R.id.rl_confirm);
        btnMap1 = findViewById(R.id.btn_map1);
        btnMap2 = findViewById(R.id.btn_map2);
        btnMap3 = findViewById(R.id.btn_map3);
        tlInsta = findViewById(R.id.tl_insta);
        tlWeb = findViewById(R.id.tl_web);
        tlLocation1 = findViewById(R.id.tl_location1);
        tlLocaion2 = findViewById(R.id.tl_location2);
        tlLocation3 = findViewById(R.id.tl_location3);
        etWeb = findViewById(R.id.et_web);
        etInsta = findViewById(R.id.et_insta);
        etLocation1 = findViewById(R.id.et_location1);
        etLocation2 = findViewById(R.id.et_location2);
        etLocation3 = findViewById(R.id.et_location3);
        aSwitch = findViewById(R.id.switchbtn);
        tvConfirm = findViewById(R.id.tv_confirm);
        ivBack = findViewById(R.id.iv_back);
        rlLayout = findViewById(R.id.rl_layout);

        btnMap1.setOnClickListener(this);
        btnMap2.setOnClickListener(this);
        btnMap3.setOnClickListener(this);
        rlConfirm.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        rlLayout.setOnClickListener(this);


        settings = getSharedPreferences("me",
                Context.MODE_PRIVATE);

        catId = settings.getInt("cat_id", 0);
        des = settings.getString("des", "");

        inviteNums = settings.getInt("invite", 0);
        web = settings.getString("web", "");
        insta = settings.getString("insta", "");
        des = settings.getString("des", "");
        lat1 = settings.getFloat("lat1", -1);
        lat2 = settings.getFloat("lat2", -1);
        lat3 = settings.getFloat("lat3", -1);
        lon1 = settings.getFloat("lon1", -1);
        lon2 = settings.getFloat("lon2", -1);
        lon3 = settings.getFloat("lon3", -1);
        userId = settings.getInt("user_id", -1);
        isFreelancer = settings.getBoolean("is_freelancer", false);

//        if(web != null)
        etWeb.setText(web);
//        if(insta != null)
        etInsta.setText(insta);
        if (lat1 > 0) {
            Geocoder geocoder = new Geocoder(CompletingInfoActivity.this, Locale.getDefault());
            List<Address> addresses = new ArrayList<>();
            try {
                addresses = geocoder.getFromLocation(lat1, lon1, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }


            Address address = addresses.get(0);
            etLocation1.setText(address.getCountryName() + address.getSubAdminArea() + address.getSubLocality());
        }

        if (lat2 > 0) {
            Geocoder geocoder = new Geocoder(CompletingInfoActivity.this, Locale.getDefault());
            List<Address> addresses = new ArrayList<>();
            try {
                addresses = geocoder.getFromLocation(lat2, lon2, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }


            Address address = addresses.get(0);
            etLocation2.setText(address.getCountryName() + address.getSubAdminArea() + address.getSubLocality());
        }

        if (lat3 > 0) {
            Geocoder geocoder = new Geocoder(CompletingInfoActivity.this, Locale.getDefault());
            List<Address> addresses = new ArrayList<>();
            try {
                addresses = geocoder.getFromLocation(lat3, lon3, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }


            Address address = addresses.get(0);
            etLocation3.setText(address.getCountryName() + address.getSubAdminArea() + address.getSubLocality());
        }


        getUerInfo();


        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    tlLocaion2.setVisibility(View.VISIBLE);
                    tlLocation3.setVisibility(View.VISIBLE);
                    btnMap2.setVisibility(View.VISIBLE);
                    btnMap3.setVisibility(View.VISIBLE);
                } else {
                    tlLocaion2.setVisibility(View.GONE);
                    tlLocation3.setVisibility(View.GONE);
                    btnMap2.setVisibility(View.GONE);
                    btnMap3.setVisibility(View.GONE);
                }
            }
        });
    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_login:
                break;
            case R.id.btn_map1:
                startMapIntent(1);
                break;
            case R.id.btn_map2:
                startMapIntent(2);
                break;
            case R.id.btn_map3:
                startMapIntent(3);
                break;
            case R.id.rl_confirm:
                confirmation();
                break;
            case R.id.iv_back:
                overridePendingTransitionExit();
                break;
            case R.id.rl_layout:
                hideKeyboard(CompletingInfoActivity.this);
                break;
        }
    }


    private void startMapIntent(int number) {
        Intent intent = new Intent(CompletingInfoActivity.this, MapActivity2.class);
        intent.putExtra("num", number);
        settings = getSharedPreferences("me",
                Context.MODE_PRIVATE);
        settings.edit().putInt("cat_id", catId).apply();
        settings.edit().putInt("invite", inviteNums).apply();
        settings.edit().putString("des", des).apply();
        settings.edit().putString("web", etWeb.getText().toString().trim()).apply();
        settings.edit().putString("insta", etInsta.getText().toString().trim()).apply();
        settings.edit().putFloat("locationLat1", (float) lat1).apply();
        settings.edit().putFloat("locationLat2", (float) lat2).apply();
        settings.edit().putFloat("locationLat1", (float) lat3).apply();
        settings.edit().putFloat("locationLon1", (float) lon1).apply();
        settings.edit().putFloat("locationLon2", (float) lon2).apply();
        settings.edit().putFloat("locationLon3", (float) lon3).apply();
        settings.edit().putBoolean("is_freelancer", isFreelancer).apply();
        startActivityForResult(intent, 123);

    }


    private void getUerInfo() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.waiting_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        RMyInfo.Fire(new RetrofitHandler.apiResponseHandler<MyInfoData>() {
            @Override
            public void onSuccess(MyInfoData myInfoData) {
                inviteNums = myInfoData.getInvited();
                userId = myInfoData.getId();
                dialog.dismiss();
                if (inviteNums >= 5)
                    tvConfirm.setText("Complete");
                else
                    tvConfirm.setText("Continue");
            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }


    private void confirmation() {
        if (checkInputs()) {
            if (tvConfirm.getText().toString().contains("omp"))
                confirmationProcess();
            else {
                Intent intent = new Intent(CompletingInfoActivity.this, ContacsListActivity.class);
                settings = getSharedPreferences("me",
                        Context.MODE_PRIVATE);
                settings.edit().putInt("cat_id", catId).apply();
                settings.edit().putInt("invite", inviteNums).apply();
                settings.edit().putString("des", des).apply();
                settings.edit().putString("web", etWeb.getText().toString().trim()).apply();
                settings.edit().putString("insta", etInsta.getText().toString().trim()).apply();
                settings.edit().putFloat("locationLat1", (float) lat1).apply();
                settings.edit().putFloat("locationLat2", (float) lat2).apply();
                settings.edit().putFloat("locationLat1", (float) lat3).apply();
                settings.edit().putFloat("locationLon1", (float) lon1).apply();
                settings.edit().putFloat("locationLon2", (float) lon2).apply();
                settings.edit().putFloat("locationLon3", (float) lon3).apply();
                settings.edit().putBoolean("is_freelancer", isFreelancer).apply();
                startActivityForResult(intent, 2);
            }
        }
    }


    private void confirmationProcess() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.done_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        ProviderInfoInData data = new ProviderInfoInData();
        data.setDescription(des);
        data.setInstagram(etInsta.getText().toString().trim());
        data.setWeb(etWeb.getText().toString().trim());
        data.setIs_virtual(aSwitch.isChecked());
        data.setUser(userId);
        data.setService(catId);
        data.setJob_done_time(settings.getFloat("duration",(float) 0.2));
        data.setService_kind("a");
        RCompleteInfo.Fire(data, new RetrofitHandler.apiResponseHandler<ResponseBody>() {
            @Override
            public void onSuccess(ResponseBody responseBody) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        settings = getSharedPreferences("me",
                                Context.MODE_PRIVATE);

                        settings.edit().putInt("cat_id",-1).apply();
                        settings.edit().putInt("invite",-1).apply();
                        settings.edit().putString("des","").apply();
                        settings.edit().putString("web","").apply();
                        settings.edit().putString("insta","").apply();
                        settings.edit().putFloat("locationLat1", -1).apply();
                        settings.edit().putFloat("locationLat2", -1).apply();
                        settings.edit().putFloat("locationLat1", -1).apply();
                        settings.edit().putFloat("locationLon1", -1).apply();
                        settings.edit().putFloat("locationLon2", -1).apply();
                        settings.edit().putFloat("locationLon3", -1).apply();
                        settings.edit().putBoolean("is_freelancer", false).apply();
                        settings.edit().putFloat("duration", -1).apply();

                        Toast.makeText(CompletingInfoActivity.this, "Registered As a Provider Successfully", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(CompletingInfoActivity.this, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        CompletingInfoActivity.this.finish();
                    }
                }, 1800);

            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (resultCode == RESULT_OK || true) {

            if (requestCode == 2) {


            } else {
                String address = data.getStringExtra("address");
                int numberi = data.getIntExtra("number", 0);
                double lat = data.getIntExtra("lat", 0);
                double lon = data.getIntExtra("lon", 0);
                fillAddress(address, numberi, lat, lon);

            }
        }
    }

    private void fillAddress(String address, int number, double lat, double lon) {
        switch (number) {
            case 1:
                lat1 = lat;
                lon1 = lon;
                if (address == null)
                    etLocation1.setText("Added");
                else
                    etLocation1.setText(address);
                break;
            case 2:
                lat2 = lat;
                lon2 = lon;
                if (address == null)
                    etLocation2.setText("Added");
                else
                    etLocation2.setText(address);
                break;
            case 3:
                lat3 = lat;
                lon3 = lon;
                if (address == null)
                    etLocation3.setText("Added");
                else
                    etLocation3.setText(address);
                break;
        }
    }


    private boolean checkInputs() {
        return true;
    }


    protected void overridePendingTransitionExit() {
        settings = getSharedPreferences("me",
                Context.MODE_PRIVATE);
        settings.edit().putInt("cat_id", catId).apply();
        settings.edit().putInt("invite", inviteNums).apply();
        settings.edit().putString("des", des).apply();
        settings.edit().putString("web", etWeb.getText().toString().trim()).apply();
        settings.edit().putString("insta", etInsta.getText().toString().trim()).apply();
        settings.edit().putFloat("locationLat1", (float) lat1).apply();
        settings.edit().putFloat("locationLat2", (float) lat2).apply();
        settings.edit().putFloat("locationLat1", (float) lat3).apply();
        settings.edit().putFloat("locationLon1", (float) lon1).apply();
        settings.edit().putFloat("locationLon2", (float) lon2).apply();
        settings.edit().putFloat("locationLon3", (float) lon3).apply();
        settings.edit().putBoolean("is_freelancer", isFreelancer).apply();
        Intent intent = new Intent();
        intent.putExtra("cat_id", catId);
        intent.putExtra("des", des);
        setResult(1, intent);
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        overridePendingTransitionExit();

    }

}
