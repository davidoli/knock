package com.knock.knocked.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.knock.knocked.R;
import com.knock.knocked.models.LoginData;
import com.knock.knocked.models.LoginOutData;
import com.knock.knocked.models.ResendData;
import com.knock.knocked.rest.RLogin;
import com.knock.knocked.rest.RResendCode;
import com.knock.knocked.rest.RetrofitHandler;
import com.knock.knocked.rest.RetrofitHandler2;

import okhttp3.ResponseBody;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ConfirmationCodeActivity extends BaseActivity implements View.OnClickListener {
    private RelativeLayout rlLogin,rlLayout;
    private String phone;
    private TextInputLayout tlCode;
    private TextInputEditText etCode;
    private boolean isAlready = false,login = false;
    private Dialog dialog;
    private TextView tvResend,tvKnocked,tvDes;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
        initView();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     * then get the list of professions and
     * experts
     */

    private void initView() {
        phone = getIntent().getStringExtra("phone");
        login = getIntent().getBooleanExtra("login",false);
        isAlready = getIntent().getBooleanExtra("already", false);

        if (isAlready)
            sendCode();

        rlLogin = findViewById(R.id.rl_login);
        tlCode = findViewById(R.id.tl_code);
        etCode = findViewById(R.id.et_code);
        tvResend = findViewById(R.id.tv_resend);
        rlLayout = findViewById(R.id.rl_layout);
        tvKnocked = findViewById(R.id.tv_knocked);
        tvDes = findViewById(R.id.tv_des);

        Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/grobold.ttf");
        tvKnocked.setTypeface(tf);

        rlLogin.setOnClickListener(this);
        tvResend.setOnClickListener(this);
        rlLayout.setOnClickListener(this);
    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_login:
                if (isValidCode())
                    validateCode();
                break;
            case R.id.tv_resend:
                if (tvResend.getText().toString().contains("Send"))
                    sendCode();
                break;
            case R.id.rl_layout:
                hideKeyboard(ConfirmationCodeActivity.this);
                break;

        }
    }


    private boolean isValidCode() {
        if (etCode.getText().length() < 5) {
            tlCode.setError("Code is incorrect");
            return false;
        }
        return true;
    }


    private void validateCode() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.waiting_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        LoginData loginData = new LoginData();
        loginData.setCellphone(phone);
        loginData.setVerification_code(etCode.getText().toString().trim());
        RLogin.Fire(loginData, new RetrofitHandler2.apiResponseHandler<LoginOutData>() {
            @Override
            public void onSuccess(LoginOutData responseBody) {
                successProcess(responseBody);
            }

            @Override
            public void onFail(int code, String message) {
                Toast.makeText(ConfirmationCodeActivity.this, "Code is not Correct", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }


    private void successProcess(LoginOutData data) {
        dialog.dismiss();
        SharedPreferences settings = getSharedPreferences("me",
                Context.MODE_PRIVATE);
        settings.edit().putString("token", data.getToken()).apply();
        if(!login) {
            Intent intent = new Intent(ConfirmationCodeActivity.this, CompleteUserInfoActivity.class);
            startActivity(intent);
            ConfirmationCodeActivity.this.finish();
        }
        else
        {
            Intent intent = new Intent(ConfirmationCodeActivity.this, MainActivity.class);
            startActivity(intent);
            ConfirmationCodeActivity.this.finish();
        }
    }


    private void sendCode() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.waiting_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        ResendData resendData = new ResendData();
        resendData.setCellphone(phone);
        RResendCode.Fire(resendData, new RetrofitHandler2.apiResponseHandler<ResponseBody>() {
            @Override
            public void onSuccess(ResponseBody responseBody) {
                timerThis();
                dialog.dismiss();
            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }


    private void timerThis() {
        new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {
                tvResend.setText("Seconds Remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                tvResend.setText("Send Again");
            }
        }.start();
    }




}
