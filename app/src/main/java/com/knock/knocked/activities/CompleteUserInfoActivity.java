package com.knock.knocked.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.knock.knocked.ApiClient;
import com.knock.knocked.ApiInterface;
import com.knock.knocked.R;
import com.knock.knocked.adapters.AvatarAdapter;
import com.knock.knocked.adapters.WorkingHourAdapter;
import com.knock.knocked.listeners.AvatarRecyclerViewListener;
import com.knock.knocked.listeners.WorkingHoursRecyclerViewListener;
import com.knock.knocked.models.AvatarModel;
import com.knock.knocked.models.AvatarOut;
import com.knock.knocked.models.LoginData;
import com.knock.knocked.models.LoginOutData;
import com.knock.knocked.models.ResendData;
import com.knock.knocked.models.WorkingHourModel;
import com.knock.knocked.rest.RLogin;
import com.knock.knocked.rest.RResendCode;
import com.knock.knocked.rest.RUserName;
import com.knock.knocked.rest.RetrofitHandler;
import com.knock.knocked.rest.RetrofitHandler2;
import com.knock.knocked.rest.models.AvatarName;
import com.knock.knocked.utilities.SpacesItemDecoration;

import org.apache.commons.io.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class CompleteUserInfoActivity extends BaseActivity implements View.OnClickListener {
    private RelativeLayout rlLogin, rlLayout;
    private String phone;
    private TextInputLayout tlCode;
    private TextInputEditText etCode;
    private boolean isAlready = false;
    private Dialog dialog;
    private TextView tvResend, tvKnocked, tvDes;
    private ImageView ivLogo;
    private int GALLERY = 1, CAMERA = 2;
    private Bitmap bitmap;
    private Uri filePath;
    private boolean isUploaded;
    private TextInputLayout tlName;
    private TextInputEditText etName;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private AvatarRecyclerViewListener listener;
    private ArrayList<AvatarModel> avatarList = new ArrayList<>();
    private File myFile;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_user_info);
        initView();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     * then get the list of professions and
     * experts
     */

    private void initView() {
        phone = getIntent().getStringExtra("phone");
        isAlready = getIntent().getBooleanExtra("already", false);


        rlLogin = findViewById(R.id.rl_login);
        tlCode = findViewById(R.id.tl_code);
        ivLogo = findViewById(R.id.iv_logo);
        etCode = findViewById(R.id.et_code);
        tvResend = findViewById(R.id.tv_resend);
        rlLayout = findViewById(R.id.rl_layout);
        tvKnocked = findViewById(R.id.tv_knocked);
        tvDes = findViewById(R.id.tv_des);
        tlName = findViewById(R.id.tl_name);
        etName = findViewById(R.id.et_name);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/grobold.ttf");
        tvKnocked.setTypeface(tf);

        rlLogin.setOnClickListener(this);
        ivLogo.setOnClickListener(this);
        rlLayout.setOnClickListener(this);


    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_login:
                Intent intent = new Intent(CompleteUserInfoActivity.this, MainActivity.class);
                startActivity(intent);
                CompleteUserInfoActivity.this.finish();
                break;
            case R.id.rl_layout:
                hideKeyboard(CompleteUserInfoActivity.this);
                break;
            case R.id.iv_logo:
                if (etName.getText().toString().length() < 1) {
                    Snackbar snackbar = Snackbar
                            .make(rlLayout, "Fill The Name Field First", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                } else
                    showPictureDialog();
                break;

        }
    }






    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Choose...");
        String[] pictureDialogItems = {
                "Pick Image From Gallery",
                "Camera", "Pick From Avatar"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (getReadStoragePermission())
                                    choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                            case 2:
                                if (havePermissions())
                                    avatarSelection();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }


    private boolean getReadStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
            return false;
        } else
            return true;

    }

    public void choosePhotoFromGallary() {
        Intent intent = new Intent();
        intent.setType("*/*");  //  */*          image/*
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }

    private void takePhotoFromCamera() {


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA);
        } else {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), filePath);
                upload(filePath);


            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == this.RESULT_CANCELED) {
            return;
        } else if (requestCode == GALLERY) {
            if (data != null) {
                filePath = data.getData();
                if (filePath != null) {
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), filePath);
                        upload(filePath);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");

            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
            Uri tempUri = getImageUri(this, photo);

            // CALL THIS METHOD TO GET THE ACTUAL PATH
            File finalFile = new File(getRealPathFromUri(tempUri));

        } else if (requestCode == 7) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 8);
        } else if (requestCode == 8) {
                avatarSelection();
        }

    }


    private void upload(Uri avatar) {
        String filePath = getRealPathFromUri(avatar);
        if (filePath != null && !filePath.isEmpty()) {
            File file = new File(filePath);
            if (file.exists()) {
                ApiInterface service = ApiClient.getClient(this).create(ApiInterface.class);
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);
                dialog = new Dialog(this);
                dialog.setContentView(R.layout.done_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                Call<AvatarOut> call = service.postFile(body);
                call.enqueue(new Callback<AvatarOut>() {
                    @Override
                    public void onResponse(Call<AvatarOut> call,
                                           Response<AvatarOut> response) {
                        isUploaded = true;
                        Glide.with(CompleteUserInfoActivity.this)
                                .load(response.body().getAvatar())  //http://karmento.ir      http://karmento.ir
                                .dontAnimate()
                                .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                                .fitCenter()//this method help to fit image into center of your ImageView
                                .into(ivLogo);

                        updateName();


                    }

                    @Override
                    public void onFailure(Call<AvatarOut> call, Throwable t) {
                        dialog.dismiss();
                    }
                });
            }
        }
    }


    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    public String getRealPathFromUri(final Uri uri) {
        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(CompleteUserInfoActivity.this, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(CompleteUserInfoActivity.this, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(CompleteUserInfoActivity.this, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(CompleteUserInfoActivity.this, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return uri.toString();
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        filePath = Uri.parse(path);
        upload(filePath);
        return Uri.parse(path);
    }


    private void avatarSelection() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.avatars_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.setCancelable(true);
        Window window = dialog.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        recyclerViewStuff();

    }




    private void recyclerViewStuff() {

        recyclerView = dialog.findViewById(R.id.recycler_view);


        listener = new AvatarRecyclerViewListener() {
            @Override
            public void onClick(View view, final int position, final AvatarModel model, String type) throws IOException {
                switch (position) {
                    case 0:
                        upudateAvatar(1);
                        break;
                    case 1:
                        upudateAvatar(2);
                        break;
                    case 2:
                        upudateAvatar(3);
                        break;
                    case 3:
                        upudateAvatar(4);
                        break;
                    case 4:
                        upudateAvatar(5);
                        break;
                    case 5:
                        upudateAvatar(6);
                        break;
                    case 6:
                        upudateAvatar(7);
                        break;
                    case 7:
                        upudateAvatar(8);
                        break;
                    case 8:
                        upudateAvatar(9);
                        break;
                    case 9:
                        upudateAvatar(10);
                        break;
                    case 10:
                        upudateAvatar(11);
                        break;
                    case 11:
                        upudateAvatar(12);
                        break;
                }
            }

        };
        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);

        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new AvatarAdapter(avatarList, CompleteUserInfoActivity.this, listener, 1);


        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();


        avatarList.clear();

        AvatarModel model = new AvatarModel();
        model.setId(1);
        model.setAddress(R.drawable.avatar1);
        avatarList.add(model);

        AvatarModel model2 = new AvatarModel();
        model.setId(2);
        model.setAddress(R.drawable.avatar2);
        avatarList.add(model2);

        AvatarModel model3 = new AvatarModel();
        model.setId(3);
        model.setAddress(R.drawable.avatar3);
        avatarList.add(model3);

        AvatarModel model4 = new AvatarModel();
        model.setId(4);
        model.setAddress(R.drawable.avatar4);
        avatarList.add(model4);

        AvatarModel model5 = new AvatarModel();
        model.setId(5);
        model.setAddress(R.drawable.avatar5);
        avatarList.add(model5);

        AvatarModel model6 = new AvatarModel();
        model.setId(6);
        model.setAddress(R.drawable.avatar6);
        avatarList.add(model6);

        AvatarModel model7 = new AvatarModel();
        model.setId(7);
        model.setAddress(R.drawable.avatar7);
        avatarList.add(model7);

        AvatarModel model8 = new AvatarModel();
        model.setId(8);
        model.setAddress(R.drawable.avatar8);
        avatarList.add(model8);

        AvatarModel model9 = new AvatarModel();
        model.setId(9);
        model.setAddress(R.drawable.avatar9);
        avatarList.add(model9);

        AvatarModel model10 = new AvatarModel();
        model.setId(10);
        model.setAddress(R.drawable.avatar10);
        avatarList.add(model10);

        AvatarModel model11 = new AvatarModel();
        model.setId(11);
        model.setAddress(R.drawable.avatar11);
        avatarList.add(model11);

        AvatarModel model12 = new AvatarModel();
        model.setId(12);
        model.setAddress(R.drawable.avatar12);
        avatarList.add(model12);

        mAdapter.notifyDataSetChanged();


    }


    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    private void updateName() {
        AvatarName data = new AvatarName();
        data.setFirst_name(etName.getText().toString().trim());
        RUserName.Fire(data, new RetrofitHandler.apiResponseHandler<ResponseBody>() {
            @Override
            public void onSuccess(ResponseBody responseBody) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(CompleteUserInfoActivity.this, MainActivity.class);
                                startActivity(intent);
                                CompleteUserInfoActivity.this.finish();
                            }
                        }, 1400);

                    }
                }, 800);
            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }


    public void upudateAvatar(int pic)
            throws IOException {

        Drawable drawable = getResources().getDrawable(R.drawable.avatar1);
        switch (pic)
        {
            case 1:
                drawable = getResources().getDrawable(R.drawable.avatar2);
                break;
            case 2:
                drawable = getResources().getDrawable(R.drawable.avatar3);
                break;
            case 3:
                drawable = getResources().getDrawable(R.drawable.avatar3);
                break;
            case 4:
                drawable = getResources().getDrawable(R.drawable.avatar4);
                break;
            case 5:
                drawable = getResources().getDrawable(R.drawable.avatar5);
                break;
            case 6:
                drawable = getResources().getDrawable(R.drawable.avatar6);
                break;
            case 7:
                drawable = getResources().getDrawable(R.drawable.avatar7);
                break;
            case 8:
                drawable = getResources().getDrawable(R.drawable.avatar8);
                break;
            case 9:
                drawable = getResources().getDrawable(R.drawable.avatar9);
                break;
            case 10:
                drawable = getResources().getDrawable(R.drawable.avatar10);
                break;
            case 11:
                drawable = getResources().getDrawable(R.drawable.avatar11);
                break;
            case 12:
                drawable = getResources().getDrawable(R.drawable.avatar12);
                break;
        }

            if (drawable != null) {
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                final byte[] bitmapdata = stream.toByteArray();


                final int random = new Random().nextInt(10000);
                myFile = new File(Environment.getExternalStorageDirectory() + "/" + File.separator + "avatar99"
                        +random+".png");
                FileUtils.writeByteArrayToFile(myFile, bitmapdata);

            }


            ApiInterface service = ApiClient.getClient(this).create(ApiInterface.class);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), myFile);
            MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", myFile.getName(), requestFile);
            dialog = new Dialog(this);
            dialog.setContentView(R.layout.done_dialog);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            Window window = dialog.getWindow();
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            Call<AvatarOut> call = service.postFile(body);
            call.enqueue(new Callback<AvatarOut>() {
                @Override
                public void onResponse(Call<AvatarOut> call,
                                       Response<AvatarOut> response) {
                    isUploaded = true;
                    Glide.with(CompleteUserInfoActivity.this)
                            .load(response.body().getAvatar())  //http://karmento.ir      http://karmento.ir
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                            .fitCenter()//this method help to fit image into center of your ImageView
                            .into(ivLogo);

                    updateName();


                }

                @Override
                public void onFailure(Call<AvatarOut> call, Throwable t) {
                    dialog.dismiss();
                }
            });


    }




    private boolean havePermissions() {
        if (ActivityCompat.checkSelfPermission(CompleteUserInfoActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(CompleteUserInfoActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 7);
            return false;
        } else if (ActivityCompat.checkSelfPermission(CompleteUserInfoActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(CompleteUserInfoActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 8);
            return false;
        }
        return true;
    }


}
