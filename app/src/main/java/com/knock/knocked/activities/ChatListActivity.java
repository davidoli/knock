package com.knock.knocked.activities;

import android.app.ActivityOptions;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.knock.knocked.R;
import com.knock.knocked.adapters.ChatListAdapter;
import com.knock.knocked.listeners.ChatListRecyclerViewListener;
import com.knock.knocked.models.ChatListModel;
import com.knock.knocked.rest.RChatList;
import com.knock.knocked.rest.RChatOrders;
import com.knock.knocked.rest.RetrofitHandler;
import com.knock.knocked.rest.models.ChatListDataOut;
import com.knock.knocked.rest.models.ChatOrdersOutData;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * Description: This is Chat list activity which
 * includes the contacts list in chat room page
 */


public class ChatListActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView ivBack;
    /**
     * RecyclerView for contacts in chat list
     */
    private RecyclerView recyclerView;
    /**
     * Adapter for contacts list
     */
    private RecyclerView.Adapter mAdapter;
    /**
     * Click Listener for items of contacts list in chat room
     */
    private ChatListRecyclerViewListener listener;
    /**
     * Array list for contacts in chat list
     */
    private ArrayList<ChatListModel> chatListModelsList = new ArrayList<>();
    private RelativeLayout rlClose;
    private Dialog dialog;
    private TextView tvEmpty, txtFilter;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);
        initView();
        recyclerViewStuff();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     */

    private void initView() {
        ivBack = findViewById(R.id.iv_back);
        tvEmpty = findViewById(R.id.tv_empty);
        txtFilter = findViewById(R.id.txt_filter);
        rlClose = findViewById(R.id.rl_close);
        recyclerView = findViewById(R.id.chat_list_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        Typeface tf2 = Typeface.createFromAsset(getAssets(), "fonts/montserrat2.otf");
        txtFilter.setTypeface(tf2);

        ivBack.setOnClickListener(this);
        rlClose.setOnClickListener(this);
    }


    /**
     * Description: start animation when exit
     * the activity with back button pressing
     */

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            super.onBackPressed();
            overridePendingTransitionExit();
        }
        return true;

    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                super.onBackPressed();
                overridePendingTransitionExit();
                break;
            case R.id.rl_close:
                overridePendingTransitionExit();
                break;
        }
    }


    /**
     * Description: Function which animate the
     * page when exit the activity
     */

    protected void overridePendingTransitionExit() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_down2, R.anim.slide_up2);
    }


    /**
     * Description: This is the method
     * which sets the recyclerview stuffs
     * its listener and layout manager and ...
     */

    private void recyclerViewStuff() {
        listener = new ChatListRecyclerViewListener() {
            @Override
            public void onClick(View view, final int position, final ChatListModel model, String type) {
                Intent i = new Intent(ChatListActivity.this, ChatBoxActivity.class);
                i.putExtra("receiver_id", model.getId());
                i.putExtra("name", model.getName());
                i.putExtra("pic", model.getPic());
                startActivity(i);
                overridePendingTransitionEnter();


            }

        };


        mAdapter = new ChatListAdapter(chatListModelsList, this, listener, 1);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        getChatList();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_down, R.anim.slide_up);
    }


    private void getChatList() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.waiting_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        RChatList.Fire(new RetrofitHandler.apiResponseHandler<ChatListDataOut>() {
            @Override
            public void onSuccess(ChatListDataOut chatListDataOut) {
                dialog.dismiss();
                try {
                    if (chatListDataOut.getList().size() == 0)
                        tvEmpty.setVisibility(View.VISIBLE);
                    else
                        tvEmpty.setVisibility(View.GONE);
                    for (int i = 0; i < chatListDataOut.getList().size(); i++) {
                        ChatListModel model = new ChatListModel();
                        model.setName(chatListDataOut.getList().get(i).getUser().getFirst_name() + " " +
                                chatListDataOut.getList().get(i).getUser().getLast_name());
                        model.setLastMessage(chatListDataOut.getList().get(i).getLast_message());
                        model.setTime("last seen recently");
                        model.setPic(chatListDataOut.getList().get(i).getUser().getAvatar());
                        model.setId(chatListDataOut.getList().get(i).getUser().getId());
                        if (chatListDataOut.getList().get(i).getOrder() != null)
                            model.setKnocked(true);
                        else
                            model.setKnocked(false);
                        chatListModelsList.add(model);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    tvEmpty.setVisibility(View.VISIBLE);
                }

                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
                tvEmpty.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
                tvEmpty.setVisibility(View.VISIBLE);
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
                tvEmpty.setVisibility(View.VISIBLE);
            }
        });
    }


    protected void overridePendingTransitionEnter() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }


}