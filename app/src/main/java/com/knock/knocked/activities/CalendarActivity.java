package com.knock.knocked.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.knock.knocked.G;
import com.knock.knocked.R;
import com.knock.knocked.adapters.CalendarVisitAdapter;
import com.knock.knocked.adapters.WorkingHourAdapter;
import com.knock.knocked.listeners.VisitRecyclerViewListener;
import com.knock.knocked.listeners.WorkingHoursRecyclerViewListener;
import com.knock.knocked.models.CalendarVisitModel;
import com.knock.knocked.models.OrdersOutData;
import com.knock.knocked.models.WorkingHourModel;
import com.knock.knocked.rest.ROrders;
import com.knock.knocked.rest.RetrofitHandler;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CalendarActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView ivCanel;
    /**
     * RecyclerView for working hours in chat list
     */
    private RecyclerView recyclerView;
    /**
     * Adapter for working hours list
     */
    private RecyclerView.Adapter mAdapter;
    /**
     * Click Listener for items of working hours list in page
     */
    private VisitRecyclerViewListener listener;
    /**
     * Array list for working hours
     */
    private ArrayList<CalendarVisitModel> calendarVisitList = new ArrayList<>();
    private Button btnReserve;
    private CalendarView calendarView;
    private Dialog dialog;
    private TextView tvName,tvEmpty;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        initView();
        recyclerViewStuff();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     * then get the list of professions and
     * experts
     */

    private void initView() {
        ivCanel = findViewById(R.id.iv_cancel);
        tvName = findViewById(R.id.tv_name);
        tvEmpty = findViewById(R.id.tv_empty);
        calendarView = findViewById(R.id.calendar_view);
        btnReserve = findViewById(R.id.btn_reserve);
        recyclerView = findViewById(R.id.recycler_view);

        ivCanel.setOnClickListener(this);
        btnReserve.setOnClickListener(this);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
               calendarVisitList.clear();
               String months="",days="";
               month = month+1;
               if(month<10)
                   months = "0"+month;
               else
                   months = months+"";

                if(dayOfMonth<10)
                    days = "0"+dayOfMonth;
                else
                    days = dayOfMonth+"";

               getThem(year+"-"+months+"-"+days);

            }
        });


        SharedPreferences settings = G.getContext().getSharedPreferences("me",
                Context.MODE_PRIVATE);
        tvName.setText(settings.getString("user_name",""));


    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.iv_cancel:
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

        }
    }



    private void recyclerViewStuff()
    {

        GridLayoutManager layoutManager = new GridLayoutManager(this,1);
        recyclerView.setLayoutManager(layoutManager);

        listener = new VisitRecyclerViewListener() {
            @Override
            public void onClick(View view, final int position, final CalendarVisitModel model, String type) {
                Intent i = new Intent(CalendarActivity.this, ChatBoxActivity.class);
                i.putExtra("receiver_id", model.getId());
                i.putExtra("name", model.getName());
                startActivity(i);
                overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
            }

        };


        mAdapter = new CalendarVisitAdapter(calendarVisitList, this, listener, 1);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();


        getThem("");
    }


    public void fadeInAnimation()
    {
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(1800);

        btnReserve.setAnimation(fadeIn);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                btnReserve.setVisibility(View.VISIBLE);
            }
        },1800);

    }


    private void getThem(String date)
    {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.waiting_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        ROrders.Fire(date, new RetrofitHandler.apiResponseHandler<OrdersOutData>() {
            @Override
            public void onSuccess(OrdersOutData ordersOutData) {
                dialog.dismiss();
                for (int i=0;i<ordersOutData.getList().size();i++)
                {
                    CalendarVisitModel model = new CalendarVisitModel();
                    model.setName(ordersOutData.getList().get(i).getOwner().getFirst_name()+" "+
                            ordersOutData.getList().get(i).getOwner().getLast_name());
                    model.setLocation("");
                    model.setId(ordersOutData.getList().get(i).getOwner().getId());
                    model.setDate(ordersOutData.getList().get(i).getOrder_time());
                    calendarVisitList.add(model);
                }
                mAdapter.notifyDataSetChanged();

                if(ordersOutData.getList().size() == 0)
                    tvEmpty.setVisibility(View.VISIBLE);
                else
                    tvEmpty.setVisibility(View.GONE);
            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }

}