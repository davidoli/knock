package com.knock.knocked.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.knock.knocked.R;
import com.knock.knocked.adapters.UserFilterAdapter;
import com.knock.knocked.listeners.UserFilterRecyclerViewListener;
import com.knock.knocked.models.LanguageData;
import com.knock.knocked.models.UserFilterModel;
import com.knock.knocked.rest.RLanguages;
import com.knock.knocked.rest.RetrofitHandler;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SelectLanguageActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout rlClose;
    /**
     * RecyclerView list for professions list
     */
    private RecyclerView userFilterRecyclerView;
    /**
     * Adapter for professions list
     */
    private RecyclerView.Adapter userFilterAdapter;
    /**
     * Listener for click action ( click on profession list items )
     */
    private UserFilterRecyclerViewListener userFilterlistener;
    /**
     * Array list for professions
     */
    private ArrayList<UserFilterModel> userFilterModelsList = new ArrayList<>();
    private RadioButton rMale,rFemale;
    private Button btnFilter;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);
        initView();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     */

    private void initView() {
        rlClose = findViewById(R.id.rl_close);
        rMale = findViewById(R.id.r_male);
        rFemale = findViewById(R.id.r_female);
        btnFilter = findViewById(R.id.btn_filter);
        userFilterRecyclerView = findViewById(R.id.recycler_view);

        setRecyclerViewStuff();

        getLanguages();

        rlClose.setOnClickListener(this);
    }


    /**
     * Description: start animation when exit
     * the activity with back button pressing
     */

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        //replaces the default 'Back' button action
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            super.onBackPressed();
            overridePendingTransitionExit();
        }
        return true;

    }



    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.rl_close:
                overridePendingTransitionExit();
                break;

//            case R.id.btn_filter:
//                filter();
//                break;
        }
    }


    /**
     * Description: Function which animate the
     * page when exit the activity
     */

    protected void overridePendingTransitionExit() {
        super.onBackPressed();
//        overridePendingTransition(R.anim.slide_down2, R.anim.slide_up2);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_down, R.anim.slide_up);
    }


    private void setRecyclerViewStuff() {

        userFilterlistener = new UserFilterRecyclerViewListener() {
            @Override
            public void onClick(View view, final int position, final UserFilterModel model, String type) {
                for (int i = 0; i < userFilterModelsList.size(); i++)
                    userFilterModelsList.get(i).setClicked(false);
                model.setClicked(true);
                userFilterAdapter.notifyDataSetChanged();
            }

        };


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        userFilterRecyclerView.setLayoutManager(layoutManager);
        userFilterAdapter = new UserFilterAdapter(userFilterModelsList, SelectLanguageActivity.this, userFilterlistener, 1);
        userFilterRecyclerView.setAdapter(userFilterAdapter);
        userFilterAdapter.notifyDataSetChanged();

    }


    private void getLanguages()
    {
        RLanguages.Fire(new RetrofitHandler.apiResponseHandler<LanguageData>() {
            @Override
            public void onSuccess(LanguageData languageData) {

                for(int i=0;i<languageData.getList().size();i++)
                {
                    UserFilterModel model = new UserFilterModel();
                    model.setLanguage(languageData.getList().get(i).getTitle());
                    model.setId(languageData.getList().get(i).getId());
                    model.setClicked(false);
                    userFilterModelsList.add(model);
                }
                userFilterAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFail(int code, String message) {
                int a =1;
            }

            @Override
            public void onFailure(Throwable t) {
                int a =1;
            }

            @Override
            public void onException(Exception e) {
                int a =1;
            }
        });
    }

    private void filter()
    {

    }

}