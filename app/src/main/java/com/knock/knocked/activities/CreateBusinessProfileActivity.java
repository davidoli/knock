package com.knock.knocked.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.snackbar.Snackbar;
import com.knock.knocked.ApiClient;
import com.knock.knocked.ApiInterface;
import com.knock.knocked.R;
import com.knock.knocked.adapters.ProfessionListAdapter;
import com.knock.knocked.models.AvatarOut;
import com.knock.knocked.models.MyInfoData;
import com.knock.knocked.models.ProfessionModel;
import com.knock.knocked.rest.RMyInfo;
import com.knock.knocked.rest.RServices;
import com.knock.knocked.rest.RUpload;
import com.knock.knocked.rest.RetrofitHandler;
import com.knock.knocked.rest.models.AvatarInData;
import com.knock.knocked.rest.models.ServicesData;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.media.MediaRecorder.VideoSource.CAMERA;


/**
 * Description: This is 'Business creating' activity
 * which includes the page for users to
 * register as a service provider
 */

public class CreateBusinessProfileActivity extends BaseActivity implements View.OnClickListener,AdapterView.OnItemSelectedListener {
    private Spinner catSpinner;
    private ArrayList<String> spinnerlist = new ArrayList<>();
    private ArrayList<Integer> spinnerIds = new ArrayList<>();
    private RelativeLayout rlContinue;
    private ImageView ivBack;
    private CircleImageView ivUpload;
    private Dialog dialog;
    private EditText etDes;
    private int catId,inviteNums;
    private String des,web,insta;
    private double lat1,lat2,lat3,lon1,lon2,lon3;
    private boolean isFreelancer;
    private Uri filePath;
    private int PICK_IMAGE_REQUEST = 1;
    private int GALLERY = 1, CAMERA = 2;
    private Bitmap bitmap;
    private LinearLayout lLayout;
    private boolean isUploaded;
    private TextView tvMe,tvMe2;
    private SharedPreferences settings;




    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_profile);
        initView();
        keyboardOpens();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     */

    private void initView() {
        catSpinner = findViewById(R.id.spinner_cat);
        rlContinue = findViewById(R.id.rl_continue);
        tvMe = findViewById(R.id.tv_me);
        tvMe2 = findViewById(R.id.tv_me2);
        ivBack = findViewById(R.id.iv_back);
        etDes = findViewById(R.id.et_des);
        ivUpload = findViewById(R.id.iv_upload);
        lLayout = findViewById(R.id.l_layout);

        catSpinner.setOnItemSelectedListener(this);
        rlContinue.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        ivUpload.setOnClickListener(this);
        lLayout.setOnClickListener(this);

        getTheServicesList();

    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_continue:
                if (checkInput()) {
                    Intent intent = new Intent(this, DurationServiceActivity.class);
                    settings = getSharedPreferences("me",
                            Context.MODE_PRIVATE);
                    settings.edit().putInt("cat_id",catId)  .apply();
                    settings.edit().putInt("invite",inviteNums).apply();
                    settings.edit().putString("des",etDes.getText().toString()).apply();
//                    settings.edit().putString("web",web).apply();
//                    settings.edit().putString("insta",insta).apply();
                    settings.edit().putFloat("locationLat1", (float) lat1).apply();
                    settings.edit().putFloat("locationLat2", (float) lat2).apply();
                    settings.edit().putFloat("locationLat1", (float) lat3).apply();
                    settings.edit().putFloat("locationLon1", (float) lon1).apply();
                    settings.edit().putFloat("locationLon2", (float) lon2).apply();
                    settings.edit().putFloat("locationLon3", (float) lon3).apply();
                    settings.edit().putBoolean("is_freelancer", isFreelancer).apply();
                    startActivityForResult(intent, 1);
                }
                break;
            case R.id.iv_back:
                super.onBackPressed();
                break;
            case R.id.iv_upload:
                showPictureDialog();
                break;
            case R.id.l_layout:
                hideKeyboard(CreateBusinessProfileActivity.this);
                break;
        }
    }


    private void getTheServicesList() {

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.waiting_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        RServices.Fire(new RetrofitHandler.apiResponseHandler<ServicesData>() {
            @Override
            public void onSuccess(ServicesData servicesData) {
                successProcess(servicesData);
            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }


    private void successProcess(ServicesData data) {
        dialog.dismiss();
        getUerInfo();

        spinnerlist.add("Business Type");
        spinnerIds.add(-1);
        for (int i = 0; i < data.getList().size(); i++) {
            spinnerlist.add(data.getList().get(i).getTitle());
            spinnerIds.add(data.getList().get(i).getId());
            ArrayAdapter aa = new ArrayAdapter(this, R.layout.simple_spinner_dropdown_item, spinnerlist);
            aa.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            //Setting the ArrayAdapter data on the Spinner
            catSpinner.setAdapter(aa);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        catId = spinnerIds.get(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private boolean checkInput() {
        if (etDes.getText().toString().length() < 2) {
            Snackbar snackbar = Snackbar
                    .make(lLayout, "Fill Description Correctly!", Snackbar.LENGTH_SHORT);
            snackbar.show();
            return false;
        }
        else if(!isUploaded)
        {
            Snackbar snackbar = Snackbar
                    .make(lLayout, "Upload Your Image First!", Snackbar.LENGTH_SHORT);
            snackbar.show();
            return false;
        }
        else if(catId<0)
        {
            Snackbar snackbar = Snackbar
                    .make(lLayout, "Select a business type!", Snackbar.LENGTH_SHORT);
            snackbar.show();
            return false;
        }
        return true;
    }


    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Choose...");
        String[] pictureDialogItems = {
                "Pick Image From Gallery",
                "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (getReadStoragePermission())
                                    choosePhotoFromGallary();
                                break;
                            case 1:

//                                if (getCameraPermission())
                                    takePhotoFromCamera();
                                break;

                        }
                    }
                });
        pictureDialog.show();
    }


    private boolean getReadStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
            return false;
        } else
            return true;

    }




    public void choosePhotoFromGallary() {
        Intent intent = new Intent();
        intent.setType("*/*");  //  */*          image/*
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }

    private void takePhotoFromCamera() {

        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);



    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), filePath);
                upload(filePath);


            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == this.RESULT_CANCELED) {
            return;
        } else if (requestCode == GALLERY) {
            if (data != null) {
                filePath = data.getData();
                if(filePath != null) {
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), filePath);
//                    upload(filePath.toString());
                        upload(filePath);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");

            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
            Uri tempUri = getImageUri(this, photo);

            // CALL THIS METHOD TO GET THE ACTUAL PATH
            File finalFile = new File(getRealPathFromURI(tempUri));

        }
        else
        {
            settings = getSharedPreferences("me",
                    Context.MODE_PRIVATE);
            int catId = data.getIntExtra("cat_id", 0);
            String des = data.getStringExtra("des");

            etDes.setText(des);

            inviteNums = settings.getInt("invite",-1);
            web = settings.getString("web","");
            des = settings.getString("des","");
            insta = settings.getString("insta","");
            lat1 = settings.getFloat("lat1",-1);
            lat2 = settings.getFloat("lat2",-1);
            lat3 = settings.getFloat("lat3",-1);
            lon1 = settings.getFloat("lon1",-1);
            lon2 = settings.getFloat("lon2",-1);
            lon3 = settings.getFloat("lon3",-1);
            catId = settings.getInt("cat_id",-1);
            isFreelancer = settings.getBoolean("is_freelancer",false);

            etDes.setText(des);


        }
    }



    private void uploadFile(final Uri fileUri) {

        String filePath = getRealPathFromUri(fileUri);
        if (filePath != null && !filePath.isEmpty()) {
            File file = new File(filePath);
            if (file.exists()) {


            }
        }
    }


    public String getRealPathFromUri(final Uri uri) {
        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(CreateBusinessProfileActivity.this, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(CreateBusinessProfileActivity.this, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(CreateBusinessProfileActivity.this, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(CreateBusinessProfileActivity.this, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        filePath = Uri.parse(path);
        upload(filePath);
//        uploadFile(filePath);
        return Uri.parse(path);
    }


    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (this.getContentResolver() != null) {
            Cursor cursor = this.getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }


    private void upload(Uri avatar) {


        String filePath = getRealPathFromUri(avatar);
        if (filePath != null && !filePath.isEmpty()) {
            File file = new File(filePath);
            if (file.exists()) {


                ApiInterface service = ApiClient.getClient(this).create(ApiInterface.class);
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);
                dialog = new Dialog(this);
                dialog.setContentView(R.layout.done_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                Call<AvatarOut> call = service.postFile(body);
                call.enqueue(new Callback<AvatarOut>() {
                    @Override
                    public void onResponse(Call<AvatarOut> call,
                                           Response<AvatarOut> response) {
                        isUploaded = true;
                        Glide.with(CreateBusinessProfileActivity.this)
                                .load(response.body().getAvatar())  //http://karmento.ir      http://karmento.ir
                                .dontAnimate()
                                .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                                .fitCenter()//this method help to fit image into center of your ImageView
                                .into(ivUpload);

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                dialog.dismiss();
                            }
                        },1800);

                    }

                    @Override
                    public void onFailure(Call<AvatarOut> call, Throwable t) {
                        dialog.dismiss();
                    }
                });
            }
        }
    }


    private void keyboardOpens() {
        getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                getWindow().getDecorView().getWindowVisibleDisplayFrame(r);
                int screenHeight = getWindow().getDecorView().getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;


                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                   tvMe.setVisibility(View.INVISIBLE);
                   tvMe2.setVisibility(View.INVISIBLE);
                } else {
                    // keyboard is closed
                    tvMe.setVisibility(View.VISIBLE);
                    tvMe2.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    private void getUerInfo() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.waiting_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        RMyInfo.Fire(new RetrofitHandler.apiResponseHandler<MyInfoData>() {
            @Override
            public void onSuccess(MyInfoData myInfoData) {
                dialog.dismiss();
                Glide.with(CreateBusinessProfileActivity.this)
                        .load(myInfoData.getAvatar())  //http://karmento.ir      http://karmento.ir
                        .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                        .fitCenter()//this method help to fit image into center of your ImageView
                        .into(ivUpload);

            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }

}