package com.knock.knocked.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.snackbar.Snackbar;
import com.knock.knocked.ApiClient;
import com.knock.knocked.ApiInterface;
import com.knock.knocked.R;
import com.knock.knocked.models.AvatarOut;
import com.knock.knocked.rest.RServices;
import com.knock.knocked.rest.RetrofitHandler;
import com.knock.knocked.rest.models.ServicesData;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DurationServiceActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private Spinner catSpinner;
    private ArrayList<String> spinnerlist = new ArrayList<>();
    private ArrayList<Integer> spinnerIds = new ArrayList<>();
    private RelativeLayout rlContinue;
    private ImageView ivBack;
    private int catId;
    private RelativeLayout lLayout;
    private float duration;
    private TextView tvPer;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duration);
        initView();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     */

    private void initView() {
        catSpinner = findViewById(R.id.spinner_cat);
        rlContinue = findViewById(R.id.rl_login);
        ivBack = findViewById(R.id.iv_back);
        lLayout = findViewById(R.id.rl_layout);
        tvPer = findViewById(R.id.tv_per);

        catSpinner.setOnItemSelectedListener(this);
        rlContinue.setOnClickListener(this);
//        ivBack.setOnClickListener(this);
//        lLayout.setOnClickListener(this);


        spinnerlist.add("Select Time Slot");
        spinnerIds.add(-1);
        spinnerlist.add("30 mins per client");
        spinnerIds.add(0);
        spinnerlist.add("1 hour per client");
        spinnerIds.add(1);
        spinnerlist.add("2 hours per client");
        spinnerIds.add(2);
//        spinnerlist.add("3 hours");
//        spinnerIds.add(3);
        ArrayAdapter aa = new ArrayAdapter(this, R.layout.simple_spinner_dropdown_item, spinnerlist);
        aa.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        catSpinner.setAdapter(aa);


//        SharedPreferences settings = getSharedPreferences("me",
//                Context.MODE_PRIVATE);
//        duration = settings.getFloat("duration",-1);
//        if(duration == .5)
//        {
//            catSpinner.setselectedi
//        }

    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_back:
                super.onBackPressed();
                break;

            case R.id.rl_layout:
                hideKeyboard(DurationServiceActivity.this);
                break;

            case R.id.rl_login:
                SharedPreferences settings = getSharedPreferences("me",
                        Context.MODE_PRIVATE);
                if(catId == 0)
                    duration = (float) 0.5;
                else if(catId == 1)
                    duration = 1;
                else if(catId == 2)
                    duration = 2;
                settings.edit().putFloat("duration", duration).apply();
                Intent intent = new Intent(this, CompletingInfoActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                break;
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        catId = spinnerIds.get(i);
        if(catId>=0)
            tvPer.setVisibility(View.GONE);
        else
            tvPer.setVisibility(View.GONE);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    protected void overridePendingTransitionExit() {
        SharedPreferences settings = getSharedPreferences("me",
                Context.MODE_PRIVATE);
        if(catId == 0)
            duration = (float) 0.5;
        else if(catId == 1)
            duration = 1;
        else if(catId == 2)
            duration = 2;
        settings.edit().putFloat("duration", duration).apply();

        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

}
