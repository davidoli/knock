package com.knock.knocked.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.knock.knocked.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class InviteFriendsActivity extends AppCompatActivity implements  View.OnClickListener {
    private RelativeLayout rlMessage;
    private int inviteNum;
    private int catId,inviteNums,userId;
    private String des,web,insta;
    private double lat1,lat2,lat3,lon1,lon2,lon3;
    private boolean isFreelancer;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);
        initView(savedInstanceState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.rl_message:
                Intent intent = new Intent(this,ContacsListActivity.class);
                intent.putExtra("invite",inviteNum);
                intent.putExtra("web", web);
                intent.putExtra("insta", insta);
                intent.putExtra("locationLat1", lat1);
                intent.putExtra("locationLon1", lon1);
                intent.putExtra("locationLat2", lat2);
                intent.putExtra("locationLon2", lon2);
                intent.putExtra("locationLat3", lat3);
                intent.putExtra("locationLon3", lon3);
                intent.putExtra("cat_id", catId);
                intent.putExtra("user_id", userId);
                intent.putExtra("des", des);
                intent.putExtra("is_freelancer", isFreelancer);
                startActivityForResult(intent,3);
                break;
        }
    }

    private void initView(Bundle savedInstanceState) {

        inviteNum = getIntent().getIntExtra("invite",0);

        web = getIntent().getStringExtra("web");
        insta = getIntent().getStringExtra("insta");
        des = getIntent().getStringExtra("des");
        lat1 = getIntent().getDoubleExtra("lat1",-1);
        lat2 = getIntent().getDoubleExtra("lat2",-1);
        lat3 = getIntent().getDoubleExtra("lat3",-1);
        lon1 = getIntent().getDoubleExtra("lon1",-1);
        lon2 = getIntent().getDoubleExtra("lon2",-1);
        lon3 = getIntent().getDoubleExtra("lon3",-1);
        catId = getIntent().getIntExtra("cat_id",-1);
        userId = getIntent().getIntExtra("user_id",-1);
        isFreelancer = getIntent().getBooleanExtra("is_freelancer",false);


        rlMessage = findViewById(R.id.rl_message);

        rlMessage.setOnClickListener(this);
    }



    protected void overridePendingTransitionExit() {
//        super.onBackPressed();
        Intent intent = new Intent();
        intent.putExtra("cat_id", catId);
        intent.putExtra("des", des);
        intent.putExtra("invite", inviteNums);
        intent.putExtra("web", web);
        intent.putExtra("insta", insta);
        intent.putExtra("locationLat1", lat1);
        intent.putExtra("locationLon1", lon1);
        intent.putExtra("locationLat2", lat2);
        intent.putExtra("locationLon2", lon2);
        intent.putExtra("locationLat3", lat3);
        intent.putExtra("locationLon3", lon3);
        intent.putExtra("cat_id", lon3);
        intent.putExtra("user_id", userId);
        intent.putExtra("is_freelancer", isFreelancer);
        setResult(123, intent);
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_down2, R.anim.slide_up2);
    }

    @Override
    public void onBackPressed() {
        overridePendingTransitionExit();
//        overridePendingTransition(R.anim.slide_down, R.anim.slide_up);
//        Intent intent = new Intent();
//        intent.putExtra("address", address.getCountryName()+address.getSubAdminArea()+address.getSubLocality());
//        intent.putExtra("number", number);
//        setResult(1, intent);
//        super.onBackPressed();
//        overridePendingTransition(R.anim.slide_down2, R.anim.slide_up2);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (resultCode == RESULT_OK || true) {
            int catId = data.getIntExtra("cat_id", 0);
            String des = data.getStringExtra("des");


            inviteNums = getIntent().getIntExtra("invite",-1);
            web = getIntent().getStringExtra("web");
            insta = getIntent().getStringExtra("insta");
            des = getIntent().getStringExtra("des");
            lat1 = getIntent().getDoubleExtra("lat1",-1);
            lat2 = getIntent().getDoubleExtra("lat2",-1);
            lat3 = getIntent().getDoubleExtra("lat3",-1);
            lon1 = getIntent().getDoubleExtra("lon1",-1);
            lon2 = getIntent().getDoubleExtra("lon2",-1);
            lon3 = getIntent().getDoubleExtra("lon3",-1);
            catId = getIntent().getIntExtra("cat_id",-1);
            userId = getIntent().getIntExtra("user_id",-1);
            isFreelancer = getIntent().getBooleanExtra("is_freelancer",false);

        }
    }

}



