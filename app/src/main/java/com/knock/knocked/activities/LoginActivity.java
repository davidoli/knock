package com.knock.knocked.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.knock.knocked.R;
import com.knock.knocked.adapters.WorkingHourAdapter;
import com.knock.knocked.listeners.WorkingHoursRecyclerViewListener;
import com.knock.knocked.models.LoginData;
import com.knock.knocked.models.WorkingHourModel;
import com.knock.knocked.rest.RLogin;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private RelativeLayout rlLogin,rlLayout;
    private TextInputLayout tlPhone;
    private TextInputEditText etPhone;
    private TextView tvRegister,tvKnocked,tvDes;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     * then get the list of professions and
     * experts
     */

    private void initView() {
        rlLogin = findViewById(R.id.rl_login);
        tlPhone = findViewById(R.id.tl_phone);
        etPhone = findViewById(R.id.et_phone);
        tvRegister = findViewById(R.id.tv_register);
        rlLayout = findViewById(R.id.rl_layout);
        tvKnocked = findViewById(R.id.tv_knocked);
        tvDes = findViewById(R.id.tv_des);

        Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/grobold.ttf");
        tvKnocked.setTypeface(tf);

        rlLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
        rlLayout.setOnClickListener(this);
    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.rl_login:
                tlPhone.setError(null);
                if(etPhone.getText().toString().trim().length()<11)
                    tlPhone.setError("Fill it correctly");
                else {
                    Intent intent = new Intent(this, ConfirmationCodeActivity.class);
                    intent.putExtra("already", true);
                    intent.putExtra("login", true);
                    intent.putExtra("phone",etPhone.getText().toString().trim() );
                    startActivity(intent);
                }
                break;
            case R.id.tv_register:
                Intent intent = new Intent(this, RegisterActivity.class);
                startActivity(intent);
                LoginActivity.this.finish();
                break;
            case R.id.rl_layout:
                hideKeyboard(LoginActivity.this);
                break;
        }
    }


}
