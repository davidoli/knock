package com.knock.knocked.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import com.knock.knocked.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    private int SPLASH_DISPLAY_LENGTH = 2000, testServerControler = 0;
    private ImageView refresh;
    private ProgressBar progressBar;
    private String url;
    private AlertDialog.Builder builder;
    private  SharedPreferences settings ;
    private Dialog dialog;
    private TextView tvKnocked;




    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

         settings = getSharedPreferences("me",
                Context.MODE_PRIVATE);

        tvKnocked = findViewById(R.id.tv_knocked);

        Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/grobold.ttf");
        tvKnocked.setTypeface(tf);



        //Set the views and widgets and their clicks of page
        initView();

        //Check the version of the app and its process after the delay of the animation of the splash
        //Set a delay for the running of the splash animation an then decide if user has registered or not
        runTheSplashAnimation();



    }

    private void initView() {

    }


    private void startNewIntent(Activity activity, Class myClass) {
        Intent intent = new Intent(activity, myClass);
        startActivity(intent);
        finish();
    }


    private void runTheSplashAnimation() {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //if user not logged into the app yet
                if (settings.getString("token", "0").equals("0")) {
                    if (isNetworkConnected())
                        startNewIntent(SplashActivity.this, LoginActivity.class);
                    else
                        noNet();
                } else
                {
                    startNewIntent(SplashActivity.this, MainActivity.class);

                }
                    //check the version function

            }
        }, 1500);

    }


    private void noNet() {
        Toast.makeText(SplashActivity.this, "Check The Network", Toast.LENGTH_SHORT).show();
        refresh.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onClick(View v) {
        //Set onclick for the refresh icon
        refresh.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }


    private void loadThePage() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isNetworkConnected() && testServerControler != 0) {
                    settings.edit().putString("issu", "0").commit();
                    String firstTime = settings.getString("token", "0");
                    String confirm = settings.getString("confirm", "0");
                    String changePass = settings.getString("change_pass", "0");


                    //if the user had wanted change the pass and had been entered the sms code sent to them but hadn't changed their pass
                    if (changePass.equals("1"))
                        startNewIntent(SplashActivity.this, RegisterActivity.class);
                    else if (!firstTime.equals("0") && confirm.equals("1"))
                        startNewIntent(SplashActivity.this, MainActivity.class);
                        //Check if its first time of entering the app, so goes to the slider page and register or login section
                    else
                        startNewIntent(SplashActivity.this, MainActivity.class);

                } else
                    errorAfterServerAccess();


            }
        }, SPLASH_DISPLAY_LENGTH);
    }


    private void errorAfterServerAccess() {
        testServerControler = 0;
        refresh.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }




}