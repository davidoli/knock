package com.knock.knocked.activities;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.knock.knocked.R;
import com.knock.knocked.models.AddAddressInData;
import com.knock.knocked.rest.RAddAddress;
import com.knock.knocked.rest.RetrofitHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.ResponseBody;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MapActivity2 extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback, LocationListener {
    private ImageView ivBack;
    private GoogleMap mMap;
    private Location currentLocation;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private TextView tvAddress;
    private double pointLat, pointLon;
    private Address address;
    private int number;
    private RelativeLayout rlLogin;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map2);
        initView();

    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     */

    private void initView() {

        ivBack = findViewById(R.id.iv_back);
        rlLogin = findViewById(R.id.rl_login);
//        tlTitle = findViewById(R.id.tl_title);
//        btnConfirm = findViewById(R.id.btn_confirm);
        tvAddress = findViewById(R.id.tv_address);
//        btnConfirm.setOnClickListener(MapActivity2.this::onClick);


        number = getIntent().getIntExtra("num",0);

        ivBack.setOnClickListener(this);
        rlLogin.setOnClickListener(this);


        locationPermissions();
        fetchLastLocation();
        SupportMapFragment mapFrag =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);

    }


    /**
     * Description: start animation when exit
     * the activity with back button pressing
     */

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            super.onBackPressed();
            overridePendingTransitionExit();
        }
        return true;

    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                super.onBackPressed();
                this.finish();
                break;
            case R.id.rl_login:
                LatLng centerLatLang =
                        mMap.getProjection().getVisibleRegion().latLngBounds.getCenter();
                Geocoder geocoder = new Geocoder(MapActivity2.this, Locale.getDefault());
                List<Address> addresses = new ArrayList<>();
                try {
                    addresses = geocoder.getFromLocation(centerLatLang.latitude, centerLatLang.longitude, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                try {
                    address = addresses.get(0);
                    tvAddress.setText(address.getCountryName() + address.getSubAdminArea() + address.getSubLocality());
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                pointLat = centerLatLang.latitude;
                pointLon = centerLatLang.longitude;


//                MapActivity2.super.onBackPressed();
//                MapActivity2.this.finish();
                overridePendingTransitionExit();
                break;
        }
    }


    /**
     * Description: Function which animate the
     * page when exit the activity
     */



    @Override
    public void onLocationChanged(Location location) {

    }


    /**
     * Description: This is the method
     * which gets the location of user
     * by GPS
     */

    private void fetchLastLocation() {

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLocation = location;
                    SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                    supportMapFragment.getMapAsync(MapActivity2.this);

                } else {
                    Toast.makeText(MapActivity2.this, "Your GPS if Off !", Toast.LENGTH_SHORT).show();
                    currentLocation = new Location("current");
                    currentLocation.setLatitude(35.123);
                    currentLocation.setLongitude(50.123);
                }
            }
        });

    }


    private void locationPermissions() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(MapActivity2.this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(MapActivity2.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            return;
        }


        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
    }


    /**
     * Description: This is the method
     * which called when google map is
     * ready to run in the application
     *
     * @return map
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            googleMap.setMyLocationEnabled(true);
            return;
        }


        MapStyleOptions mapStyleOptions = MapStyleOptions.loadRawResourceStyle(MapActivity2.this, R.raw.map_style);
        googleMap.setMapStyle(mapStyleOptions);
        enableMyLocationIfPermitted();
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        try {
            LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        } catch (Exception e) {
            e.printStackTrace();
            LatLng latLng = new LatLng(35.6833, 51.333332);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        }


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                mMap.clear();
                MarkerOptions marker = new MarkerOptions().position(new LatLng(point.latitude, point.longitude)).title("New Marker");
                googleMap.addMarker(marker);

                pointLat = point.latitude;
                pointLon = point.longitude;

                Geocoder geocoder = new Geocoder(MapActivity2.this, Locale.getDefault());
                List<Address> addresses = new ArrayList<>();
                try {
                    addresses = geocoder.getFromLocation(point.latitude, point.longitude, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                try {
                    address = addresses.get(0);
                    tvAddress.setText(address.getCountryName() + address.getSubAdminArea() + address.getSubLocality());
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }



//                btnConfirm.setBackgroundResource(R.drawable.btn_gradient_me);




            }
        });


    }


    /**
     * Description: This is the method
     * which shows the current location
     * icon in map layout of the application
     */


    private void enableMyLocationIfPermitted() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        } else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }

    }




    protected void overridePendingTransitionExit() {
//        super.onBackPressed();
        Intent intent = new Intent();
        try {
            intent.putExtra("address", address.getCountryName() + address.getSubAdminArea() + address.getSubLocality());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        intent.putExtra("number", number);
        intent.putExtra("lat", pointLat);
        intent.putExtra("lon", pointLon);
        setResult(123, intent);
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        overridePendingTransitionExit();

    }




}