package com.knock.knocked.activities;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.maps.android.clustering.ClusterManager;
import com.knock.knocked.R;
import com.knock.knocked.adapters.ExpertListAdapter;
import com.knock.knocked.adapters.ProfessionListAdapter;
import com.knock.knocked.adapters.UserFilterAdapter;
import com.knock.knocked.listeners.ExpertRecyclerViewListener;
import com.knock.knocked.listeners.ProfessionsRecyclerViewListener;
import com.knock.knocked.listeners.UserFilterRecyclerViewListener;
import com.knock.knocked.models.ExpertInData;
import com.knock.knocked.models.ExpertModel;
import com.knock.knocked.models.LanguageData;
import com.knock.knocked.models.MyInfoData;
import com.knock.knocked.models.ProfessionModel;
import com.knock.knocked.models.UserFilterModel;
import com.knock.knocked.rest.RExpertList;
import com.knock.knocked.rest.RLanguages;
import com.knock.knocked.rest.RMyInfo;
import com.knock.knocked.rest.RServices;
import com.knock.knocked.rest.RetrofitHandler;
import com.knock.knocked.rest.models.ExpertData;
import com.knock.knocked.rest.models.ServicesData;
import com.knock.knocked.utilities.MarkerClusterRenderer;
import com.knock.knocked.utilities.Person;

import java.util.ArrayList;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * Description: This is our main activity which
 * includes the map
 */

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback {

    private GoogleMap mMap;
    private Location currentLocation;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private ImageView ivMenu, ivAddresses, ivGender, ivCurrent, ivArrow, ivArrow2, ivLoc;
    private TextView tvBottomCat, tvGender, tvNum, tvEditInfo;
    private RelativeLayout rlBottom, rlContent, rlMessage;
    private LinearLayout nav_addresses, navInvite, navCalendar, navBusiness, navLang, lLnaguage, lGender2;
    private DrawerLayout drawerLayout;
    private ClusterManager<Person> mClusterManager;
    private ActionBarDrawerToggle mDrawerToggle;
    private NavigationView nvView;
    private float lastTranslate = 0.0f;
    private Location mLastLocation;
    private Marker mCurrLocationMarker;
    private GoogleApiClient mGoogleApiClient;
    private View locationButton;
    private boolean filter, maleBool, femaleBool;
    private ArrayList<Integer> languagesList = new ArrayList<>();
    private RelativeLayout rlClose, rlView;
    private Dialog dialog;
    private boolean canClick = true;
    /**
     * RecyclerView list for professions list
     */
    private RecyclerView userFilterRecyclerView;
    /**
     * Adapter for professions list
     */
    private RecyclerView.Adapter userFilterAdapter;
    /**
     * Listener for click action ( click on profession list items )
     */
    private UserFilterRecyclerViewListener userFilterlistener;
    /**
     * Array list for professions
     */
    private ArrayList<UserFilterModel> userFilterModelsList = new ArrayList<>();
    private TextView tvClear, tvLanguage;
    private TextView rMale, rFemale, tvNum2;
    private Button btnFilter;
    private ArrayList<Integer> languages = new ArrayList<>();
    private int serviceId;
    protected LocationRequest locationRequest;
    int REQUEST_CHECK_SETTINGS = 100;
    private LinearLayout lGender;
    private boolean isFirest = true;


    /**
     * RecyclerView list for providers list
     */
    private RecyclerView ProvidersRecyclerView;
    /**
     * Adapter for providers RecyclerView
     */
    private RecyclerView.Adapter mAdapter;
    /**
     * Listener for click action ( click on providers list items )
     */
    private ExpertRecyclerViewListener listener;
    /**
     * Array list for providers
     */
    private ArrayList<ExpertModel> providerList = new ArrayList<>();
    /**
     * RecyclerView list for professions list
     */
    private RecyclerView professionsRecyclerView;
    /**
     * Adapter for professions list
     */
    private RecyclerView.Adapter professionsAdapter;
    /**
     * Listener for click action ( click on profession list items )
     */
    private ProfessionsRecyclerViewListener professionslistener;
    /**
     * Array list for professions
     */
    private ArrayList<ProfessionModel> professionsModelsList = new ArrayList<>();
    /**
     * Spinner like TextView in main page
     * which clicking on it shows the
     * list of professions
     */
    private TextView tvSpinner;
    /**
     * Layout of professions which
     * comes up like a pop up
     * to show the professions list
     */
    private RelativeLayout lProfessions;
    /**
     * Cancel button of the profession
     * pop up page
     */
    private ImageView ivCancel;
    private LinearLayoutManager layoutManager2;
    /**
     * Integer list which gets the
     * coordinates of the spinner
     * like text view for profs list
     * animation
     */
    private int[] location = new int[2];

    private RelativeLayout rlUserFilter;
    private TextView tvNavigatinoName, txtFilter;
    private LinearLayout navLogOut;
    private View view;
    private boolean isCollapse, isCollapse2;
    private int filterCount;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Configuration configuration = getResources().getConfiguration();
//        configuration.setLayoutDirection(new Locale("fa"));
//        getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
        setContentView(R.layout.activity_main);
        initView();

        locationPermissions();
        fetchLastLocation();
        setRecyclerViewStuff();

        SupportMapFragment mapFrag =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);


        getMyInfo();


        FirebaseApp.initializeApp(this);


        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {

            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);


    }


    /**
     * Description: This is the method
     * which called when google map is
     * ready to run in the application
     *
     * @return map
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            googleMap.setMyLocationEnabled(true);
            return;
        }


        MapStyleOptions mapStyleOptions = MapStyleOptions.loadRawResourceStyle(MainActivity.this, R.raw.map_style);
        googleMap.setMapStyle(mapStyleOptions);
        enableMyLocationIfPermitted();
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        try {
            LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        } catch (Exception e) {
            e.printStackTrace();
        }
        mClusterManager = new ClusterManager<Person>(MainActivity.this, googleMap);
        mClusterManager.setRenderer(new MarkerClusterRenderer(MainActivity.this, mMap, mClusterManager));
        googleMap.setOnCameraIdleListener(mClusterManager);
        googleMap.setOnMarkerClickListener(mClusterManager);
        googleMap.setOnInfoWindowClickListener(mClusterManager);
        LatLng latLng = new LatLng(35.6833, 51.333332);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        mClusterManager.cluster();

        Person person = new Person(35.6833, 51.333332, "", "");
        mClusterManager.addItem(person);


        int serviceId = getSharedPreferences("me", Context.MODE_PRIVATE).getInt("service_id", 0);
        if (filter) {
            providerList.clear();
            getExpertsList(serviceId, maleBool, femaleBool, languages);
        } else
            getExpertsList(null, maleBool, femaleBool, languages);


    }


    /**
     * Description: This is the method
     * which shows the current location
     * icon in map layout of the application
     */


    private void enableMyLocationIfPermitted() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        } else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }

    }


    /**
     * Description: This is the method
     * which gets the location of user
     * by GPS
     */

    private void fetchLastLocation() {

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLocation = location;
                    SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                    supportMapFragment.getMapAsync(MainActivity.this);

                } else {
                    Toast.makeText(MainActivity.this, "Your GPS if Off !", Toast.LENGTH_SHORT).show();
                    currentLocation = new Location("current");
                    currentLocation.setLatitude(35.123);
                    currentLocation.setLongitude(50.123);
                }
            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResult) {
        switch (requestCode) {
            case 1:
                if (grantResult.length > 0 && grantResult[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLastLocation();
                    break;
                } else {
                    Toast.makeText(MainActivity.this, "Location permission missing", Toast.LENGTH_SHORT).show();
                    break;
                }
            case 3:
                startNewIntent(ContacsListActivity.class);
                break;
        }
    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.iv_menu:
                drawerLayout.openDrawer(Gravity.START);
                break;
            case R.id.iv_cancel:
//                filterResult();
                animateProfPage(null);
                break;
            case R.id.iv_prof:
                break;
            case R.id.tv_bottom_cat:
                animateProfPage(null);
                break;
            case R.id.rl_content:
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    closeDrawer();
                break;
            case R.id.rl_content1:
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    closeDrawer();
                break;
            case R.id.rl_message:
                startSlideUpIntent(ChatListActivity.class);
                break;
            case R.id.iv_addresses:
                startSlideUpIntent2(AddressFilterActivity.class);
                break;
            case R.id.iv_gender:
                animateProfPageUser(null);
//                startSlideUpIntent(UserFilterActivity.class);
                break;
            case R.id.nav_addresses:
                startNewIntent(AddressesActivity.class);
                break;
            case R.id.nav_invite:
//                if (getContactPermission())
                startNewIntent(InviteFriendActivity.class);
                break;
            case R.id.iv_current:
                getMyLocation();
                break;
            case R.id.nav_calendar:
                startNewIntent(CalendarActivity.class);
                break;
            case R.id.nav_business:
                startNewIntent(CreateBusinessProfileActivity.class);
                break;
            case R.id.rl_close:
                filterResult();
//                animateProfPageUser(null);
                break;
//            case R.id.tv_clear:
//                rMale.setChecked(false);
//                rFemale.setChecked(false);
//                break;
            case R.id.btn_filter:
                filterResult();
                break;
            case R.id.nav_lang:
                startNewIntent(SelectLanguageActivity.class);
                break;
            case R.id.nav_logout:
                SharedPreferences settings = getSharedPreferences("me",
                        Context.MODE_PRIVATE);
                settings.edit().putString("token", "0").apply();
                MainActivity.this.finish();
                break;
            case R.id.view:
                animateProfPage(null);
                break;
            case R.id.rl_view:
                animateProfPage(null);
                break;
            case R.id.tv_language:
                if (isCollapse)
                    expand(userFilterRecyclerView, 1);
                else
                    collapse(userFilterRecyclerView, 1);
                break;
            case R.id.l_language:
                if (isCollapse)
                    expand(userFilterRecyclerView, 1);
                else
                    collapse(userFilterRecyclerView, 1);
                break;
            case R.id.gender:
                if (isCollapse2)
                    expand(lGender, 2);
                else
                    collapse(lGender, 2);
                break;
            case R.id.l_gender2:
                if (isCollapse2)
                    expand(lGender, 2);
                else
                    collapse(lGender, 2);
                break;
            case R.id.r_male:
                if (maleBool) {
                    filterCount--;
                    maleBool = false;
                    Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/montserrat.otf");
                    rMale.setTypeface(tf, Typeface.NORMAL);
                    tvNum.setText(filterCount + "");
                } else {
                    filterCount++;
                    maleBool = true;
                    if (femaleBool) {
                        femaleBool = false;
                        filterCount--;
                    }
                    Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/montserrat.otf");
                    rMale.setTypeface(tf, Typeface.BOLD);
                    rFemale.setTypeface(tf, Typeface.NORMAL);
                    tvNum.setText(filterCount + "");
                }
                break;
            case R.id.r_female:
                if (femaleBool) {
                    filterCount--;
                    femaleBool = false;
                    Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/montserrat.otf");
                    rFemale.setTypeface(tf, Typeface.NORMAL);
                    tvNum.setText(filterCount + "");
                } else {
                    filterCount++;
                    femaleBool = true;
                    if (maleBool) {
                        maleBool = false;
                        filterCount--;
                    }
                    Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/montserrat.otf");
                    rFemale.setTypeface(tf, Typeface.BOLD);
                    rMale.setTypeface(tf, Typeface.NORMAL);
                    tvNum.setText(filterCount + "");
                }
                break;
            case R.id.iv_arrow:
                if (isCollapse)
                    expand(userFilterRecyclerView, 1);
                else
                    collapse(userFilterRecyclerView, 1);
                break;
            case R.id.iv_arrow2:
                if (isCollapse2)
                    expand(lGender, 2);
                else
                    collapse(lGender, 2);
                break;

        }
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     * then get the list of professions and
     * experts
     */

    private void initView() {


        locationButton = ((View) findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        locationButton.setVisibility(View.GONE);
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        rlp.setMargins(-2150, -2150, 0, 0);

        ivMenu = findViewById(R.id.iv_menu);
        txtFilter = findViewById(R.id.txt_filter);
        tvNum2 = findViewById(R.id.tv_num2);
        ivLoc = findViewById(R.id.iv_loc);
        rMale = findViewById(R.id.r_male);
        lGender2 = findViewById(R.id.l_gender2);
        tvEditInfo = findViewById(R.id.edit_info);
        rFemale = findViewById(R.id.r_female);
        navLogOut = findViewById(R.id.nav_logout);
        tvNavigatinoName = findViewById(R.id.navigation_name);
        tvBottomCat = findViewById(R.id.tv_bottom_cat);
        drawerLayout = findViewById(R.id.drawer_layout);
        lProfessions = findViewById(R.id.l_professions);
        rlBottom = findViewById(R.id.rl_bottom);
        rlContent = findViewById(R.id.rl_content1);
        rlMessage = findViewById(R.id.rl_message);
        rlUserFilter = findViewById(R.id.rl_user_filter);
        navLang = findViewById(R.id.nav_lang);
        nav_addresses = findViewById(R.id.nav_addresses);
        navInvite = findViewById(R.id.nav_invite);
        navBusiness = findViewById(R.id.nav_business);
        nvView = findViewById(R.id.nv_view);
        navCalendar = findViewById(R.id.nav_calendar);
        lLnaguage = findViewById(R.id.l_language);
        ivCancel = findViewById(R.id.iv_cancel);
        ivCurrent = findViewById(R.id.iv_current);
        ivGender = findViewById(R.id.iv_gender);
        ivAddresses = findViewById(R.id.iv_addresses);
        tvSpinner = findViewById(R.id.spinner);
        tvGender = findViewById(R.id.gender);
        ivArrow = findViewById(R.id.iv_arrow);
        ivArrow2 = findViewById(R.id.iv_arrow2);
        view = findViewById(R.id.view);
        rlView = findViewById(R.id.rl_view);
        tvLanguage = findViewById(R.id.tv_language);
        tvNum = findViewById(R.id.tv_num);
        lGender = findViewById(R.id.l_gender);
        ProvidersRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        professionsRecyclerView = (RecyclerView) findViewById(R.id.recycler_view2);


        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/monstserratlight.otf");
//        tvNavigatinoName.setTypeface(tf);
        tvEditInfo.setTypeface(tf);


        Typeface tf3 = Typeface.createFromAsset(getAssets(), "fonts/montserrat2.otf");
        txtFilter.setTypeface(tf3);

        ivMenu.setOnClickListener(this);
        tvSpinner.setOnClickListener(this);
        ivCancel.setOnClickListener(this);
        rlBottom.setOnClickListener(this);
        rlContent.setOnClickListener(this);
        rlMessage.setOnClickListener(this);
        ivAddresses.setOnClickListener(this);
        ivGender.setOnClickListener(this);
        tvBottomCat.setOnClickListener(this);
        nav_addresses.setOnClickListener(this);
        navInvite.setOnClickListener(this);
        ivCurrent.setOnClickListener(this);
        navCalendar.setOnClickListener(this);
        navBusiness.setOnClickListener(this);
        navLang.setOnClickListener(this);
        navLogOut.setOnClickListener(this);
        view.setOnClickListener(this);
        rlView.setOnClickListener(this);
        tvLanguage.setOnClickListener(this);
        ivArrow.setOnClickListener(this);
        ivArrow2.setOnClickListener(this);
        lGender.setOnClickListener(this);
        tvGender.setOnClickListener(this);
        rMale.setOnClickListener(this);
        rFemale.setOnClickListener(this);
        lLnaguage.setOnClickListener(this);
        lGender2.setOnClickListener(this);


        Typeface tf2 = Typeface.createFromAsset(getAssets(), "fonts/montserrat2.otf");
        tvBottomCat.setTypeface(tf2);

        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));


        setListeners();
        getSpinnerLocation();
        getTheServicesList();


        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, null, R.string.common_open_on_phone, R.string.app_name) {
            @SuppressLint("NewApi")
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float moveFactor = (nvView.getWidth() * slideOffset);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    rlContent.setTranslationX(moveFactor);
                } else {
                    TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    rlContent.startAnimation(anim);

                    lastTranslate = moveFactor;
                }
            }
        };


        drawerLayout.setDrawerListener(mDrawerToggle);


        //user filter page
        rlClose = findViewById(R.id.rl_close);
        tvClear = findViewById(R.id.tv_clear);
        rMale = findViewById(R.id.r_male);
        rFemale = findViewById(R.id.r_female);
        btnFilter = findViewById(R.id.btn_filter);
        userFilterRecyclerView = findViewById(R.id.recycler_view_user);

        setRecyclerViewStuffUser();


        collapse(lGender, 2);

        rlClose.setOnClickListener(this);
        tvClear.setOnClickListener(this);
        btnFilter.setOnClickListener(this);


    }


    /**
     * Description: This is the method
     * which executes to decide if
     * 'fade in' should run or 'fade out'
     */

    private void animateProfPage(Integer id) {

        if (id != null)
            getExpertsList(id, maleBool, femaleBool, languages);
        if (lProfessions.getVisibility() == View.VISIBLE) {
            slideDown(lProfessions);
            ivLoc.setVisibility(View.VISIBLE);
        } else {
            slideUp(lProfessions);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ivLoc.setVisibility(View.GONE);
                }
            }, 300);

        }
    }


    private void animateProfPageUser(Integer id) {

        if (id != null)
            getExpertsList(id, maleBool, femaleBool, languages);
        if (rlUserFilter.getVisibility() == View.VISIBLE) {
            slideDown(rlUserFilter);
            ivLoc.setVisibility(View.VISIBLE);
        } else {
            slideUp(rlUserFilter);
            ivLoc.setVisibility(View.GONE);
        }
    }


    private void setListeners() {
        listener = new ExpertRecyclerViewListener() {
            @Override
            public void onClick(View view, final int position, final ExpertModel model, String type) {
                if (canClick) {
                    Intent i = new Intent(MainActivity.this, ExpertInfoActivity.class);
                    String transitionName = getString(R.string.animation);
                    ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, view, transitionName);
                    i.putExtra("name", model.getName());
                    i.putExtra("pic", model.getPic());
                    i.putExtra("price", model.getPrice());
                    i.putExtra("rate", model.getRate());
                    i.putExtra("time", model.getTime());
                    i.putExtra("job", model.getJob());
                    i.putExtra("des", model.getDes());
                    i.putExtra("web", model.getWeb());
                    i.putExtra("insta", model.getInsta());
                    i.putExtra("id", model.getId());
                    i.putExtra("user_id", model.getUserId());
                    i.putExtra("order", model.getOrders());
                    i.putExtra("quality", model.getQuality());
                    i.putExtra("price_rate", model.getPriceRate());
                    i.putExtra("recommend", model.getRecommend());
                    startActivity(i, transitionActivityOptions.toBundle());
                    canClick = false;
                }


            }

        };

        professionslistener = new ProfessionsRecyclerViewListener() {
            @Override
            public void onClick(View view, final int position, final ProfessionModel model, String type, LinearLayout layout) {
                providerList.clear();
                for (int i = 0; i < professionsModelsList.size(); i++)
                    professionsModelsList.get(i).setClicked(false);
                model.setClicked(true);
                SharedPreferences settings = getSharedPreferences("me",
                        Context.MODE_PRIVATE);
                settings.edit().putInt("service_id", model.getId());
                serviceId = model.getId();
                professionsAdapter.notifyDataSetChanged();
                tvSpinner.setText(model.getJobTitle());
                tvBottomCat.setText(model.getJobTitle());
                animateProfPage(model.getId());
            }

        };
    }


    /**
     * Description: This is the method
     * which gets the list of professions
     * and load them into our profession list
     */


    private void getTheServicesList() {

        professionsAdapter = new ProfessionListAdapter(professionsModelsList, MainActivity.this, professionslistener, 1);
        professionsRecyclerView.setAdapter(professionsAdapter);
        professionsAdapter.notifyDataSetChanged();


        RServices.Fire(new RetrofitHandler.apiResponseHandler<ServicesData>() {
            @Override
            public void onSuccess(ServicesData servicesData) {

                tvSpinner.setText(servicesData.getList().get(0).getTitle());
                ProfessionModel modeli = new ProfessionModel();
                modeli.setClicked(true);
                modeli.setId(0);
                modeli.setJobTitle("All Services");
                professionsModelsList.add(modeli);
                for (int i = 0; i < servicesData.getList().size(); i++) {
                    ProfessionModel model = new ProfessionModel();
                    model.setJobTitle(servicesData.getList().get(i).getTitle());
                    model.setId(servicesData.getList().get(i).getId());

                    professionsModelsList.add(model);
                }

                professionsAdapter = new ProfessionListAdapter(professionsModelsList, MainActivity.this, professionslistener, 1);
                professionsRecyclerView.setAdapter(professionsAdapter);
                professionsAdapter.notifyDataSetChanged();

                ProvidersRecyclerView.animate().translationX(0);

                tvNum2.setText((professionsModelsList.size() - 1) + "");

                getLanguages();
            }

            @Override
            public void onFail(int code, String message) {
                Toast.makeText(MainActivity.this, "Error with code: " + code, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(MainActivity.this, "Failure", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onException(Exception e) {
                Toast.makeText(MainActivity.this, "Exception e", Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * Description: This is the method
     * which gets the list of providers
     * and load them into our provider list
     */

    private void getExpertsList(Integer serviceId, boolean male, boolean female, ArrayList<Integer> list) {
        try {
            dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.waiting_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        setRecyclerViewStuff();
        String url = "provider/";
        if (serviceId != null) {
            if (serviceId != 0)
                url = url + "?service=" + serviceId;

        }
        if (male && !female) {
            if (url.contains("?"))
                url = url + "&gender=m";
            else
                url = url + "?gender=m";
        } else if (female && !male) {
            if (url.contains("?"))
                url = url + "&gender=f";
            else
                url = url + "?gender=f";
        }
        if (list == null)
            list = new ArrayList<>();
        if (list.size() != 0) {
            if (url.contains("?"))
                url = url + "&language=" + list.get(0);
            else
                url = url + "?language=" + list.get(0);
        }
        RExpertList.Fire(url, new RetrofitHandler.apiResponseHandler<ExpertData>() {
            @Override
            public void onSuccess(ExpertData expertData) {
                dialog.dismiss();
                addExpertsToList(expertData);
            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
                int a = 2;
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
                int a = 2;
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
                int a = 2;
            }
        });

    }


    /**
     * Description: This is the method
     * which checks the location permissions
     * and take them from user if needed
     */


    private void locationPermissions() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            return;
        }


        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
    }


    /**
     * Description: This is the method
     * which sets the recyclerview stuffs
     * its listener and layout manager and ...
     */

    private void setRecyclerViewStuff() {

//        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        ProvidersRecyclerView.setLayoutManager(layoutManager);
        layoutManager2 = new LinearLayoutManager(this);
        professionsRecyclerView.setHasFixedSize(true);
        professionsRecyclerView.setLayoutManager(layoutManager2);


        mAdapter = new ExpertListAdapter(providerList, MainActivity.this, listener, 1);
        ProvidersRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

    }


    /**
     * Description: This is the method
     * which get the exact coordination
     * of spinner in layout for
     * using in animation methods
     */

    private void getSpinnerLocation() {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tvSpinner.getLocationInWindow(location);
            }
        }, 700);
    }

    private void slideDown(View view) {
        //Load animation
        Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down3);


        view.startAnimation(slide_down);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.setVisibility(View.INVISIBLE);
                rlBottom.setVisibility(View.VISIBLE);
            }
        }, 250);
    }

    private void slideUp(View view) {
        //Load animation
        Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);


// Start animation
        view.startAnimation(slide_up);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.setVisibility(View.VISIBLE);
                rlBottom.setVisibility(View.INVISIBLE);
            }
        }, 250);
    }


    private void addExpertsToList(ExpertData expertData) {

        mMap.clear();
        if (expertData.getList() != null) {
            for (int i = 0; i < expertData.getList().size(); i++) {
                ExpertModel model = new ExpertModel();
                model.setName(expertData.getList().get(i).getUser().getFirst_name() + " " + expertData.getList().get(i).getUser().getLast_name());
                model.setPrice(expertData.getList().get(i).getOrder_count());
                model.setTime(expertData.getList().get(i).getService().getTitle());
                model.setPic(expertData.getList().get(i).getUser().getAvatar());
                model.setOrders(expertData.getList().get(i).getOrder_count());
                model.setPriceRate(expertData.getList().get(i).getRate());
                model.setQuality(expertData.getList().get(i).getQuality());
                model.setRecommend(expertData.getList().get(i).getLikes());
                model.setDes(expertData.getList().get(i).getDescription());
                model.setId(expertData.getList().get(i).getId());
                model.setUserId(expertData.getList().get(i).getUser().getId());
                model.setJob(expertData.getList().get(i).getService().getTitle());
                model.setRate(expertData.getList().get(i).getRate());
//                model.setInsta(expertData.getList().get(i).getLikes());


                if (expertData.getList().get(i).getCybers() != null) {
                    for (int k = 0; k < expertData.getList().get(i).getCybers().size(); k++) {
                        if (expertData.getList().get(i).getCybers().get(k).getKind().contains("w"))
                            model.setWeb(expertData.getList().get(i).getCybers().get(k).getLink());
                        else if (expertData.getList().get(i).getCybers().get(k).getKind().contains("i"))
                            model.setInsta(expertData.getList().get(i).getCybers().get(k).getLink());

                    }
                }

                Marker mPositionMarker;

                if (expertData.getList().get(i).getLocations() != null) {
                    for (int j = 0; j < expertData.getList().get(i).getLocations().size(); j++)



                    mPositionMarker = mMap.addMarker(new MarkerOptions()
                            .flat(true)
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.mipmap.location))
                            .anchor(0.5f, 0.5f)
                            .position(
                                    new LatLng(expertData.getList().get(i).getLocations().get(j).getLatitude(),
                                            expertData.getList().get(i).getLocations().get(j).getLongitude())));
//                        mClusterManager.addItem(new Person(expertData.getList().get(i).getLocations().get(j).getLatitude()
//                                , expertData.getList().get(i).getLocations().get(j).getLongitude(), ""
//                                , ""));
                }

                providerList.add(model);

            }

            mClusterManager.cluster();// it will draw what's been previsouly added.

            mAdapter = new ExpertListAdapter(providerList, MainActivity.this, listener, 1);
            ProvidersRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void closeDrawer() {
        Toast.makeText(this, "ok", Toast.LENGTH_SHORT).show();
        rlContent.animate().translationX(0).setDuration(300);
        drawerLayout.closeDrawer(GravityCompat.START);
    }


    private void startSlideUpIntent(Class myClass) {
        Intent intent = new Intent(this, myClass);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
    }


    private void startSlideUpIntent2(Class myClass) {
        Intent intent = new Intent(this, myClass);
        startActivityForResult(intent, 1);
        overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
    }


    private void startNewIntent(Class myClass) {
        Intent intent = new Intent(MainActivity.this, myClass);
        startActivity(intent);
    }


    private boolean getContactPermission() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, 3);
            return false;
        } else
            return true;
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == REQUEST_CHECK_SETTINGS) {

            if (resultCode == RESULT_OK) {

                LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//                Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//                double longitude = location.getLongitude();
//                double latitude = location.getLatitude();
//
//                LatLng latLng = new LatLng(latitude, longitude);
//                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ivCurrent.performClick();
                    }
                }, 1000);

                Toast.makeText(getApplicationContext(), "GPS enabled", Toast.LENGTH_LONG).show();
            } else {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        LatLng latLng = new LatLng(35.6833, 51.333332);
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                    }
                }, 1000);


                Toast.makeText(getApplicationContext(), "GPS is not enabled", Toast.LENGTH_LONG).show();
            }

        }

        if (resultCode == RESULT_OK || true) {

            if (resultCode == RESULT_OK || true) {
                double lat = data.getDoubleExtra("lat", 0);
                double lon = data.getDoubleExtra("lon", 0);
                if (lat != 0) {
                    LatLng latLng = new LatLng(lat, lon);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                }
            } else {
                switch (requestCode) {
                    case 3:
                        Cursor cursor = null;
                        try {
                            String phoneNo = null;
                            String name = null;

                            Uri uri = data.getData();
                            cursor = getContentResolver().query(uri, null, null, null, null);
                            cursor.moveToFirst();
                            int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                            phoneNo = cursor.getString(phoneIndex);
                            name = cursor.getString(nameIndex);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        }
    }


    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        currentLocation = location;
        if (mGoogleApiClient != null) {

            Marker mPositionMarker;
            mPositionMarker = mMap.addMarker(new MarkerOptions()
                    .flat(true)
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.mipmap.loclogo))
                    .anchor(0.5f, 0.5f)
                    .position(
                            new LatLng(location.getLatitude(), location
                                    .getLongitude())));

            animateMarker(mPositionMarker, location); // Helper method for smooth
            // animation

            mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location
                    .getLatitude(), location.getLongitude())));

//            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    private void getMyLocation() {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (statusOfGPS)
            locationButton.performClick();
        else
        {
            Snackbar snackbar = Snackbar
                    .make(rlBottom, "GPS is off!", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        filter = getIntent().getBooleanExtra("filter", false);
        maleBool = getIntent().getBooleanExtra("male", false);
        femaleBool = getIntent().getBooleanExtra("female", false);
        languagesList = getIntent().getIntegerArrayListExtra("language");
        canClick = true;


    }


    private void getLanguages() {
        RLanguages.Fire(new RetrofitHandler.apiResponseHandler<LanguageData>() {
            @Override
            public void onSuccess(LanguageData languageData) {

                for (int i = 0; i < languageData.getList().size(); i++) {
                    UserFilterModel model = new UserFilterModel();
                    model.setLanguage(languageData.getList().get(i).getTitle());
                    model.setId(languageData.getList().get(i).getId());
                    model.setClicked(false);
                    userFilterModelsList.add(model);
                }
                userFilterAdapter.notifyDataSetChanged();

                collapse(userFilterRecyclerView, 1);

            }

            @Override
            public void onFail(int code, String message) {
                int a = 1;
            }

            @Override
            public void onFailure(Throwable t) {
                int a = 1;
            }

            @Override
            public void onException(Exception e) {
                int a = 1;
            }
        });
    }


    private void setRecyclerViewStuffUser() {

        userFilterlistener = new UserFilterRecyclerViewListener() {
            @Override
            public void onClick(View view, final int position, final UserFilterModel model, String type) {
                for (int i = 0; i < userFilterModelsList.size(); i++) {
                    if (i != position) {
                        if (userFilterModelsList.get(i).isClicked())
                            filterCount--;
                        userFilterModelsList.get(i).setClicked(false);
                        userFilterAdapter.notifyItemChanged(i);
                    }
                }
                if (!model.isClicked()) {
                    model.setClicked(true);
                } else {
                    model.setClicked(false);
                    filterCount--;
                }
                int y = 0;
                for (int i = 0; i < userFilterModelsList.size(); i++) {
                    if (userFilterModelsList.get(i).isClicked())
                        y++;
                }
                userFilterAdapter.notifyItemChanged(position);
                if (isFirest) {
                    isFirest = false;
                    if (y != 0)
                        filterCount += 1;
                } else if (y != 0)
                    filterCount++;
                tvNum.setText(filterCount + "");

            }

        };


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        userFilterRecyclerView.setLayoutManager(layoutManager);
        userFilterAdapter = new UserFilterAdapter(userFilterModelsList, MainActivity.this, userFilterlistener, 1);
        userFilterRecyclerView.setAdapter(userFilterAdapter);
        userFilterAdapter.notifyDataSetChanged();

    }

    private void filterResult() {
        providerList.clear();
        languages.clear();
        for (int j = 0; j < userFilterModelsList.size(); j++) {
            if (userFilterModelsList.get(j).isClicked())
                languages.add(userFilterModelsList.get(j).getId());
        }
        if (serviceId != 0)
            getExpertsList(serviceId, maleBool, femaleBool, languages);
        else
            getExpertsList(null, maleBool, femaleBool, languages);
        slideDown(rlUserFilter);
    }


    private void getMyInfo() {

        RMyInfo.Fire(new RetrofitHandler.apiResponseHandler<MyInfoData>() {
            @Override
            public void onSuccess(MyInfoData myInfoData) {
                SharedPreferences settings = getSharedPreferences("me",
                        Context.MODE_PRIVATE);
                settings.edit().putString("user_name", myInfoData.getFirst_name() + " " +
                        myInfoData.getLast_name()).apply();
                settings.edit().putInt("user_id", myInfoData.getId()).apply();
                tvNavigatinoName.setText(myInfoData.getFirst_name() + " " + myInfoData.getLast_name());
                getFireBaseToken();

            }

            @Override
            public void onFail(int code, String message) {

            }

            @Override
            public void onFailure(Throwable t) {

            }

            @Override
            public void onException(Exception e) {

            }
        });
    }




    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        builder.build()
                );

        result.setResultCallback(this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull Result result) {
        final Status status = result.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:

                // NO need to show the dialog;

                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location settings are not satisfied. Show the user a dialog

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().

                    status.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);

                } catch (IntentSender.SendIntentException e) {

                    //failed to show
                }
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }
    }


    public void animateMarker(final Marker marker, final Location location) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final LatLng startLatLng = marker.getPosition();
        final double startRotation = marker.getRotation();
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);

                double lng = t * location.getLongitude() + (1 - t)
                        * startLatLng.longitude;
                double lat = t * location.getLatitude() + (1 - t)
                        * startLatLng.latitude;

                float rotation = (float) (t * location.getBearing() + (1 - t)
                        * startRotation);

                marker.setPosition(new LatLng(lat, lng));
                marker.setRotation(rotation);

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }


    public void expand(final View v, int type) {
        if (type == 1) {
            isCollapse = false;
            ivArrow.setRotation(180);
        } else {
            isCollapse2 = false;
            ivArrow2.setRotation(180);
        }
        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Expansion speed of 1dp/ms
//        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        a.setDuration(200);
        v.startAnimation(a);
    }

    public void collapse(final View v, int type) {
        if (type == 1) {
            ivArrow.setRotation(0);
            isCollapse = true;
        } else {
            ivArrow2.setRotation(0);
            isCollapse2 = true;
        }
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Collapse speed of 1dp/ms
//        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        a.setDuration(200);
        v.startAnimation(a);
    }

    private void getFireBaseToken()
    {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }

                        // Get new Instance ID token
                        String fireBaseToken = task.getResult().getToken();
                        int a = 2;

                    }
                });
    }

}
