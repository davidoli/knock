package com.knock.knocked.activities;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.knock.knocked.G;
import com.knock.knocked.R;
import com.knock.knocked.models.ExpertInData;
import com.knock.knocked.rest.RSendOrder;
import com.knock.knocked.rest.RetrofitHandler2;
import com.knock.knocked.rest.models.SendOrderInData;

import okhttp3.ResponseBody;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * Description: This is expert info activity
 * which shows the details information of
 * the service provider
 */

public class ExpertInfoActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivBack;
    /**
     * Icon for showing distance in first layer in depth
     */
    private ImageView ivDistance;
    /**
     * Profile Image in first layer in depth
     */
    private ImageView ivProf1;
    /**
     * Profile Image in third layer in depth
     */
    private ImageView ivProf3;
    /**
     * Profile Image in second layer in depth
     */
    private ImageView ivProf2;
    /**
     * Cancel Icon in Green layer
     * which pops up
     */
    private ImageView ivCancel;
    /**
     * First Radio Button Icon in
     * Green layout which pops up
     */
    private ImageView ivRadio;
    /**
     * Second Radio Button Icon in
     * Green layout which pops up
     */
    private ImageView ivRaido2;
    /**
     * Plus Icon in
     * Green layout which pops up
     */
    private ImageView ivPlus;
    /**
     * Blue rectangle at bottom of
     * Green layout which pops up
     */
    private LinearLayout lBottom;
    /**
     * radio button shape
     * of the review page for user
     */
    private LinearLayout lReview;


    /**
     * Booking layout(radio and text) of the
     * Green layout which pops up
     */
    private LinearLayout lBooking;
    /**
     * Small layout at right down
     * side of the page to offer
     */
    private RelativeLayout rlBottom;
    /**
     * Green layout which pops up
     */
    private RelativeLayout rlBottom2;
    /**
     * Icon for showing the distance
     */
    private TextView tvDistance;
    /**
     * Text view which shows the
     * fist line of info text about
     * provider
     */
    private TextView tvAbout;
    /**
     * Text view which shows the
     * second line of info text about
     * provider
     */
    private TextView tvAbout2;
    /**
     * Text view which shows the
     * third line of info text about
     * provider
     */
    private TextView tvAbout3;
    /**
     * Name of provider in first layer
     * in depth
     */
    private TextView tvName;
    /**
     * Name of provider in second layer
     * in depth
     */
    private TextView tvName2;
    /**
     * Name of provider in third layer
     * in depth
     */
    private TextView tvName3;
    /**
     * Job des of provider in first layer
     * in depth
     */
    private TextView tvJob;
    /**
     * Job des of provider in third layer
     * in depth
     */
    private TextView tvJob3;
    /**
     * First Text view for radio in
     * Green layout which pops up
     */
    private TextView  tvRadio;
    /**
     * Second Text view for radio in
     * Green layout which pops up
     */
    private TextView tvRadio2;
    /**
     * Plus Icon Text view for radio in
     * Green layout which pops up
     */
    private TextView  tvPlus;
    /**
     * Flag for deciding opening chat page
     * or pops down the green layer
     */
    private boolean chatControl;
    private LinearLayout lRate,lWeb,lInsta;
    private String des,name,web,insta,pic,job;
    private int price,id,priceRate,quality,orders,recommend,userId;
    private TextView tvPrice,tvRecom,tvOrders,tvWeb,tvInsta,txtAbout;
    private ImageView pStar1,pStar2,pStar3,qStar1,qStar2,qStar3;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expert_info);
        initView();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     */

    private void initView() {

        des = getIntent().getStringExtra("des");
        job = getIntent().getStringExtra("job");
        name = getIntent().getStringExtra("name");
        web = getIntent().getStringExtra("web");
        insta = getIntent().getStringExtra("insta");
        pic = getIntent().getStringExtra("pic");
        price = getIntent().getIntExtra("price",0);
        id = getIntent().getIntExtra("id",0);
        userId = getIntent().getIntExtra("user_id",0);
        priceRate = getIntent().getIntExtra("price_rate",0);
        quality = getIntent().getIntExtra("quality",0);
        orders = getIntent().getIntExtra("order",0);
        recommend = getIntent().getIntExtra("recommend",0);

        ivBack = findViewById(R.id.iv_back);
        tvPrice = findViewById(R.id.tv_price);
        ivRadio = findViewById(R.id.iv_radio);
        ivRaido2 = findViewById(R.id.iv_radio2);
        ivPlus = findViewById(R.id.iv_plus);
        pStar1 = findViewById(R.id.p_star_1);
        pStar2 = findViewById(R.id.p_star_2);
        pStar3 = findViewById(R.id.p_star_3);
        qStar1 = findViewById(R.id.q_star_1);
        qStar2 = findViewById(R.id.q_star_2);
        txtAbout = findViewById(R.id.txt_about);
        qStar3 = findViewById(R.id.q_star_3);
        ivDistance = findViewById(R.id.iv_distance);
        tvAbout = findViewById(R.id.tv_about);
        tvAbout3 = findViewById(R.id.tv_about3);
        tvAbout2 = findViewById(R.id.tv_job2);
        tvName2 = findViewById(R.id.tv_name2);
        tvWeb = findViewById(R.id.tv_web);
        tvInsta = findViewById(R.id.tv_insta);
        tvName3 = findViewById(R.id.tv_name3);
        tvRadio = findViewById(R.id.tv_radio);
        tvRadio2 = findViewById(R.id.tv_radio2);
        tvPlus = findViewById(R.id.tv_plus);
        tvDistance = findViewById(R.id.tv_distance);
        tvJob = findViewById(R.id.tv_job);
        tvJob3 = findViewById(R.id.tv_job3);
        tvRecom = findViewById(R.id.tv_recom);
        tvOrders= findViewById(R.id.tv_orders);
        tvName = findViewById(R.id.tv_name);
        tvName3 = findViewById(R.id.tv_name3);
        lBottom = findViewById(R.id.l_bottom);
        lReview = findViewById(R.id.l_review);
        rlBottom = findViewById(R.id.rl_bottom);
        rlBottom2 = findViewById(R.id.rl_bottom2);
        ivProf1 = findViewById(R.id.iv_prof);
        ivProf2 = findViewById(R.id.iv_prof2);
        ivProf3 = findViewById(R.id.iv_prof3);
        ivCancel = findViewById(R.id.iv_cancel);
        lBooking = findViewById(R.id.l_booking);
        lRate = findViewById(R.id.l_rate);
        lWeb = findViewById(R.id.l_web);
        lInsta = findViewById(R.id.l_insta);


        ivBack.setOnClickListener(this);
        rlBottom.setOnClickListener(this);
        ivCancel.setOnClickListener(this);
        lBooking.setOnClickListener(this);
        lRate.setOnClickListener(this);
        lReview.setOnClickListener(this);
        lInsta.setOnClickListener(this);
        lWeb.setOnClickListener(this);

        tvName2.setText(name);
        tvName.setText(name);
        tvName3.setText(name);
//        tvAbout3.setText(des);
//        tvAbout.setText(des);
//        tvAbout2.setText(des);
        tvJob3.setText(des);
        tvJob.setText(des);
        tvPrice.setText("$"+price);
        tvRecom.setText(recommend+"");
        tvOrders.setText(orders+"");
        tvAbout2.setText(job);

        Glide.with(ExpertInfoActivity.this)
                .load(pic)  //http://karmento.ir      http://karmento.ir
                .placeholder(R.mipmap.profile)
                .dontAnimate()
                .error(R.mipmap.profile)//in case of any glide exception or not able to download then this image will be appear . if you won't mention this error() then nothing to worry placeHolder image would be remain as it is.
                .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                .fitCenter()
                .into(ivProf3);

//        quality;
//        priceRate

        if(quality<3)
            qStar3.setImageResource(R.mipmap.star1);
        if(quality<2)
            qStar2.setImageResource(R.mipmap.star1);
        if(quality<1)
            qStar1.setImageResource(R.mipmap.star1);

        if(priceRate<3)
            pStar3.setImageResource(R.mipmap.star1);
        if(priceRate<2)
            pStar2.setImageResource(R.mipmap.star1);
        if(priceRate<1)
            pStar1.setImageResource(R.mipmap.star1);


        animatePage();



        Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/montserrat2.otf");
        tvInsta.setTypeface(tf);
        tvWeb.setTypeface(tf);


        Typeface tf3 = Typeface.createFromAsset(getAssets(),"fonts/montserratsemi.otf");
        tvName3.setTypeface(tf3);
        tvName.setTypeface(tf3);
        tvName2.setTypeface(tf3);
        txtAbout.setTypeface(tf3);



        Typeface tf2 = Typeface.createFromAsset(getAssets(),"fonts/montserratm.otf");
//        tvJob.setTypeface(tf2);
//        tvAbout2.setTypeface(tf2);
//        tvAbout.setTypeface(tf2);



    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.rl_bottom:
                if (!chatControl)
                    startNewIntent();
                else
                {
                    Intent intent = new Intent(ExpertInfoActivity.this, ChatBoxActivity.class);
                    intent.putExtra("receiver_id",userId);
                    intent.putExtra("name",name);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }

                break;
            case R.id.l_booking:
                startChat();
                break;
            case R.id.iv_cancel:
                backToExpertInfo();
                break;
            case R.id.l_rate:
                Intent intent2 = new Intent(ExpertInfoActivity.this, RateAppActivity.class);
                intent2.putExtra("id",id);
                startActivity(intent2);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.l_review:
                Intent intent3 = new Intent(ExpertInfoActivity.this, ReviewActivity.class);
                intent3.putExtra("name",tvName3.getText().toString().trim());
                intent3.putExtra("provider_id",id);
                startActivity(intent3);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.l_web:
                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(web));
                    startActivity(browserIntent);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    Toast.makeText(this, "No Website founded", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.l_insta:
                try {
                    Intent browserIntent2 = new Intent(Intent.ACTION_VIEW, Uri.parse(insta));
                    startActivity(browserIntent2);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    Toast.makeText(this, "No Account founded", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    /**
     * Description: This is the method
     * which pops up the green layer
     */

    private void startNewIntent() {
        chatControl = true;
        rlBottom2.setBackgroundResource(R.color.rlbottom);
        rlBottom2.animate().translationY(0).setDuration(400);
        rlBottom2.animate().translationX(0).setDuration(400);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ivProf1.animate().translationY(200);
                ivProf3.animate().translationY(20);
                ivProf2.animate().translationY(0);


//                tvName3.animate().translationY(20).setDuration(500);
                tvAbout3.animate().translationY(20).setDuration(500);
                tvJob3.animate().translationY(20).setDuration(500);
                txtAbout.animate().translationY(20).setDuration(500);


                tvName2.animate().translationY(0).setDuration(500);
                tvAbout2.animate().translationY(0).setDuration(500);
                tvRadio.animate().translationY(0).setDuration(500);
                tvRadio2.animate().translationY(0).setDuration(800);
                tvPlus.animate().translationY(0).setDuration(1000);
                ivRadio.animate().translationY(0).setDuration(500);
                ivRaido2.animate().translationY(0).setDuration(800);
                ivPlus.animate().translationY(0).setDuration(1000);

            }
        }, 300);

    }



    /**
     * Description: This is the method
     * which pops down the green layer
     */

    private void backToExpertInfo() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ivProf2.setVisibility(View.INVISIBLE);
            }
        }, 200);

        ivProf3.animate().translationY(0);
        tvJob3.animate().translationY(0);
        txtAbout.animate().translationY(0);
        tvAbout3.animate().translationY(0);

        tvRadio.animate().translationY(30);
        tvRadio2.animate().translationY(30);
        tvPlus.animate().translationY(30);
        ivRadio.animate().translationY(30);
        ivRaido2.animate().translationY(30);
        ivPlus.animate().translationY(30);

        rlBottom2.animate().translationY(2040);
        rlBottom2.animate().translationX(685);
        chatControl = false;
    }


    /**
     * Description: This is the method
     * which starts the chat list activity page
     */

    private void startChat()
    {

        Intent intent = new Intent(this,BookingActivity.class);
        intent.putExtra("provider_id",id);
        intent.putExtra("name",name);
        intent.putExtra("user_id",userId);
        startActivity(intent);
        overridePendingTransitionEnter();
    }



    /**
     * Description: This is the method
     * which runs entering the chat list
     * page animation
     */

    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }



    /**
     * Description: This is the method
     * which defines all the animations
     * for the layout and widgets inside it
     */

    private void animatePage()
    {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                lBottom.animate().translationY(0);

            }
        }, 100);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rlBottom.animate().translationY(0);

            }
        }, 400);


        Handler handler1 = new Handler();
        handler1.postDelayed(new Runnable() {
            @Override
            public void run() {
                tvName.animate().translationY(0);
                tvAbout.animate().translationY(0);
                tvJob.animate().translationY(0);
                tvDistance.animate().translationY(0);
                ivDistance.animate().translationY(0);

                tvName3.animate().translationY(0);
                tvAbout3.animate().translationY(0);
                tvJob3.animate().translationY(0);
                txtAbout.animate().translationY(0);
            }
        }, 200);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        rlBottom2.setBackgroundResource(R.color.transparent);
        rlBottom2.animate().translationY(2040);
        rlBottom2.animate().translationX(685);

    }


}