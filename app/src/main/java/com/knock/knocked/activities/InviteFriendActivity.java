package com.knock.knocked.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.knock.knocked.R;
import com.knock.knocked.models.ResendData;
import com.knock.knocked.rest.RInviteFriend;
import com.knock.knocked.rest.RetrofitHandler;

import java.util.Objects;

import okhttp3.ResponseBody;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class InviteFriendActivity extends BaseActivity implements View.OnClickListener {

    private ImageView ivBack;
    private RelativeLayout rlMessage,rlLayout;
    private Button btnSend;
    private ImageView ivContact;
    private TextInputEditText etPhone;
    private TextInputLayout tlPhone;
    private RelativeLayout rlLogin;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_invite_friend);

//        btnSend = findViewById(R.id.btn_send);
        ivBack = findViewById(R.id.iv_back);
        ivContact = findViewById(R.id.iv_contact);
        etPhone = findViewById(R.id.et_phone);
        rlMessage = findViewById(R.id.rl_message);
        tlPhone = findViewById(R.id.tl_phone);
        rlLayout = findViewById(R.id.rl_layout);
        rlLogin = findViewById(R.id.rl_login);


        ivBack.setOnClickListener(this);
        rlLogin.setOnClickListener(this);
        ivContact.setOnClickListener(this);
        rlLayout.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                super.onBackPressed();
                break;
            case R.id.rl_login:
                tlPhone.setError(null);
                if (etPhone.getText().toString().length() >= 11)
                    inviteFriend();
                else
                    tlPhone.setError("Fill it correctly");
                break;
            case R.id.iv_contact:
                @SuppressLint("IntentReset")
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, 101);
                break;
            case R.id.rl_layout:
                hideKeyboard(InviteFriendActivity.this);
                break;


        }
    }


    @SuppressLint("Recycle")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == 101) && (resultCode == RESULT_OK)) {
            Cursor cursor = null;
            try {
                Uri uri = data.getData();
                cursor = getContentResolver().query(Objects.requireNonNull(uri), new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER}, null, null, null);
                if (cursor != null && cursor.moveToNext()) {
                    String phone = cursor.getString(0);
                    etPhone.setText(phone);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void inviteFriend() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.done_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        ResendData data = new ResendData();
        data.setCellphone(etPhone.getText().toString().trim());
        RInviteFriend.Fire(data, new RetrofitHandler.apiResponseHandler<ResponseBody>() {
            @Override
            public void onSuccess(ResponseBody responseBody) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        InviteFriendActivity.super.onBackPressed();
                        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                        InviteFriendActivity.this.finish();
                        dialog.dismiss();
                    }
                },1800);
            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }

}
