package com.knock.knocked.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.knock.knocked.R;
import com.knock.knocked.adapters.AddressFilterAdapter;
import com.knock.knocked.listeners.AddressFilterRecyclerViewListener;
import com.knock.knocked.models.AddressFilterModel;
import com.knock.knocked.models.AddressModel;
import com.knock.knocked.models.AddressOutData;
import com.knock.knocked.models.LoginOutData;
import com.knock.knocked.rest.RAddresses;
import com.knock.knocked.rest.RDeleteAddress;
import com.knock.knocked.rest.RetrofitHandler;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddressFilterActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout rlClose;
    /**
     * RecyclerView list for professions list
     */
    private RecyclerView addressFilterRecyclerView;
    /**
     * Adapter for professions list
     */
    private RecyclerView.Adapter addressFilterAdapter;
    /**
     * Listener for click action ( click on profession list items )
     */
    private AddressFilterRecyclerViewListener addressFilterlistener;
    /**
     * Array list for professions
     */
    private ArrayList<AddressFilterModel> addressFilterModelsList = new ArrayList<>();
    private double lat,lon;
    private RelativeLayout rlAddress;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_filter);
        initView();
        setRecyclerViewStuff();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     */

    private void initView() {
        rlClose = findViewById(R.id.rl_close);
        addressFilterRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        rlAddress = (RelativeLayout) findViewById(R.id.rl_address);

        rlClose.setOnClickListener(this);
        rlAddress.setOnClickListener(this);
    }


    /**
     * Description: start animation when exit
     * the activity with back button pressing
     */

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        //replaces the default 'Back' button action
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            super.onBackPressed();
            overridePendingTransitionExit();
        }
        return true;

    }



    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.rl_close:
                overridePendingTransitionExit();
                break;
            case R.id.rl_address:
                addNewAddress();
                break;
        }
    }


    /**
     * Description: Function which animate the
     * page when exit the activity
     */

    protected void overridePendingTransitionExit() {
//        super.onBackPressed();
        Intent intent = new Intent();
        intent.putExtra("lat", lat);
        intent.putExtra("lon", lon);
        setResult(1, intent);
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_down2, R.anim.slide_up2);
    }

    @Override
    public void onBackPressed() {
//        String data = mEditText.getText();
//        Intent intent = new Intent();
//        intent.putExtra("lat", lat);
//        intent.putExtra("lon", lon);
//        setResult(1, intent);
//        overridePendingTransitionExit();
    }


//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        overridePendingTransition(R.anim.slide_down, R.anim.slide_up);
//    }

    private void addNewAddress() {
        Intent intent = new Intent(AddressFilterActivity.this, MapActivity.class);
        startActivity(intent);
        overridePendingTransitionEnter();
    }


    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
    }



    private void setRecyclerViewStuff() {

        addressFilterlistener = new AddressFilterRecyclerViewListener() {
            @Override
            public void onClick(View view, final int position, final AddressFilterModel model, String type) {
                if(type.contains("layout")) {
                    for (int i = 0; i < addressFilterModelsList.size(); i++)
                        addressFilterModelsList.get(i).setClicked(false);
                    model.setClicked(true);
                    addressFilterAdapter.notifyDataSetChanged();
                    lat = model.getLat();
                    lon = model.getLon();
                    rlClose.performClick();
                }
                else
                {
                    new AlertDialog.Builder(AddressFilterActivity.this)
                            .setTitle("Remove Address")
                            .setMessage("Do you really want to Remove The Address?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    deleteProcess(position, model.getId());
                                }})
                            .setNegativeButton(android.R.string.no, null).show();
                }
            }

        };



        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        addressFilterRecyclerView.setLayoutManager(layoutManager);
        addressFilterAdapter = new AddressFilterAdapter(addressFilterModelsList, AddressFilterActivity.this, addressFilterlistener, 1);
        addressFilterRecyclerView.setAdapter(addressFilterAdapter);
        addressFilterAdapter.notifyDataSetChanged();


//        getAddresses();

    }

    private void getAddresses() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.waiting_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        RAddresses.Fire(new RetrofitHandler.apiResponseHandler<AddressOutData>() {
            @Override
            public void onSuccess(AddressOutData addressOutData) {
                if (addressOutData.getFavorites() != null)
                    successProcess(addressOutData);
                dialog.dismiss();
            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }


    private void successProcess(AddressOutData addressOutData) {
        for (int i = 0; i < addressOutData.getFavorites().size(); i++) {
            AddressFilterModel model = new AddressFilterModel();
            model.setAddress(addressOutData.getFavorites().get(i).getName());
            model.setId(addressOutData.getFavorites().get(i).getId());
            model.setLat(addressOutData.getFavorites().get(i).getLatitude());
            model.setLon(addressOutData.getFavorites().get(i).getLongitude());
            addressFilterModelsList.add(model);
        }
        addressFilterAdapter.notifyDataSetChanged();
    }



    private void deleteProcess(int pos, int id) {
        RDeleteAddress.Fire(id, new RetrofitHandler.apiResponseHandler<LoginOutData>() {
            @Override
            public void onSuccess(LoginOutData loginOutData) {
                addressFilterModelsList.remove(pos);
                addressFilterAdapter.notifyItemRemoved(pos);
            }

            @Override
            public void onFail(int code, String message) {

            }

            @Override
            public void onFailure(Throwable t) {

            }

            @Override
            public void onException(Exception e) {

            }
        });
    }


    @Override
    public void onResume()
    {
        super.onResume();
        addressFilterModelsList.clear();
        getAddresses();
    }


}