package com.knock.knocked.activities;

import android.animation.ObjectAnimator;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.knock.knocked.G;
import com.knock.knocked.R;
import com.knock.knocked.adapters.ChatListAdapter;
import com.knock.knocked.adapters.WorkingHourAdapter;
import com.knock.knocked.listeners.ChatListRecyclerViewListener;
import com.knock.knocked.listeners.WorkingHoursRecyclerViewListener;
import com.knock.knocked.models.ChatListModel;
import com.knock.knocked.models.WorkingHourModel;
import com.knock.knocked.rest.RSendOrder;
import com.knock.knocked.rest.RetrofitHandler;
import com.knock.knocked.rest.RetrofitHandler2;
import com.knock.knocked.rest.models.SendOrderInData;

import org.w3c.dom.Text;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BookingActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView ivCanel;
    /**
     * RecyclerView for working hours in chat list
     */
    private RecyclerView recyclerView;
    /**
     * Adapter for working hours list
     */
    private RecyclerView.Adapter mAdapter;
    /**
     * Click Listener for items of working hours list in page
     */
    private WorkingHoursRecyclerViewListener listener;
    /**
     * Array list for working hours
     */
    private ArrayList<WorkingHourModel> workingHoursList = new ArrayList<>();
    private Button btnReserve;
    private CalendarView calendarView;
    private int providerId, timeId, userId;
    private String date, name;
    private Dialog dialog;
    private TextView tvName;
    private boolean fadeInFlag = false,control = true;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        initView();
        recyclerViewStuff();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     * then get the list of professions and
     * experts
     */

    private void initView() {
        providerId = getIntent().getIntExtra("provider_id", 0);
        userId = getIntent().getIntExtra("user_id", 0);
        name = getIntent().getStringExtra("name");
        ivCanel = findViewById(R.id.iv_cancel);
        tvName = findViewById(R.id.tv_name);
        calendarView = findViewById(R.id.calendar_view);
        btnReserve = findViewById(R.id.btn_reserve);
        recyclerView = findViewById(R.id.recycler_view);

        ivCanel.setOnClickListener(this);
        btnReserve.setOnClickListener(this);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                String months = "", days = "";
                month = month + 1;
                if (month < 10)
                    months = "0" + month;
                else
                    months = months + "";

                if (dayOfMonth < 10)
                    days = "0" + dayOfMonth;
                else
                    days = dayOfMonth + "";

                date = year + "-" + months + "-" + days;

            }
        });

        tvName.setText(name);

    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_cancel:
                overridePendingTransitionExit();
//                onBackPressed();
//                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.btn_reserve:
                dialog = new Dialog(this);
                dialog.setContentView(R.layout.done_dialog);
//                dialog.getWindow().setBackgroundDrawableResource(R.color.me);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sendOffer();
                    }
                },1800);

                break;
        }
    }


    private void recyclerViewStuff() {

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);

        listener = new WorkingHoursRecyclerViewListener() {
            @Override
            public void onClick(View view, final int position, final WorkingHourModel model, String type) {
                if(control) {
                    if (!fadeInFlag)
                        fadeInAnimation();
                    for (int i = 0; i < workingHoursList.size(); i++) {
                        if (i != position) {
                            workingHoursList.get(i).setStat(0);
                            mAdapter.notifyItemChanged(i);
                        } else {
                            if (workingHoursList.get(position).getStat() == 1) {
                                workingHoursList.get(position).setStat(0);
                                mAdapter.notifyItemChanged(position);
                                btnReserve.setVisibility(View.INVISIBLE);
                            } else {
                                fadeInAnimation();
                                timeId = position;
                                workingHoursList.get(i).setStat(1);
                                mAdapter.notifyItemChanged(i);
                            }
                        }

                    }
                }

            }

        };


        mAdapter = new WorkingHourAdapter(workingHoursList, this, listener, 1);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();


        for (int i = 0; i < 6; i++) {
            WorkingHourModel model = new WorkingHourModel();
            model.setTime((i * 2) + " - " + ((i * 2) + 2) + "" + " AM");
            model.setStatus("All Free");
            workingHoursList.add(model);
        }

        for (int i = 6; i < 12; i++) {
            WorkingHourModel model = new WorkingHourModel();
            model.setTime((i * 2) + " - " + ((i * 2) + 2) + "" + " PM");
            model.setStatus("All Free");
            workingHoursList.add(model);
        }

        mAdapter.notifyDataSetChanged();
    }


    public void fadeInAnimation() {
        fadeInFlag = true;
        control = false;
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(1800);

        btnReserve.setVisibility(View.VISIBLE);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                btnReserve.setVisibility(View.VISIBLE);
                control = true;
            }
        }, 100);

    }

    private void sendOffer() {

        SharedPreferences settings = G.getContext().getSharedPreferences("me",
                Context.MODE_PRIVATE);
        int myId = settings.getInt("user_id", 0);
        SendOrderInData data = new SendOrderInData();

        data.setOwner(myId);
        data.setProvider(providerId);
        data.setOrder_date(date);
        data.setOrder_time(timeId + "");
        RSendOrder.Fire(data, new RetrofitHandler.apiResponseHandler<ResponseBody>() {
            @Override
            public void onSuccess(ResponseBody responseBody) {
                Intent intent = new Intent(BookingActivity.this, ChatBoxActivity.class);
                intent.putExtra("receiver_id", userId);
                intent.putExtra("name", name);
                startActivity(intent);

            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }


    protected void overridePendingTransitionExit() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if(dialog != null)
            dialog.dismiss();
    }
}