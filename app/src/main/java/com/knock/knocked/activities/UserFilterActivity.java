package com.knock.knocked.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.knock.knocked.R;
import com.knock.knocked.adapters.UserFilterAdapter;
import com.knock.knocked.listeners.UserFilterRecyclerViewListener;
import com.knock.knocked.models.LanguageData;
import com.knock.knocked.models.UserFilterModel;
import com.knock.knocked.rest.RLanguages;
import com.knock.knocked.rest.RetrofitHandler;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class UserFilterActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout rlClose;
    /**
     * RecyclerView list for professions list
     */
    private RecyclerView userFilterRecyclerView;
    /**
     * Adapter for professions list
     */
    private RecyclerView.Adapter userFilterAdapter;
    /**
     * Listener for click action ( click on profession list items )
     */
    private UserFilterRecyclerViewListener userFilterlistener;
    /**
     * Array list for professions
     */
    private ArrayList<UserFilterModel> userFilterModelsList = new ArrayList<>();
    private TextView tvClear, tvLanguage;
    private RadioButton rMale, rFemale;
    private Button btnFilter;
    private boolean maleBool, femaleBool, isCollapse;
    private ArrayList<Integer> languages = new ArrayList<>();


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_filter);
        initView();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     */

    private void initView() {
        rlClose = findViewById(R.id.rl_close);
        tvClear = findViewById(R.id.tv_clear);
        rMale = findViewById(R.id.r_male);
        rFemale = findViewById(R.id.r_female);
        btnFilter = findViewById(R.id.btn_filter);
        tvLanguage = findViewById(R.id.tv_language);
        userFilterRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        setRecyclerViewStuff();

        getLanguages();

        rlClose.setOnClickListener(this);
        tvClear.setOnClickListener(this);
        btnFilter.setOnClickListener(this);
        tvLanguage.setOnClickListener(this);
    }


    /**
     * Description: start animation when exit
     * the activity with back button pressing
     */

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            super.onBackPressed();
            overridePendingTransitionExit();
        }
        return true;

    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_close:
                overridePendingTransitionExit();
                break;
            case R.id.tv_clear:
                rMale.setChecked(false);
                rFemale.setChecked(false);
                break;
            case R.id.btn_filter:
                filter();
                break;
            case R.id.tv_language:
                if (isCollapse)
                    collapse(userFilterRecyclerView);
                else
                    expand(userFilterRecyclerView);
                break;
        }
    }


    /**
     * Description: Function which animate the
     * page when exit the activity
     */

    protected void overridePendingTransitionExit() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_down2, R.anim.slide_up2);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_down, R.anim.slide_up);
    }


    private void setRecyclerViewStuff() {

        userFilterlistener = new UserFilterRecyclerViewListener() {
            @Override
            public void onClick(View view, final int position, final UserFilterModel model, String type) {
                for (int i = 0; i < userFilterModelsList.size(); i++)
                    userFilterModelsList.get(i).setClicked(false);
                model.setClicked(true);
                userFilterAdapter.notifyDataSetChanged();
                rlClose.performClick();
            }

        };


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        userFilterRecyclerView.setLayoutManager(layoutManager);
        userFilterAdapter = new UserFilterAdapter(userFilterModelsList, UserFilterActivity.this, userFilterlistener, 1);
        userFilterRecyclerView.setAdapter(userFilterAdapter);
        userFilterAdapter.notifyDataSetChanged();

    }


    private void getLanguages() {
        RLanguages.Fire(new RetrofitHandler.apiResponseHandler<LanguageData>() {
            @Override
            public void onSuccess(LanguageData languageData) {

                for (int i = 0; i < languageData.getList().size(); i++) {
                    UserFilterModel model = new UserFilterModel();
                    model.setLanguage(languageData.getList().get(i).getTitle());
                    model.setId(languageData.getList().get(i).getId());
                    model.setClicked(true);
                    userFilterModelsList.add(model);
                }
                userFilterAdapter.notifyDataSetChanged();

                collapse(userFilterRecyclerView);

            }

            @Override
            public void onFail(int code, String message) {
                int a = 1;
            }

            @Override
            public void onFailure(Throwable t) {
                int a = 1;
            }

            @Override
            public void onException(Exception e) {
                int a = 1;
            }
        });
    }

    private void filter() {
        if (rMale.isChecked())
            maleBool = true;
        if (rFemale.isChecked())
            femaleBool = true;
        for (int j = 0; j < userFilterModelsList.size(); j++) {
            if (userFilterModelsList.get(j).isClicked())
                languages.add(userFilterModelsList.get(j).getId());
        }

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("male", maleBool);
        intent.putExtra("female", femaleBool);
        intent.putExtra("language", languages);
        intent.putExtra("filter", true);
        startActivity(intent);
        this.finish();

    }


    public  void expand(final View v) {
        isCollapse = false;
        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Expansion speed of 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public  void collapse(final View v) {
        isCollapse = true;
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Collapse speed of 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }


}