package com.knock.knocked.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.knock.knocked.R;
import com.knock.knocked.rest.RRegister;
import com.knock.knocked.rest.RetrofitHandler;
import com.knock.knocked.rest.RetrofitHandler2;
import com.knock.knocked.rest.models.RegisterData;

import okhttp3.ResponseBody;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {
    private RelativeLayout rlLogin,rlLayout;
    private TextInputEditText etPhone, etName, etLastName;
    private TextInputLayout tlName,tlLastName,tlPhone;
    private Dialog dialog;
    private TextView tvLogin,tvKnocked,tvDes;
    private String IMEINumber,fireBaseToken;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     * then get the list of professions and
     * experts
     */

    private void initView() {
        rlLogin = findViewById(R.id.rl_login);
        rlLayout= findViewById(R.id.rl_layout);
        etName = findViewById(R.id.et_name);
        etLastName = findViewById(R.id.et_last);
        etPhone = findViewById(R.id.et_phone);
        tlName = findViewById(R.id.tl_name);
        tlLastName = findViewById(R.id.tl_last);
        tvLogin = findViewById(R.id.tv_login);
        tlPhone = findViewById(R.id.tl_phone);
        tvKnocked = findViewById(R.id.tv_knocked);
        tvDes = findViewById(R.id.tv_des);

        Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/grobold.ttf");
        tvKnocked.setTypeface(tf);




        rlLogin.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
        rlLayout.setOnClickListener(this);

        getFireBaseToken();
    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_login:
                getIMEI();
                break;
            case R.id.tv_login:
                Intent intent = new Intent(this,LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.rl_layout:
                hideKeyboard(RegisterActivity.this);
                break;
        }
    }


    private void register() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.waiting_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        resetInputs();
        RegisterData registerData = new RegisterData();
        registerData.setCellphone(etPhone.getText().toString());
//        registerData.setFirst_name(etName.getText().toString());
        registerData.setType("android");
        registerData.setRegistration_id(fireBaseToken);
        registerData.setDevice_id(IMEINumber);
        RRegister.Fire(registerData, new RetrofitHandler2.apiResponseHandler<ResponseBody>() {
            @Override
            public void onSuccess(ResponseBody registerData) {
                successProcess();
            }

            @Override
            public void onFail(int code, String message) {
                if (code == 400)
                    failProccess();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }

    private void failProccess() {
        dialog.dismiss();
        Toast.makeText(this, "This Phone Number Had been already registered", Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(RegisterActivity.this,ConfirmationCodeActivity.class);
//        intent.putExtra("phone",etPhone.getText().toString().trim());
//        intent.putExtra("already",true);
//        startActivity(intent);
//        RegisterActivity.this.finish();
    }

    private void successProcess() {
        dialog.dismiss();
        Intent intent = new Intent(RegisterActivity.this,ConfirmationCodeActivity.class);
        intent.putExtra("phone",etPhone.getText().toString().trim());
        startActivity(intent);
        RegisterActivity.this.finish();
    }

    private boolean isValidInputs()
    {
        if(etPhone.getText().toString().length() < 11) {
            tlPhone.setError("Must be 11 digits");
            return false;
        }
//        if(etName.getText().toString().trim().length() < 3) {
//            tlPhone.setError("Must be at least 3 characters");
//            return false;
//        }


        return true;
    }

    private void resetInputs()
    {
        tlName.setError(null);
        tlLastName.setError(null);
        tlPhone.setError(null);
    }



    private void getIMEI()
    {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 7);
            ActivityCompat.requestPermissions(RegisterActivity.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    7);
            return ;
        }
        else {
            IMEINumber = telephonyManager.getDeviceId();
            if (isValidInputs())
                register();
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 7: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    IMEINumber = telephonyManager.getDeviceId();
                    rlLogin.performClick();

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(RegisterActivity.this, "Permission denied to read your Phone state", Toast.LENGTH_SHORT).show();
                }
                return;
            }


        }
    }


    private void getFireBaseToken()
    {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }

                        // Get new Instance ID token
                         fireBaseToken = task.getResult().getToken();

                    }
                });
    }


}
