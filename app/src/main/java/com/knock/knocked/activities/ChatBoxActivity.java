package com.knock.knocked.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.JsonObject;
import com.knock.knocked.G;
import com.knock.knocked.R;
import com.knock.knocked.adapters.ChatBoxesAdapter;
import com.knock.knocked.adapters.ReviewListAdapter;
import com.knock.knocked.listeners.ReviewListRecyclerListener;
import com.knock.knocked.models.AllChatsOutData;
import com.knock.knocked.models.ReviewModel;
import com.knock.knocked.rest.RAcceptOrder;
import com.knock.knocked.rest.RAllChats;
import com.knock.knocked.rest.RChatOrders;
import com.knock.knocked.rest.RRejectOrder;
import com.knock.knocked.rest.RRejectOrderByOwner;
import com.knock.knocked.rest.RetrofitHandler;
import com.knock.knocked.rest.RetrofitHandler2;
import com.knock.knocked.rest.models.ChatOrdersOutData;
import com.knock.knocked.rest.models.RejectData;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChatBoxActivity extends BaseActivity implements View.OnClickListener {
    private ImageView ivBack;
    /**
     * RecyclerView for contacts in chat list
     */
    private RecyclerView recyclerView;
    /**
     * Adapter for contacts list
     */
    private RecyclerView.Adapter mAdapter;
    /**
     * Click Listener for items of contacts list in chat room
     */
    private ReviewListRecyclerListener listener;
    /**
     * Array list for contacts in chat list
     */
    private ArrayList<ReviewModel> chatListModelsList = new ArrayList<>();
    private WebSocketClient mWebSocketClient;
    private ImageView ivSend, ivCall, ivProf;
    private EditText etMessage;
    private Dialog dialog;
    private int receiverId, orderId;
    private TextView tvName, tvMessage, tvMessage2;
    private String name,pic;
    private RelativeLayout rlLayout, rlRej, rlCancel;
    private LinearLayout lLayout;
    private Button btnAccept, btnReject, btnCancel;
    private boolean isOwner;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        connectWebSocket();
        initView();
        keyboardOpens();
        recyclerViewStuff();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     */

    private void initView() {
        name = getIntent().getStringExtra("name");
        pic = getIntent().getStringExtra("pic");

        ivBack = findViewById(R.id.iv_cancel);
        ivProf = findViewById(R.id.iv_prof);
        ivCall = findViewById(R.id.iv_call);
        tvMessage = findViewById(R.id.message);
        tvMessage2 = findViewById(R.id.message2);
        rlLayout = findViewById(R.id.rl_layout);
        lLayout = findViewById(R.id.l_layout);
        etMessage = findViewById(R.id.et_message);
        tvName = findViewById(R.id.tv_name);
        ivSend = findViewById(R.id.iv_send);
        rlRej = findViewById(R.id.rl_rej);
        rlCancel = findViewById(R.id.rl_cancel);
        btnCancel = findViewById(R.id.btn_cancel);
        btnReject = findViewById(R.id.btn_reject);
        btnAccept = findViewById(R.id.btn_accept);
        recyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        ivBack.setOnClickListener(this);
        ivSend.setOnClickListener(this);
        rlLayout.setOnClickListener(this);
        ivCall.setOnClickListener(this);
        lLayout.setOnClickListener(this);
        recyclerView.setOnClickListener(this);
        btnReject.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnAccept.setOnClickListener(this);


        tvName.setText(name);


        Glide.with(this)
                .load(pic)  //http://karmento.ir      http://karmento.ir
                .placeholder(R.drawable.avatar2)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                .fitCenter()
                .into(ivProf);//this method help t

    }


    /**
     * Description: start animation when exit
     * the activity with back button pressing
     */

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            super.onBackPressed();
            overridePendingTransitionExit();
        }
        return true;

    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_cancel:
                super.onBackPressed();
                overridePendingTransitionExit();
                break;
            case R.id.rl_close:
                overridePendingTransitionExit();
                break;
            case R.id.iv_send:
                if (etMessage.getText().toString().trim().length() != 0)
                    sendMessage();
//                mWebSocketClient.close();
                break;
            case R.id.rl_layout:
                hideKeyboard(ChatBoxActivity.this);
                break;
            case R.id.iv_call:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:0919804871"));
                startActivity(intent);
                break;
            case R.id.recycler_view:
                hideKeyboard(ChatBoxActivity.this);
                break;
            case R.id.btn_reject:
                new AlertDialog.Builder(ChatBoxActivity.this)
                        .setTitle("Reject Order")
                        .setMessage("Do you really want to reject the order?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                rejectProcess();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
                break;
            case R.id.btn_cancel:
                new AlertDialog.Builder(ChatBoxActivity.this)
                        .setTitle("Cancel Order")
                        .setMessage("Do you really want to cancel the order?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                rejectProcess();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
                break;
            case R.id.btn_accept:
                new AlertDialog.Builder(ChatBoxActivity.this)
                        .setTitle("Accept Order")
                        .setMessage("Do you really want to accept the order?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                acceptProcess();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
                break;
        }
    }


    /**
     * Description: Function which animate the
     * page when exit the activity
     */

    protected void overridePendingTransitionExit() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }


    /**
     * Description: This is the method
     * which sets the recyclerview stuffs
     * its listener and layout manager and ...
     */

    private void recyclerViewStuff() {
        listener = new ReviewListRecyclerListener() {
            @Override
            public void onClick(View view, final int position, final ReviewModel model, String type) {
                hideKeyboard(ChatBoxActivity.this);
            }

        };


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(false);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new ChatBoxesAdapter(chatListModelsList, this, listener, 1);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();


        mAdapter.notifyDataSetChanged();


        receiverId = 41;
        receiverId = getIntent().getIntExtra("receiver_id", 0);
        getAllChats();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_down, R.anim.slide_up);
    }


    private void connectWebSocket() {
        URI uri;
        try {
            SharedPreferences settings = getSharedPreferences("me",
                    Context.MODE_PRIVATE);
            String token = settings.getString("token", "");
            uri = new URI("ws://dev.hoonamapps.com/ws/chat/" + token + "/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        mWebSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                Log.i("Websocket", "Opened");
//                mWebSocketClient.send("Hello from " + Build.MANUFACTURER + " " + Build.MODEL);
            }

            @Override
            public void onMessage(String s) {
                final String message = s;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject obj = new JSONObject(s);
                            String m = obj.getString("message");
                            String status = obj.getString("action");
                            String date = obj.getString("date");
                            if (status.contains("received")) {
                                ReviewModel model = new ReviewModel();
                                model.setDes(m);
                                model.setType(2);
                                model.setName("Me");
                                model.setDate(date);
//                            model.setName(m);
                                chatListModelsList.add(model);
                                mAdapter.notifyItemInserted(chatListModelsList.size() - 1);
                            } else {
                                ReviewModel model = new ReviewModel();
                                model.setDes(m);
                                model.setType(1);
                                model.setDate(date);
//                            model.setName(m);
                                chatListModelsList.add(model);
                                mAdapter.notifyItemInserted(chatListModelsList.size() - 1);
                            }

                            recyclerView.smoothScrollToPosition(chatListModelsList.size());


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
            }

            @Override
            public void onClose(int i, String s, boolean b) {
                Log.i("Websocket", "Closed " + s);
                mWebSocketClient.connect();
            }

            @Override
            public void onError(Exception e) {
                Log.i("Websocket", "Error " + e.getMessage());
            }
        };
        mWebSocketClient.connect();
    }


    public void sendMessage() {
        JSONObject obj = null;
        EditText editText = findViewById(R.id.et_message);
        String json = "{status:message,action:send,message:'" + etMessage.getText().toString().trim() + "',destination:" + receiverId + "}";

        try {

            obj = new JSONObject(json);


        } catch (Throwable t) {
            Log.e("My App", "Could not parse malformed JSON: \"" + json + "\"");
        }
        try {
            mWebSocketClient.send(obj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        editText.setText("");
    }


    private void getAllChats() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.waiting_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        RAllChats.Fire(receiverId, new RetrofitHandler.apiResponseHandler<AllChatsOutData>() {
            @Override
            public void onSuccess(AllChatsOutData allChatsOutData) {
                dialog.dismiss();
                getChatsSuccess(allChatsOutData);
                getOrders();
            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }


    private void getChatsSuccess(AllChatsOutData allChatsOutData) {

        try {
            for (int i = 0; i < allChatsOutData.getList().size(); i++) {
                if (allChatsOutData.getList().get(i).getSource().getId() == receiverId) {
                    ReviewModel model = new ReviewModel();
                    model.setType(1);
                    model.setName(allChatsOutData.getList().get(i).getSource().getFirst_name() +
                            " " + allChatsOutData.getList().get(i).getSource().getLast_name());
                    model.setDes(allChatsOutData.getList().get(i).getMessage());
                    model.setDate(allChatsOutData.getList().get(i).getDate());
                    chatListModelsList.add(model);
                    mAdapter.notifyItemInserted(chatListModelsList.size() - 1);
                } else if (allChatsOutData.getList().get(i).getDestination().getId() == receiverId) {
                    ReviewModel model = new ReviewModel();
                    model.setType(2);
                    model.setName(allChatsOutData.getList().get(i).getSource().getFirst_name() +
                            " " + allChatsOutData.getList().get(i).getSource().getLast_name());
                    model.setDes(allChatsOutData.getList().get(i).getMessage());
                    model.setDate(allChatsOutData.getList().get(i).getDate());
                    chatListModelsList.add(model);
                    mAdapter.notifyItemInserted(chatListModelsList.size() - 1);
                }
            }
            recyclerView.smoothScrollToPosition(chatListModelsList.size() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void keyboardOpens() {
        getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                getWindow().getDecorView().getWindowVisibleDisplayFrame(r);
                int screenHeight = getWindow().getDecorView().getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;


                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    if (chatListModelsList.size() != 0)
                        recyclerView.smoothScrollToPosition(chatListModelsList.size() - 1);
                } else {
                    // keyboard is closed
                }
            }
        });
    }


    private void getOrders() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.waiting_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        RChatOrders.Fire(new RetrofitHandler.apiResponseHandler<ChatOrdersOutData>() {
            @Override
            public void onSuccess(ChatOrdersOutData chatOrdersOutData) {
                dialog.dismiss();
                if (chatOrdersOutData.getPendding().size() != 0) {
                    for (int i = 0; i < chatOrdersOutData.getPendding().size(); i++) {
                        if (receiverId == chatOrdersOutData.getPendding().get(i).getOwner().getId()) {
                            orderId = chatOrdersOutData.getPendding().get(i).getId();
                            rlRej.setVisibility(View.VISIBLE);
                            rlCancel.setVisibility(View.GONE);

                            String myDate = convertDate(chatOrdersOutData.getPendding().get(i).getOrder_date());
                            String time = convertTime(chatOrdersOutData.getPendding().get(i).getOrder_time());
                            String name = chatOrdersOutData.getPendding().get(i).getOwner().getFirst_name();
                            tvMessage.setText(name + " asked if you are available on\n"
                                    + myDate + " " + time);
                            tvMessage2.setText(name + " asked if you are available on\n"
                                    + myDate + " " + time);
                        }
                    }
                } else if (chatOrdersOutData.getAccepted().size() != 0) {
                    for (int i = 0; i < chatOrdersOutData.getAccepted().size(); i++) {
                        if (receiverId == chatOrdersOutData.getAccepted().get(i).getOwner().getId()) {
                            orderId = chatOrdersOutData.getAccepted().get(i).getId();
                            rlRej.setVisibility(View.GONE);
                            rlCancel.setVisibility(View.VISIBLE);
                            String myDate = convertDate(chatOrdersOutData.getAccepted().get(i).getOrder_date());
                            String time = convertTime(chatOrdersOutData.getAccepted().get(i).getOrder_time());
                            String name = chatOrdersOutData.getAccepted().get(i).getOwner().getFirst_name();

                            tvMessage2.setText(name + " asked if you are available on\n"
                                    + myDate + " " + time);
                        }
                    }
                } else if (chatOrdersOutData.getRequest().size() != 0) {
                    for (int i = 0; i < chatOrdersOutData.getRequest().size(); i++) {
                        if (receiverId == chatOrdersOutData.getRequest().get(i).getProvider().getId()) {
                            isOwner = true;
                            orderId = chatOrdersOutData.getRequest().get(i).getId();
                            rlRej.setVisibility(View.GONE);
                            rlCancel.setVisibility(View.VISIBLE);
                            String myDate = convertDate(chatOrdersOutData.getRequest().get(i).getOrder_date());
                            String time = convertTime(chatOrdersOutData.getRequest().get(i).getOrder_time());
                            String name = chatOrdersOutData.getRequest().get(i).getProvider().getUser().getFirst_name();

                            tvMessage.setText(name + "You asked from" + name + " if he/she is available on\n"
                                    + myDate + " " + time);
                            tvMessage2.setText(name + "You asked from" + name + " if he/she is available on\n"
                                    + myDate + " " + time);
                        }
                    }
                } else {
                    rlRej.setVisibility(View.GONE);
                    rlCancel.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
                rlRej.setVisibility(View.GONE);
                rlCancel.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
                rlRej.setVisibility(View.GONE);
                rlCancel.setVisibility(View.GONE);
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
                rlRej.setVisibility(View.GONE);
                rlCancel.setVisibility(View.GONE);
            }
        });
    }

    private void rejectProcess() {
        if (!isOwner) {
            dialog = new Dialog(this);
            dialog.setContentView(R.layout.done_dialog);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            Window window = dialog.getWindow();
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            RejectData data = new RejectData();
            data.setOrder(orderId);
            RRejectOrder.Fire(data, new RetrofitHandler.apiResponseHandler<ResponseBody>() {
                @Override
                public void onSuccess(ResponseBody responseBody) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                            rlRej.setVisibility(View.GONE);
                        }
                    }, 1800);

                }

                @Override
                public void onFail(int code, String message) {
                    dialog.dismiss();
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                }

                @Override
                public void onException(Exception e) {
                    dialog.dismiss();
                }
            });
        } else {
            dialog = new Dialog(this);
            dialog.setContentView(R.layout.done_dialog);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            Window window = dialog.getWindow();
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            RejectData data = new RejectData();
            data.setOrder(orderId);
            RRejectOrderByOwner.Fire(data, new RetrofitHandler.apiResponseHandler<ResponseBody>() {
                @Override
                public void onSuccess(ResponseBody responseBody) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                            rlRej.setVisibility(View.GONE);
                        }
                    }, 1800);

                }

                @Override
                public void onFail(int code, String message) {
                    dialog.dismiss();
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                }

                @Override
                public void onException(Exception e) {
                    dialog.dismiss();
                }
            });
        }
    }


    private void acceptProcess() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.done_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        RejectData data = new RejectData();
        data.setOrder(orderId);
        RAcceptOrder.Fire(data, new RetrofitHandler.apiResponseHandler<ResponseBody>() {
            @Override
            public void onSuccess(ResponseBody responseBody) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        rlRej.setVisibility(View.GONE);
                        rlCancel.setVisibility(View.VISIBLE);
                    }
                }, 1800);

            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }

    private String convertTime(int myTime) {
        switch (myTime) {
            case 0:
                return ",At:0-2 AM";
            case 1:
                return ",At:2-4 AM";
            case 2:
                return ",At:4-6 AM";
            case 3:
                return ",At:6-8 AM";
            case 4:
                return ",At:8-10 AM";
            case 5:
                return ",At:10-12 AM";
            case 6:
                return ",At:12-14 PM";
            case 7:
                return ",At:14-16 PM";
            case 8:
                return ",At:16-18 PM";
            case 9:
                return ",At:18-20 PM";
            case 10:
                return ",At:20-22 PM";
            case 11:
                return ",At:22-24 PM";
        }
        return "";
    }

    private String convertDate(String date) {
        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, Integer.parseInt(year));
        cal.set(Calendar.MONTH, Integer.parseInt(month));
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day));

        SimpleDateFormat format = new SimpleDateFormat("EEEE, MMMM d, yyyy");
        return format.format(cal.getTime());
    }


}
