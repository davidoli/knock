package com.knock.knocked.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.knock.knocked.R;
import com.knock.knocked.adapters.AddressAdapter;
import com.knock.knocked.adapters.AddressFilterAdapter;
import com.knock.knocked.listeners.AddressFilterRecyclerViewListener;
import com.knock.knocked.listeners.AddressRecyclerViewListener;
import com.knock.knocked.models.AddressFilterModel;
import com.knock.knocked.models.AddressModel;
import com.knock.knocked.models.AddressOutData;
import com.knock.knocked.models.LoginOutData;
import com.knock.knocked.rest.RAddresses;
import com.knock.knocked.rest.RDeleteAddress;
import com.knock.knocked.rest.RetrofitHandler;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddressesActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout rlClose;
    /**
     * RecyclerView list for professions list
     */
    private RecyclerView addressRecyclerView;
    /**
     * Adapter for professions list
     */
    private RecyclerView.Adapter addressAdapter;
    /**
     * Listener for click action ( click on profession list items )
     */
    private AddressRecyclerViewListener addresslistener;
    /**
     * Array list for professions
     */
    private ArrayList<AddressModel> addressModelsList = new ArrayList<>();
    private ImageView ivBack;
    private RelativeLayout rlAddress;
    private boolean fromMap;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addressses);
        initView();
        setRecyclerViewStuff();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     */

    private void initView() {
        rlClose = findViewById(R.id.rl_close);
        ivBack = findViewById(R.id.iv_back);
        addressRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        rlAddress = (RelativeLayout) findViewById(R.id.rl_address);

        rlClose.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        rlAddress.setOnClickListener(this);
    }


    /**
     * Description: start animation when exit
     * the activity with back button pressing
     */

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            super.onBackPressed();
            overridePendingTransitionExit();
        }
        return true;

    }


    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                super.onBackPressed();
                break;
            case R.id.rl_address:
                addNewAddress();
                break;
        }
    }


    /**
     * Description: Function which animate the
     * page when exit the activity
     */

    protected void overridePendingTransitionExit() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_down2, R.anim.slide_up2);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }


    private void setRecyclerViewStuff() {

        addresslistener = new AddressRecyclerViewListener() {
            @Override
            public void onClick(View view, final int position, final AddressModel model, String type) {
                new AlertDialog.Builder(AddressesActivity.this)
                        .setTitle("Remove Address")
                        .setMessage("Do you really want to Remove The Address?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                deleteProcess(position, model.getId());
                            }})
                        .setNegativeButton(android.R.string.no, null).show();

            }

        };

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        addressRecyclerView.setLayoutManager(layoutManager);
        addressAdapter = new AddressAdapter(addressModelsList, AddressesActivity.this, addresslistener, 1);
        addressRecyclerView.setAdapter(addressAdapter);
        addressAdapter.notifyDataSetChanged();


//        getAddresses();

    }


    private void getAddresses() {
        RAddresses.Fire(new RetrofitHandler.apiResponseHandler<AddressOutData>() {
            @Override
            public void onSuccess(AddressOutData addressOutData) {
                if (addressOutData.getFavorites() != null)
                    successProcess(addressOutData);
            }

            @Override
            public void onFail(int code, String message) {
                int a = 1;
            }

            @Override
            public void onFailure(Throwable t) {
                int a = 1;
            }

            @Override
            public void onException(Exception e) {
                int a = 1;
            }
        });
    }


    private void successProcess(AddressOutData addressOutData) {
        for (int i = 0; i < addressOutData.getFavorites().size(); i++) {
            AddressModel model = new AddressModel();
            model.setAddress(addressOutData.getFavorites().get(i).getName());
            model.setId(addressOutData.getFavorites().get(i).getId());
            model.setLat(addressOutData.getFavorites().get(i).getLatitude());
            model.setLon(addressOutData.getFavorites().get(i).getLongitude());
            addressModelsList.add(model);
        }
        addressAdapter.notifyDataSetChanged();
    }


    private void deleteProcess(int pos, int id) {
        RDeleteAddress.Fire(id, new RetrofitHandler.apiResponseHandler<LoginOutData>() {
            @Override
            public void onSuccess(LoginOutData loginOutData) {
                addressModelsList.remove(pos);
                addressAdapter.notifyItemRemoved(pos);
            }

            @Override
            public void onFail(int code, String message) {

            }

            @Override
            public void onFailure(Throwable t) {

            }

            @Override
            public void onException(Exception e) {

            }
        });
    }

    private void addNewAddress() {
        Intent intent = new Intent(AddressesActivity.this, MapActivity.class);
        startActivity(intent);
        overridePendingTransitionEnter();
    }


    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.push_down_out, R.anim.push_down_in);
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (fromMap) {
            addressModelsList.clear();
            getAddresses();
//        } else
//            fromMap = true;
    }


}