package com.knock.knocked.activities;

import android.app.ActivityOptions;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.knock.knocked.R;
import com.knock.knocked.adapters.ChatListAdapter;
import com.knock.knocked.adapters.ReviewListAdapter;
import com.knock.knocked.listeners.ChatListRecyclerViewListener;
import com.knock.knocked.listeners.ReviewListRecyclerListener;
import com.knock.knocked.models.ChatListModel;
import com.knock.knocked.models.ReviewModel;
import com.knock.knocked.rest.RProviderReviews;
import com.knock.knocked.rest.RReview;
import com.knock.knocked.rest.RetrofitHandler;
import com.knock.knocked.rest.models.ReviewOutData;

import java.util.ArrayList;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ReviewActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView ivBack;
    /**
     * RecyclerView for contacts in chat list
     */
    private RecyclerView recyclerView;
    /**
     * Adapter for contacts list
     */
    private RecyclerView.Adapter mAdapter;
    /**
     * Click Listener for items of contacts list in chat room
     */
    private ReviewListRecyclerListener listener;
    /**
     * Array list for contacts in chat list
     */
    private ArrayList<ReviewModel> chatListModelsList = new ArrayList<>();
    private Dialog dialog;
    private int count,providerId;
    private String name;
    private TextView tvName,tvCount,tvEmpty;



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        initView();
        recyclerViewStuff();
    }


    /**
     * Description: This is the view method
     * which defines the widgets
     * and their clicks in the layout
     */

    private void initView() {
        name = getIntent().getStringExtra("name");
        providerId = getIntent().getIntExtra("provider_id",0);
        ivBack = findViewById(R.id.iv_cancel);
        tvName = findViewById(R.id.tv_name);
        tvCount = findViewById(R.id.tv_num);
        tvEmpty = findViewById(R.id.tv_empty);
        recyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        tvName.setText(name);

        ivBack.setOnClickListener(this);
    }


    /**
     * Description: start animation when exit
     * the activity with back button pressing
     */

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        //replaces the default 'Back' button action
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            super.onBackPressed();
            overridePendingTransitionExit();
        }
        return true;

    }



    /**
     * Description: This is the method
     * which defines all the click listeners
     * for the clickable widgets of the layout
     */

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.iv_cancel:
                super.onBackPressed();
                overridePendingTransitionExit();
                break;
            case R.id.rl_close:
                overridePendingTransitionExit();
                break;
        }
    }


    /**
     * Description: Function which animate the
     * page when exit the activity
     */

    protected void overridePendingTransitionExit() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }



    /**
     * Description: This is the method
     * which sets the recyclerview stuffs
     * its listener and layout manager and ...
     */

    private void recyclerViewStuff()
    {
        listener = new ReviewListRecyclerListener() {
            @Override
            public void onClick(View view, final int position, final ReviewModel model, String type) {

            }

        };


        mAdapter = new ReviewListAdapter(chatListModelsList, this, listener, 1);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();


       getReviews();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_down, R.anim.slide_up);
    }



    private void getReviews()
    {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.waiting_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        RProviderReviews.Fire(providerId,new RetrofitHandler.apiResponseHandler<ReviewOutData>() {
            @Override
            public void onSuccess(ReviewOutData reviewOutData) {
                dialog.dismiss();
                tvCount.setText(reviewOutData.getList().size()+" reviews");
                for (int i=0;i<reviewOutData.getList().size();i++)
                {
                    ReviewModel model = new ReviewModel();
                    model.setName(reviewOutData.getList().get(i).getOwner().getFirst_name()+" "+
                            reviewOutData.getList().get(i).getOwner().getLast_name());
                    model.setDes(reviewOutData.getList().get(i).getReview());
                    model.setDate("some time");
                    chatListModelsList.add(model);
                }
                mAdapter.notifyDataSetChanged();

                if(chatListModelsList.size() > 0) {
                    tvEmpty.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
                else
                {
                    tvEmpty.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }


}
