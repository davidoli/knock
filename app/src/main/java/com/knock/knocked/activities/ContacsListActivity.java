package com.knock.knocked.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.knock.knocked.R;
import com.knock.knocked.adapters.ContactAdapter;
import com.knock.knocked.listeners.ContactRecyclerViewListener;
import com.knock.knocked.models.ContactModel;
import com.knock.knocked.models.ProviderInfoInData;
import com.knock.knocked.models.ResendData;
import com.knock.knocked.rest.RCompleteInfo;
import com.knock.knocked.rest.RInviteFriend;
import com.knock.knocked.rest.RetrofitHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import okhttp3.ResponseBody;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ContacsListActivity extends BaseActivity implements View.OnClickListener {

    private ArrayList<ContactModel> myDontProjectModelsList = new ArrayList<>();
    private ImageView ivBack;
    private Button btnSend;
    private int modelNumber;
    private int inviteNum, numberInvitedSoFar;
    private int catId, inviteNums, userId;
    private String des, web, insta;
    private double lat1, lat2, lat3, lon1, lon2, lon3;
    private boolean isFreelancer;
    private Dialog dialog;
    private RelativeLayout rlLayout, rlLogin;
    private TextInputLayout tlPhone, tlPhone2, tlPhone3;
    private TextInputEditText etPhone, etPhone2, etPhone3;
    private ImageView ivContact1, ivContact2, ivContact3;
    private SharedPreferences settings;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_contacts_list);

        settings = getSharedPreferences("me",
                Context.MODE_PRIVATE);

        inviteNum = settings.getInt("invite", 0);

        web = settings.getString("web", "");
        insta = settings.getString("insta", "");
        des = settings.getString("des", "");
        lat1 = settings.getFloat("lat1", -1);
        lat2 = settings.getFloat("lat2", -1);
        lat3 = settings.getFloat("lat3", -1);
        lon1 = settings.getFloat("lon1", -1);
        lon2 = settings.getFloat("lon2", -1);
        lon3 = settings.getFloat("lon3", -1);
        catId = settings.getInt("cat_id", -1);
        userId = settings.getInt("user_id", -1);
        isFreelancer = settings.getBoolean("is_freelancer", false);


        ivBack = findViewById(R.id.iv_back);
        rlLogin = findViewById(R.id.rl_login);
        rlLayout = findViewById(R.id.rl_layout);
        tlPhone = findViewById(R.id.tl_phone);
        tlPhone2 = findViewById(R.id.tl_phone2);
        tlPhone3 = findViewById(R.id.tl_phone3);
        etPhone = findViewById(R.id.et_phone);
        etPhone2 = findViewById(R.id.et_phone2);
        etPhone3 = findViewById(R.id.et_phone3);
        ivContact1 = findViewById(R.id.iv_contact1);
        ivContact2 = findViewById(R.id.iv_contact2);
        ivContact3 = findViewById(R.id.iv_contact3);

        ivBack.setOnClickListener(this);
        rlLogin.setOnClickListener(this);
//        btnSend.setOnClickListener(this);
        rlLayout.setOnClickListener(this);
        ivContact1.setOnClickListener(this);
        ivContact2.setOnClickListener(this);
        ivContact3.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                overridePendingTransitionExit();
                break;
            case R.id.rl_login:
                tlPhone.setError(null);
                tlPhone2.setError(null);
                tlPhone3.setError(null);
                if (etPhone.getText().toString().trim().length() < 11)
                    tlPhone.setError("Enter phone number correctly");
                else if (etPhone2.getText().toString().trim().length() < 11)
                    tlPhone2.setError("Enter phone number correctly");
                else if (etPhone3.getText().toString().trim().length() < 11)
                    tlPhone3.setError("Enter phone number correctly");
                else {
                    dialog = new Dialog(this);
                    dialog.setContentView(R.layout.done_dialog);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                    Window window = dialog.getWindow();
                    window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    sendProcess();
                }
                break;
            case R.id.rl_layout:
                hideKeyboard(ContacsListActivity.this);
                break;
            case R.id.iv_contact1:
                modelNumber = 1;
                Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(contactPickerIntent, 101);
                break;
            case R.id.iv_contact2:
                modelNumber = 2;
                Intent contactPickerIntent2 = new Intent(Intent.ACTION_PICK,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(contactPickerIntent2, 101);
                break;
            case R.id.iv_contact3:
                modelNumber = 3;
                Intent contactPickerIntent3 = new Intent(Intent.ACTION_PICK,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(contactPickerIntent3, 101);
                break;

        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 101:
                    Cursor cursor = null;
                    try {
                        String phoneNo = null;
                        String name = null;

                        Uri uri = data.getData();
                        cursor = getContentResolver().query(uri, null, null, null, null);
                        cursor.moveToFirst();
                        int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                        int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                        phoneNo = cursor.getString(phoneIndex);
                        name = cursor.getString(nameIndex);

                        if (modelNumber == 1)
                            etPhone.setText(phoneNo);
                        else if (modelNumber == 2)
                            etPhone2.setText(phoneNo);
                        else if (modelNumber == 3)
                            etPhone3.setText(phoneNo);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } else {

        }
    }


    private void sendProcess() {
        if (inviteNum < 3) {
            ResendData data = new ResendData();
            if (inviteNum == 0)
                data.setCellphone(etPhone.getText().toString().trim());
            else if(inviteNum == 1)
                data.setCellphone(etPhone2.getText().toString().trim());
            else if(inviteNum == 2)
                data.setCellphone(etPhone3.getText().toString().trim());
            RInviteFriend.Fire(data, new RetrofitHandler.apiResponseHandler<ResponseBody>() {
                @Override
                public void onSuccess(ResponseBody responseBody) {
                    if (inviteNum < 3) {
                        inviteNum += 1;
                        sendProcess();
                    } else
                        confirmationProcess();
                }

                @Override
                public void onFail(int code, String message) {

                }

                @Override
                public void onFailure(Throwable t) {

                }

                @Override
                public void onException(Exception e) {

                }
            });
        } else
            confirmationProcess();
    }


    private void confirmationProcess() {

        ProviderInfoInData data = new ProviderInfoInData();
        data.setDescription(des);
        data.setInstagram(insta);
        data.setWeb(web);
        data.setIs_virtual(isFreelancer);
        data.setUser(userId);
        data.setService_kind("a");
        data.setService(catId);
        data.setJob_done_time(settings.getFloat("duration",(float)0.2));
        RCompleteInfo.Fire(data, new RetrofitHandler.apiResponseHandler<ResponseBody>() {
            @Override
            public void onSuccess(ResponseBody responseBody) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        settings = getSharedPreferences("me",
                                Context.MODE_PRIVATE);
                        settings.edit().putInt("cat_id",-1).apply();
                        settings.edit().putInt("invite",-1).apply();
                        settings.edit().putString("des","").apply();
                        settings.edit().putString("web","").apply();
                        settings.edit().putString("insta","").apply();
                        settings.edit().putFloat("locationLat1", -1).apply();
                        settings.edit().putFloat("locationLat2", -1).apply();
                        settings.edit().putFloat("locationLat1", -1).apply();
                        settings.edit().putFloat("locationLon1", -1).apply();
                        settings.edit().putFloat("locationLon2", -1).apply();
                        settings.edit().putFloat("locationLon3", -1).apply();
                        settings.edit().putBoolean("is_freelancer", false).apply();
                        settings.edit().putFloat("duration", -1).apply();

                        Toast.makeText(ContacsListActivity.this, "Registered As a Provider Successfully", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(ContacsListActivity.this, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        ContacsListActivity.this.finish();
                        dialog.dismiss();
                    }
                }, 400);

            }

            @Override
            public void onFail(int code, String message) {
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
            }

            @Override
            public void onException(Exception e) {
                dialog.dismiss();
            }
        });
    }


    protected void overridePendingTransitionExit() {
        Intent intent = new Intent();

        settings = getSharedPreferences("me",
                Context.MODE_PRIVATE);
        settings.edit().putInt("cat_id", catId);
        settings.edit().putInt("invite", inviteNums);
        settings.edit().putString("des", des);
        settings.edit().putString("web", web);
        settings.edit().putString("insta", insta);
        settings.edit().putFloat("locationLat1", (float) lat1);
        settings.edit().putFloat("locationLat2", (float) lat2);
        settings.edit().putFloat("locationLat1", (float) lat3);
        settings.edit().putFloat("locationLon1", (float) lon1);
        settings.edit().putFloat("locationLon2", (float) lon2);
        settings.edit().putFloat("locationLon3", (float) lon3);
        settings.edit().putBoolean("is_freelancer", isFreelancer);
        settings.edit().putInt("user_id", userId);
        setResult(123, intent);
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_down2, R.anim.slide_up2);
    }

    @Override
    public void onBackPressed() {
        overridePendingTransitionExit();

    }

}