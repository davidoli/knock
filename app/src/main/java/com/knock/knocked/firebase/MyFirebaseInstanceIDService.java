//package com.knock.knocked.firebase;
//
///**
// * Created by lashgari.f on 25/09/2018.
// */
//
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.util.Log;
//
//import com.google.firebase.iid.FirebaseInstanceId;
//
//
//public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
//    private static final String TAG = "MyFirebaseIIDService";
//
//
//    @Override
//    public void onTokenRefresh() {
//        // Get updated InstanceID token.
//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//        Log.d(TAG, "Refreshed token: " + refreshedToken);
//        // If you want to send messages to this application instance or
//        // manage this apps subscriptions on the server side, send the
//        // Instance ID token to your app server.
//        SharedPreferences settings =  getSharedPreferences("me",
//                Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = settings.edit();
////        editor.putString(Constants.FIREBASE_TOKEN, refreshedToken);
//        editor.commit();
//
//    }
//
//}