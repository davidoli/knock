package com.knock.knocked.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.knock.knocked.activities.ExpertInfoActivity;
import com.knock.knocked.models.ExpertInData;
import com.knock.knocked.rest.models.ExpertData;
import com.knock.knocked.rest.models.ServicesData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Url;

public class RExpertList extends RetrofitHandler {
    public class Model {
        @SerializedName("status")
        @Expose
        public int status;
    }

    interface IModel {

        @GET()
        Call<ExpertData> getExperts(@Url String url);
    }


    public  static void Fire(final String url,apiResponseHandler<ExpertData>  responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RExpertList.IModel.class,context).getExperts(url);
            }
        }, responseHandler);
    }
}