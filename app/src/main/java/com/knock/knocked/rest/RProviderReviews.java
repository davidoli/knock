package com.knock.knocked.rest;

import com.knock.knocked.models.MyInfoData;
import com.knock.knocked.rest.models.ReviewOutData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class RProviderReviews extends RetrofitHandler {


    interface IModel {

        @GET("provider/review/{provider_id}/")
        Call<ReviewOutData> getReviews(@Path("provider_id") int id);
    }


    public  static void Fire(final int id,apiResponseHandler<ReviewOutData>  responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RProviderReviews.IModel.class,context).getReviews(id);
            }
        }, responseHandler);
    }
}

