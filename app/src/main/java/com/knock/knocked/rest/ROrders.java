package com.knock.knocked.rest;

import com.knock.knocked.models.MyInfoData;
import com.knock.knocked.models.OrdersOutData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ROrders extends RetrofitHandler {


    interface IModel {

        @GET("order/")
        Call<OrdersOutData> getOrders(@Query("date") String date);
    }


    public  static void Fire(final String date,apiResponseHandler<OrdersOutData>  responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(ROrders.IModel.class,context).getOrders(date);
            }
        }, responseHandler);
    }
}

