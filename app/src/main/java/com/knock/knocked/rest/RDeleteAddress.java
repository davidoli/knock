package com.knock.knocked.rest;

import com.knock.knocked.models.LoginData;
import com.knock.knocked.models.LoginOutData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.POST;
import retrofit2.http.Path;

public class RDeleteAddress extends RetrofitHandler {


    interface IModel {

        @DELETE("user_favorite/{id}/")
        Call<LoginOutData> deleteAddress(@Path("id") int id);
    }


    public  static void Fire(final int id,apiResponseHandler<LoginOutData> responseHandler){
        Request(new RetrofitHandler.fireApi() {
            @Override
            public Call Fire() {
                return getClient(RDeleteAddress.IModel.class,context).deleteAddress(id);
            }
        }, responseHandler);
    }
}



