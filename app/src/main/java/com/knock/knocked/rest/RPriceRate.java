package com.knock.knocked.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.knock.knocked.rest.models.PriceRateInData;
import com.knock.knocked.rest.models.RegisterData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class RPriceRate extends RetrofitHandler {



    interface IModel {

        @POST("qualification/rate/")
        Call<ResponseBody> rate(@Body PriceRateInData registerData);
    }


    public  static void Fire(final PriceRateInData registerData, apiResponseHandler<ResponseBody> responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RPriceRate.IModel.class,context).rate(registerData);
            }
        }, responseHandler);
    }
}

