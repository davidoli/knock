package com.knock.knocked.rest;

import com.knock.knocked.rest.models.ChatListDataOut;
import com.knock.knocked.rest.models.ChatOrdersOutData;

import retrofit2.Call;
import retrofit2.http.GET;

public class RChatOrders extends RetrofitHandler {


    interface IModel {

        @GET("order/me/")
        Call<ChatOrdersOutData> getlist();
    }


    public  static void Fire(apiResponseHandler<ChatOrdersOutData>  responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RChatOrders.IModel.class,context).getlist();
            }
        }, responseHandler);
    }
}

