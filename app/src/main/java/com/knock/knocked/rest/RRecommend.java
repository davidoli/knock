package com.knock.knocked.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.knock.knocked.rest.models.RecommendInData;
import com.knock.knocked.rest.models.RegisterData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class RRecommend extends RetrofitHandler {


    interface IModel {

        @POST("qualification/like/")
        Call<ResponseBody> rate(@Body RecommendInData registerData);
    }


    public  static void Fire(final RecommendInData registerData, apiResponseHandler<ResponseBody> responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RRecommend.IModel.class,context).rate(registerData);
            }
        }, responseHandler);
    }
}