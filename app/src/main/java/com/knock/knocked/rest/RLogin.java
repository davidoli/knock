package com.knock.knocked.rest;

import com.knock.knocked.models.LoginData;
import com.knock.knocked.models.LoginOutData;
import com.knock.knocked.rest.models.RegisterData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class RLogin extends RetrofitHandler2 {


    interface IModel {

        @POST("login/")
        Call<LoginOutData> login(@Body LoginData loginData);
    }


    public  static void Fire(LoginData loginData,apiResponseHandler<LoginOutData> responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RLogin.IModel.class,context).login(loginData);
            }
        }, responseHandler);
    }
}