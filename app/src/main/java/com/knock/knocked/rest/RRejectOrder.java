package com.knock.knocked.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.knock.knocked.rest.models.RegisterData;
import com.knock.knocked.rest.models.RejectData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PATCH;
import retrofit2.http.POST;

public class RRejectOrder extends RetrofitHandler {



    interface IModel {

        @PATCH("order/reject/")
        Call<ResponseBody> reject(@Body RejectData rejectData);
    }


    public  static void Fire(final RejectData rejectData, apiResponseHandler<ResponseBody> responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RRejectOrder.IModel.class,context).reject(rejectData);
            }
        }, responseHandler);
    }
}

