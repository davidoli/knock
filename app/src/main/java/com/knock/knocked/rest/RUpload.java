package com.knock.knocked.rest;

import com.knock.knocked.models.AvatarOut;
import com.knock.knocked.models.LoginData;
import com.knock.knocked.models.LoginOutData;
import com.knock.knocked.rest.models.AvatarInData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PATCH;
import retrofit2.http.POST;

public class RUpload  extends RetrofitHandler {


    interface IModel {

        @PATCH("user/avatar/")
        Call<AvatarOut> upload(@Body AvatarInData loginData);
    }


    public  static void Fire(AvatarInData loginData, RetrofitHandler.apiResponseHandler<AvatarOut> responseHandler){
        Request(new RetrofitHandler.fireApi() {
            @Override
            public Call Fire() {
                return getClient(RUpload.IModel.class,context).upload(loginData);
            }
        }, responseHandler);
    }
}



