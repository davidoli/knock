package com.knock.knocked.rest.models;

import java.util.List;

public class ServicesData {

    private int total_page_num;
    private int total_row;
    private List<ListBean> list;

    public int getTotal_page_num() {
        return total_page_num;
    }

    public void setTotal_page_num(int total_page_num) {
        this.total_page_num = total_page_num;
    }

    public int getTotal_row() {
        return total_row;
    }

    public void setTotal_row(int total_row) {
        this.total_row = total_row;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        private int id;
        private String title;
        private String icon;
        private Object parent;
        private ProvidersBean providers;
        private boolean is_publish;
        private List<?> childern;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public Object getParent() {
            return parent;
        }

        public void setParent(Object parent) {
            this.parent = parent;
        }

        public ProvidersBean getProviders() {
            return providers;
        }

        public void setProviders(ProvidersBean providers) {
            this.providers = providers;
        }

        public boolean isIs_publish() {
            return is_publish;
        }

        public void setIs_publish(boolean is_publish) {
            this.is_publish = is_publish;
        }

        public List<?> getChildern() {
            return childern;
        }

        public void setChildern(List<?> childern) {
            this.childern = childern;
        }

        public static class ProvidersBean {
        }
    }
}
