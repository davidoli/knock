package com.knock.knocked.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.knock.knocked.models.AddressOutData;
import com.knock.knocked.rest.models.ExpertData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public class RAddresses extends RetrofitHandler {


    interface IModel {

        @GET("user/me/")
        Call<AddressOutData> getAddresses();
    }


    public  static void Fire(apiResponseHandler<AddressOutData>  responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RAddresses.IModel.class,context).getAddresses();
            }
        }, responseHandler);
    }
}