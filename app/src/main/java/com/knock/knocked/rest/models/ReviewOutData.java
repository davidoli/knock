package com.knock.knocked.rest.models;

import java.util.List;

public class ReviewOutData {

    /**
     * list : [{"owner":{"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$1lQgimkS4ho2$t9Gt95Z7G5tIdtBZ7P3gnPHNGwQzboRXN4YakyiuVgk=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/IMG_20190513_003650_418_9E22eS1.jpg","email":null,"gender":"m","language":null,"invited":5},"provider":111,"review":"The is something new in your work and I like it so I recommend this provider to all of my friends, Good Luck."},{"owner":{"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$1lQgimkS4ho2$t9Gt95Z7G5tIdtBZ7P3gnPHNGwQzboRXN4YakyiuVgk=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/IMG_20190513_003650_418_9E22eS1.jpg","email":null,"gender":"m","language":null,"invited":5},"provider":111,"review":"Hjth is a great place to work for and I am very excited about this opportunity and I look forward to working with you and your company for the opportunity to work with you"},{"owner":{"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$1lQgimkS4ho2$t9Gt95Z7G5tIdtBZ7P3gnPHNGwQzboRXN4YakyiuVgk=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/IMG_20190513_003650_418_9E22eS1.jpg","email":null,"gender":"m","language":null,"invited":5},"provider":111,"review":"It is a great place to work for and I am very excited about this opportunity and I look forward to working with you and your company for the opportunity to work with you"},{"owner":{"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$1lQgimkS4ho2$t9Gt95Z7G5tIdtBZ7P3gnPHNGwQzboRXN4YakyiuVgk=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/IMG_20190513_003650_418_9E22eS1.jpg","email":null,"gender":"m","language":null,"invited":5},"provider":111,"review":"Okh and all other information about the program is"},{"owner":{"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$1lQgimkS4ho2$t9Gt95Z7G5tIdtBZ7P3gnPHNGwQzboRXN4YakyiuVgk=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/IMG_20190513_003650_418_9E22eS1.jpg","email":null,"gender":"m","language":null,"invited":5},"provider":111,"review":"U can come to my house"},{"owner":{"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$1lQgimkS4ho2$t9Gt95Z7G5tIdtBZ7P3gnPHNGwQzboRXN4YakyiuVgk=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/IMG_20190513_003650_418_9E22eS1.jpg","email":null,"gender":"m","language":null,"invited":5},"provider":111,"review":"U are the best"}]
     * total_page_num : 1
     * total_row : 6
     */

    private int total_page_num;
    private int total_row;
    private List<ListBean> list;

    public int getTotal_page_num() {
        return total_page_num;
    }

    public void setTotal_page_num(int total_page_num) {
        this.total_page_num = total_page_num;
    }

    public int getTotal_row() {
        return total_row;
    }

    public void setTotal_row(int total_row) {
        this.total_row = total_row;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * owner : {"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$1lQgimkS4ho2$t9Gt95Z7G5tIdtBZ7P3gnPHNGwQzboRXN4YakyiuVgk=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/IMG_20190513_003650_418_9E22eS1.jpg","email":null,"gender":"m","language":null,"invited":5}
         * provider : 111
         * review : The is something new in your work and I like it so I recommend this provider to all of my friends, Good Luck.
         */

        private OwnerBean owner;
        private int provider;
        private String review;

        public OwnerBean getOwner() {
            return owner;
        }

        public void setOwner(OwnerBean owner) {
            this.owner = owner;
        }

        public int getProvider() {
            return provider;
        }

        public void setProvider(int provider) {
            this.provider = provider;
        }

        public String getReview() {
            return review;
        }

        public void setReview(String review) {
            this.review = review;
        }

        public static class OwnerBean {
            /**
             * id : 40
             * username : 09198048741
             * password : pbkdf2_sha256$150000$1lQgimkS4ho2$t9Gt95Z7G5tIdtBZ7P3gnPHNGwQzboRXN4YakyiuVgk=
             * is_superuser : false
             * is_provider : false
             * first_name : David
             * last_name : Kalami
             * avatar : http://dev.hoonamapps.com/telemarket/media/avatars/IMG_20190513_003650_418_9E22eS1.jpg
             * email : null
             * gender : m
             * language : null
             * invited : 5
             */

            private int id;
            private String username;
            private String password;
            private boolean is_superuser;
            private boolean is_provider;
            private String first_name;
            private String last_name;
            private String avatar;
            private Object email;
            private String gender;
            private Object language;
            private int invited;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public boolean isIs_superuser() {
                return is_superuser;
            }

            public void setIs_superuser(boolean is_superuser) {
                this.is_superuser = is_superuser;
            }

            public boolean isIs_provider() {
                return is_provider;
            }

            public void setIs_provider(boolean is_provider) {
                this.is_provider = is_provider;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public Object getEmail() {
                return email;
            }

            public void setEmail(Object email) {
                this.email = email;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public Object getLanguage() {
                return language;
            }

            public void setLanguage(Object language) {
                this.language = language;
            }

            public int getInvited() {
                return invited;
            }

            public void setInvited(int invited) {
                this.invited = invited;
            }
        }
    }
}
