package com.knock.knocked.rest;

import com.knock.knocked.models.AvatarOut;
import com.knock.knocked.rest.models.AvatarInData;
import com.knock.knocked.rest.models.AvatarName;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PATCH;

public class RUserName extends RetrofitHandler {


    interface IModel {

        @PATCH("user/avatar/")
        Call<ResponseBody> upload(@Body AvatarName loginData);
    }


    public  static void Fire(AvatarName loginData, RetrofitHandler.apiResponseHandler<ResponseBody> responseHandler){
        Request(new RetrofitHandler.fireApi() {
            @Override
            public Call Fire() {
                return getClient(RUserName.IModel.class,context).upload(loginData);
            }
        }, responseHandler);
    }
}

