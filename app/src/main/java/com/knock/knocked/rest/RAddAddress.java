package com.knock.knocked.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.knock.knocked.models.AddAddressInData;
import com.knock.knocked.rest.models.RegisterData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class RAddAddress extends RetrofitHandler {



    interface IModel {

        @POST("user_favorite/")
        Call<ResponseBody> addAddress(@Body AddAddressInData addAddressInData);
    }


    public  static void Fire(final AddAddressInData addAddressInData, apiResponseHandler<ResponseBody> responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RAddAddress.IModel.class,context).addAddress(addAddressInData);
            }
        }, responseHandler);
    }
}
