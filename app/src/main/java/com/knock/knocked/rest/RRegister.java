package com.knock.knocked.rest;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.knock.knocked.rest.models.RegisterData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public class RRegister extends RetrofitHandler2 {

    public class Model {
        @SerializedName("status")
        @Expose
        public int status;
    }

    interface IModel {

        @POST("register/")
        Call<ResponseBody> register(@Body RegisterData registerData);
    }


    public  static void Fire(final RegisterData registerData,apiResponseHandler<ResponseBody> responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RRegister.IModel.class,context).register(registerData);
            }
        }, responseHandler);
    }
}