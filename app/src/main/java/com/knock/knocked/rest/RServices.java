package com.knock.knocked.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.knock.knocked.rest.models.ServicesData;
import retrofit2.Call;
import retrofit2.http.GET;


public class RServices extends RetrofitHandler {


    interface IModel {

        @GET("service/")
        Call<ServicesData> getServices();
    }


    public  static void Fire(apiResponseHandler<ServicesData>  responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RServices.IModel.class,context).getServices();
            }
        }, responseHandler);
    }
}