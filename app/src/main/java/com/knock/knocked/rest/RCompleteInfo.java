package com.knock.knocked.rest;

import com.knock.knocked.models.LoginOutData;
import com.knock.knocked.models.ProviderInfoInData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.POST;
import retrofit2.http.Path;

public class RCompleteInfo extends RetrofitHandler {


    interface IModel {

        @POST("provider/")
        Call<ResponseBody> completeInfo(@Body ProviderInfoInData data);
    }


    public  static void Fire(final ProviderInfoInData data, apiResponseHandler<ResponseBody> responseHandler){
        Request(new RetrofitHandler.fireApi() {
            @Override
            public Call Fire() {
                return getClient(RCompleteInfo.IModel.class,context).completeInfo(data);
            }
        }, responseHandler);
    }
}


