package com.knock.knocked.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.knock.knocked.models.ResendData;
import com.knock.knocked.rest.models.RegisterData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class RResendCode extends RetrofitHandler2 {

    public class Model {
        @SerializedName("status")
        @Expose
        public int status;
    }

    interface IModel {

        @POST("resend_verification_code/")
        Call<ResponseBody> resendCode(@Body ResendData resendData);
    }


    public  static void Fire(final ResendData resendData, apiResponseHandler<ResponseBody> responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RResendCode.IModel.class,context).resendCode(resendData);
            }
        }, responseHandler);
    }
}



