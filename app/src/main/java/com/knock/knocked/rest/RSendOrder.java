package com.knock.knocked.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.knock.knocked.models.ResendData;
import com.knock.knocked.rest.models.SendOrderInData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class RSendOrder extends RetrofitHandler {

    public class Model {
        @SerializedName("status")
        @Expose
        public int status;
    }

    interface IModel {

        @POST("order/")
        Call<ResponseBody> resendCode(@Body SendOrderInData resendData);
    }


    public  static void Fire(final SendOrderInData resendData, apiResponseHandler<ResponseBody> responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RSendOrder.IModel.class,context).resendCode(resendData);
            }
        }, responseHandler);
    }
}


