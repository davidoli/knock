package com.knock.knocked.rest.models;

import java.util.List;

public class ExpertData {

    /**
     * list : [{"id":111,"user":{"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$I4a197Ox15Rm$TOEFf3Oyw3qrIe1V8AvGjwhesBXeauHeQU4h6zQ/pxM=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":5},"service":{"id":10,"title":"Fitness Trainer","icon":"http://dev.hoonamapps.com/telemarket/media/icons/no-image.jpg","parent":null,"is_publish":true},"price":50000,"discount":3000,"is_virtual":false,"is_publish":true,"locations":[{"latitude":31.254,"longitude":57.659}],"languages":[{"id":3,"title":"Armenian","direction":"ltr"}],"description":"I believe sometime you were suffering this problem when refresh a recycle-view adapter when you come back from another activity. Typically we are doing restart the activity if there have a change in second activity","cybers":[{"kind":"i","link":"adf"}],"rate":1,"quality":1,"order_count":3,"likes":0},{"id":112,"user":{"id":42,"username":"09120345231","password":"pbkdf2_sha256$150000$BA5EQsNg2Ib9$oTogHoDpqhziT+DVbRdR6XG5qHjnG1jEluUL9fO1P4Y=","is_superuser":false,"is_provider":false,"first_name":"pedram","last_name":"mokarrami","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":41},"service":{"id":7,"title":"Car wash","icon":"http://dev.hoonamapps.com/telemarket/media/icons/no-image.jpg","parent":null,"is_publish":true},"price":0,"discount":0,"is_virtual":false,"is_publish":true,"locations":[],"languages":[],"description":"sdad","cybers":[{"id":42,"kind":"i","address":"asdads"},{"id":43,"kind":"w","address":"www.dsf.com"}],"rate":1,"quality":1,"order_count":0,"likes":0}]
     * total_page_num : 1
     * total_row : 10
     */

    private int total_page_num;
    private int total_row;
    private List<ListBean> list;

    public int getTotal_page_num() {
        return total_page_num;
    }

    public void setTotal_page_num(int total_page_num) {
        this.total_page_num = total_page_num;
    }

    public int getTotal_row() {
        return total_row;
    }

    public void setTotal_row(int total_row) {
        this.total_row = total_row;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 111
         * user : {"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$I4a197Ox15Rm$TOEFf3Oyw3qrIe1V8AvGjwhesBXeauHeQU4h6zQ/pxM=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":5}
         * service : {"id":10,"title":"Fitness Trainer","icon":"http://dev.hoonamapps.com/telemarket/media/icons/no-image.jpg","parent":null,"is_publish":true}
         * price : 50000
         * discount : 3000
         * is_virtual : false
         * is_publish : true
         * locations : [{"latitude":31.254,"longitude":57.659}]
         * languages : [{"id":3,"title":"Armenian","direction":"ltr"}]
         * description : I believe sometime you were suffering this problem when refresh a recycle-view adapter when you come back from another activity. Typically we are doing restart the activity if there have a change in second activity
         * cybers : [{"kind":"i","link":"adf"}]
         * rate : 1
         * quality : 1
         * order_count : 3
         * likes : 0
         */

        private int id;
        private UserBean user;
        private ServiceBean service;
        private int price;
        private int discount;
        private boolean is_virtual;
        private boolean is_publish;
        private String description;
        private int rate;
        private int quality;
        private int order_count;
        private int likes;
        private List<LocationsBean> locations;
        private List<LanguagesBean> languages;
        private List<CybersBean> cybers;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public ServiceBean getService() {
            return service;
        }

        public void setService(ServiceBean service) {
            this.service = service;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public int getDiscount() {
            return discount;
        }

        public void setDiscount(int discount) {
            this.discount = discount;
        }

        public boolean isIs_virtual() {
            return is_virtual;
        }

        public void setIs_virtual(boolean is_virtual) {
            this.is_virtual = is_virtual;
        }

        public boolean isIs_publish() {
            return is_publish;
        }

        public void setIs_publish(boolean is_publish) {
            this.is_publish = is_publish;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getRate() {
            return rate;
        }

        public void setRate(int rate) {
            this.rate = rate;
        }

        public int getQuality() {
            return quality;
        }

        public void setQuality(int quality) {
            this.quality = quality;
        }

        public int getOrder_count() {
            return order_count;
        }

        public void setOrder_count(int order_count) {
            this.order_count = order_count;
        }

        public int getLikes() {
            return likes;
        }

        public void setLikes(int likes) {
            this.likes = likes;
        }

        public List<LocationsBean> getLocations() {
            return locations;
        }

        public void setLocations(List<LocationsBean> locations) {
            this.locations = locations;
        }

        public List<LanguagesBean> getLanguages() {
            return languages;
        }

        public void setLanguages(List<LanguagesBean> languages) {
            this.languages = languages;
        }

        public List<CybersBean> getCybers() {
            return cybers;
        }

        public void setCybers(List<CybersBean> cybers) {
            this.cybers = cybers;
        }

        public static class UserBean {
            /**
             * id : 40
             * username : 09198048741
             * password : pbkdf2_sha256$150000$I4a197Ox15Rm$TOEFf3Oyw3qrIe1V8AvGjwhesBXeauHeQU4h6zQ/pxM=
             * is_superuser : false
             * is_provider : false
             * first_name : David
             * last_name : Kalami
             * avatar : http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg
             * email : null
             * gender : m
             * language : null
             * invited : 5
             */

            private int id;
            private String username;
            private String password;
            private boolean is_superuser;
            private boolean is_provider;
            private String first_name;
            private String last_name;
            private String avatar;
            private Object email;
            private String gender;
            private Object language;
            private int invited;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public boolean isIs_superuser() {
                return is_superuser;
            }

            public void setIs_superuser(boolean is_superuser) {
                this.is_superuser = is_superuser;
            }

            public boolean isIs_provider() {
                return is_provider;
            }

            public void setIs_provider(boolean is_provider) {
                this.is_provider = is_provider;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public Object getEmail() {
                return email;
            }

            public void setEmail(Object email) {
                this.email = email;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public Object getLanguage() {
                return language;
            }

            public void setLanguage(Object language) {
                this.language = language;
            }

            public int getInvited() {
                return invited;
            }

            public void setInvited(int invited) {
                this.invited = invited;
            }
        }

        public static class ServiceBean {
            /**
             * id : 10
             * title : Fitness Trainer
             * icon : http://dev.hoonamapps.com/telemarket/media/icons/no-image.jpg
             * parent : null
             * is_publish : true
             */

            private int id;
            private String title;
            private String icon;
            private Object parent;
            private boolean is_publish;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getIcon() {
                return icon;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }

            public Object getParent() {
                return parent;
            }

            public void setParent(Object parent) {
                this.parent = parent;
            }

            public boolean isIs_publish() {
                return is_publish;
            }

            public void setIs_publish(boolean is_publish) {
                this.is_publish = is_publish;
            }
        }

        public static class LocationsBean {
            /**
             * latitude : 31.254
             * longitude : 57.659
             */

            private double latitude;
            private double longitude;

            public double getLatitude() {
                return latitude;
            }

            public void setLatitude(double latitude) {
                this.latitude = latitude;
            }

            public double getLongitude() {
                return longitude;
            }

            public void setLongitude(double longitude) {
                this.longitude = longitude;
            }
        }

        public static class LanguagesBean {
            /**
             * id : 3
             * title : Armenian
             * direction : ltr
             */

            private int id;
            private String title;
            private String direction;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDirection() {
                return direction;
            }

            public void setDirection(String direction) {
                this.direction = direction;
            }
        }

        public static class CybersBean {
            /**
             * kind : i
             * link : adf
             */

            private String kind;
            private String link;

            public String getKind() {
                return kind;
            }

            public void setKind(String kind) {
                this.kind = kind;
            }

            public String getLink() {
                return link;
            }

            public void setLink(String link) {
                this.link = link;
            }
        }
    }
}
