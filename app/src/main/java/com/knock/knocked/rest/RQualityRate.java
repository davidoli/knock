package com.knock.knocked.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.knock.knocked.rest.models.QualityRateData;
import com.knock.knocked.rest.models.RegisterData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class RQualityRate extends RetrofitHandler {

    public class Model {
        @SerializedName("status")
        @Expose
        public int status;
    }

    interface IModel {

        @POST("qualification/quality/")
        Call<ResponseBody> rate(@Body QualityRateData registerData);
    }


    public  static void Fire(final QualityRateData registerData, apiResponseHandler<ResponseBody> responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RQualityRate.IModel.class,context).rate(registerData);
            }
        }, responseHandler);
    }
}