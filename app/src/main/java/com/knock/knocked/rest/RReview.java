package com.knock.knocked.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.knock.knocked.rest.models.RegisterData;
import com.knock.knocked.rest.models.ReviewInData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class RReview extends RetrofitHandler {



    interface IModel {

        @POST("qualification/review/")
        Call<ResponseBody> rate(@Body ReviewInData registerData);
    }


    public  static void Fire(final ReviewInData registerData, apiResponseHandler<ResponseBody> responseHandler){
        Request(new RetrofitHandler.fireApi() {
            @Override
            public Call Fire() {
                return getClient(RReview.IModel.class,context).rate(registerData);
            }
        }, responseHandler);
    }
}


