package com.knock.knocked.rest;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.knock.knocked.G;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by david on 7/31/2018.
 */

public abstract class RetrofitHandler {

    public static final String BASE_URL = "http://dev.hoonamapps.com/telemarket/api/v0/";   //http://192.168.10.56:7800
    private static final String CONTENT_TYPE = "application/json";
    public static Context context;


    private static Retrofit retrofit = null;


    public interface apiResponseHandler<apiModel>{
        void onSuccess(apiModel model);
        void onFail(int code, String message);
        void onFailure(Throwable t);
        void onException(Exception e);
    }


    protected  interface fireApi<apiModel>{
        Call<apiModel> Fire();
    }


    public static Retrofit getClient(String baseUrl) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okClient(context))
                    .build();
        }
        return retrofit;
    }


    private static OkHttpClient okClient(final Context context) {
        return new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request().newBuilder()
                                .addHeader("Accept", CONTENT_TYPE)
                                .addHeader("Content-Type", CONTENT_TYPE)
                                .addHeader("Authorization", getServerKey(context))
//                                .addHeader("X-CSRFToken", "bSGL6efPY1WPCDIDENBVSe7SWMvgyzDkvSbrApcLzSaltdVrqIA7zx40w42u8CJi")
                                .build();
                        return chain.proceed(request);
                    }
                })
                .build();
    }


    protected   static <apiInterface> apiInterface getClient(final Class<apiInterface> service,Context context2) {
        context = context2;
        return getClient(BASE_URL).create(service);
    }

    protected static <apiModel> void FireException(final apiResponseHandler<apiModel> responseHandler, Exception e){
        if(responseHandler != null) {
            responseHandler.onException(e);
        }
    }

    //abstract apiInterface getClient();
    protected  static <apiModel> void Request(fireApi fireApi, final apiResponseHandler<apiModel> responseHandler){
        try {

            //Create Service
            //apiInterface client = RetrofitHandler.getClient(service);
            //apiInterface mService = RetrofitClient.getClient(ApiUtils.BASE_URL).create(service);
            Call<apiModel> method = fireApi.Fire();

            //Execute Request
            method.enqueue(new Callback<apiModel>() {
                @Override
                public void onResponse(Call<apiModel> call, Response<apiModel> response) {
                    if (responseHandler != null) {
                        if (response.isSuccessful()) responseHandler.onSuccess(response.body());
                        else responseHandler.onFail(response.code(),response.errorBody().toString());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<apiModel> call, Throwable t) {
                    if (responseHandler != null) {
                        responseHandler.onFailure(t);
                    }
                }
            });
        } catch (Exception e) {
            if (responseHandler != null) {
                responseHandler.onException(e);
            }
        }

    }



    private static String getServerKey(Context context) {

        SharedPreferences settings = G.getContext().getSharedPreferences("me",
                Context.MODE_PRIVATE);
        String token = settings.getString("token", "0");
        String result="";
        if (token.equals("0"))
            result = "Bearer " + "";
        else
            result = "Bearer " + token;
        return result;


    }

    public void setContext(Context context) {
        this.context = context;
    }
}
