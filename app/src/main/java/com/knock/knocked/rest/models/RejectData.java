package com.knock.knocked.rest.models;

public class RejectData {
    private int order;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
