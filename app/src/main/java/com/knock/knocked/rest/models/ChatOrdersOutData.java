package com.knock.knocked.rest.models;

import java.util.List;

public class ChatOrdersOutData {


    private int total_page_num;
    private int total_row;
    private List<PenddingBean> pendding;
    private List<AcceptedBean> accepted;
    private List<RequestBean> request;

    public int getTotal_page_num() {
        return total_page_num;
    }

    public void setTotal_page_num(int total_page_num) {
        this.total_page_num = total_page_num;
    }

    public int getTotal_row() {
        return total_row;
    }

    public void setTotal_row(int total_row) {
        this.total_row = total_row;
    }

    public List<PenddingBean> getPendding() {
        return pendding;
    }

    public void setPendding(List<PenddingBean> pendding) {
        this.pendding = pendding;
    }

    public List<AcceptedBean> getAccepted() {
        return accepted;
    }

    public void setAccepted(List<AcceptedBean> accepted) {
        this.accepted = accepted;
    }

    public List<RequestBean> getRequest() {
        return request;
    }

    public void setRequest(List<RequestBean> request) {
        this.request = request;
    }

    public static class PenddingBean {


        private int id;
        private OwnerBean owner;
        private ProviderBean provider;
        private int price;
        private double latitude;
        private double longitude;
        private String request_date;
        private String order_date;
        private int order_time;
        private String accept_date;
        private String reject_date;
        private String owner_reject_date;
        private int status;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public OwnerBean getOwner() {
            return owner;
        }

        public void setOwner(OwnerBean owner) {
            this.owner = owner;
        }

        public ProviderBean getProvider() {
            return provider;
        }

        public void setProvider(ProviderBean provider) {
            this.provider = provider;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public String getRequest_date() {
            return request_date;
        }

        public void setRequest_date(String request_date) {
            this.request_date = request_date;
        }

        public String getOrder_date() {
            return order_date;
        }

        public void setOrder_date(String order_date) {
            this.order_date = order_date;
        }

        public int getOrder_time() {
            return order_time;
        }

        public void setOrder_time(int order_time) {
            this.order_time = order_time;
        }

        public String getAccept_date() {
            return accept_date;
        }

        public void setAccept_date(String accept_date) {
            this.accept_date = accept_date;
        }

        public String getReject_date() {
            return reject_date;
        }

        public void setReject_date(String reject_date) {
            this.reject_date = reject_date;
        }

        public String getOwner_reject_date() {
            return owner_reject_date;
        }

        public void setOwner_reject_date(String owner_reject_date) {
            this.owner_reject_date = owner_reject_date;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public static class OwnerBean {


            private int id;
            private String username;
            private String password;
            private boolean is_superuser;
            private boolean is_provider;
            private String first_name;
            private String last_name;
            private String avatar;
            private String email;
            private String gender;
            private String language;
            private int invited;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public boolean isIs_superuser() {
                return is_superuser;
            }

            public void setIs_superuser(boolean is_superuser) {
                this.is_superuser = is_superuser;
            }

            public boolean isIs_provider() {
                return is_provider;
            }

            public void setIs_provider(boolean is_provider) {
                this.is_provider = is_provider;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getLanguage() {
                return language;
            }

            public void setLanguage(String language) {
                this.language = language;
            }

            public int getInvited() {
                return invited;
            }

            public void setInvited(int invited) {
                this.invited = invited;
            }
        }

        public static class ProviderBean {


            private int id;
            private UserBean user;
            private ServiceBean service;
            private int price;
            private int discount;
            private boolean is_virtual;
            private boolean is_publish;
            private String description;
            private int job_done_time;
            private List<LocationsBean> locations;
            private List<LanguagesBean> languages;
            private List<?> cybers;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public UserBean getUser() {
                return user;
            }

            public void setUser(UserBean user) {
                this.user = user;
            }

            public ServiceBean getService() {
                return service;
            }

            public void setService(ServiceBean service) {
                this.service = service;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public int getDiscount() {
                return discount;
            }

            public void setDiscount(int discount) {
                this.discount = discount;
            }

            public boolean isIs_virtual() {
                return is_virtual;
            }

            public void setIs_virtual(boolean is_virtual) {
                this.is_virtual = is_virtual;
            }

            public boolean isIs_publish() {
                return is_publish;
            }

            public void setIs_publish(boolean is_publish) {
                this.is_publish = is_publish;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public int getJob_done_time() {
                return job_done_time;
            }

            public void setJob_done_time(int job_done_time) {
                this.job_done_time = job_done_time;
            }

            public List<LocationsBean> getLocations() {
                return locations;
            }

            public void setLocations(List<LocationsBean> locations) {
                this.locations = locations;
            }

            public List<LanguagesBean> getLanguages() {
                return languages;
            }

            public void setLanguages(List<LanguagesBean> languages) {
                this.languages = languages;
            }

            public List<?> getCybers() {
                return cybers;
            }

            public void setCybers(List<?> cybers) {
                this.cybers = cybers;
            }

            public static class UserBean {

                private int id;
                private String username;
                private String password;
                private boolean is_superuser;
                private boolean is_provider;
                private String first_name;
                private String last_name;
                private String avatar;
                private String email;
                private String gender;
                private String language;
                private int invited;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getPassword() {
                    return password;
                }

                public void setPassword(String password) {
                    this.password = password;
                }

                public boolean isIs_superuser() {
                    return is_superuser;
                }

                public void setIs_superuser(boolean is_superuser) {
                    this.is_superuser = is_superuser;
                }

                public boolean isIs_provider() {
                    return is_provider;
                }

                public void setIs_provider(boolean is_provider) {
                    this.is_provider = is_provider;
                }

                public String getFirst_name() {
                    return first_name;
                }

                public void setFirst_name(String first_name) {
                    this.first_name = first_name;
                }

                public String getLast_name() {
                    return last_name;
                }

                public void setLast_name(String last_name) {
                    this.last_name = last_name;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLanguage() {
                    return language;
                }

                public void setLanguage(String language) {
                    this.language = language;
                }

                public int getInvited() {
                    return invited;
                }

                public void setInvited(int invited) {
                    this.invited = invited;
                }
            }

            public static class ServiceBean {


                private int id;
                private String title;
                private String icon;
                private String parent;
                private boolean is_publish;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getIcon() {
                    return icon;
                }

                public void setIcon(String icon) {
                    this.icon = icon;
                }

                public String getParent() {
                    return parent;
                }

                public void setParent(String parent) {
                    this.parent = parent;
                }

                public boolean isIs_publish() {
                    return is_publish;
                }

                public void setIs_publish(boolean is_publish) {
                    this.is_publish = is_publish;
                }
            }

            public static class LocationsBean {


                private int id;
                private double latitude;
                private double longitude;
                private String address;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public double getLatitude() {
                    return latitude;
                }

                public void setLatitude(double latitude) {
                    this.latitude = latitude;
                }

                public double getLongitude() {
                    return longitude;
                }

                public void setLongitude(double longitude) {
                    this.longitude = longitude;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }
            }

            public static class LanguagesBean {

                private int id;
                private String title;
                private String direction;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getDirection() {
                    return direction;
                }

                public void setDirection(String direction) {
                    this.direction = direction;
                }
            }
        }
    }

    public static class AcceptedBean {

        private int id;
        private OwnerBeanX owner;
        private ProviderBeanX provider;
        private int price;
        private int latitude;
        private int longitude;
        private String request_date;
        private String order_date;
        private int order_time;
        private String accept_date;
        private String reject_date;
        private String owner_reject_date;
        private int status;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public OwnerBeanX getOwner() {
            return owner;
        }

        public void setOwner(OwnerBeanX owner) {
            this.owner = owner;
        }

        public ProviderBeanX getProvider() {
            return provider;
        }

        public void setProvider(ProviderBeanX provider) {
            this.provider = provider;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public int getLatitude() {
            return latitude;
        }

        public void setLatitude(int latitude) {
            this.latitude = latitude;
        }

        public int getLongitude() {
            return longitude;
        }

        public void setLongitude(int longitude) {
            this.longitude = longitude;
        }

        public String getRequest_date() {
            return request_date;
        }

        public void setRequest_date(String request_date) {
            this.request_date = request_date;
        }

        public String getOrder_date() {
            return order_date;
        }

        public void setOrder_date(String order_date) {
            this.order_date = order_date;
        }

        public int getOrder_time() {
            return order_time;
        }

        public void setOrder_time(int order_time) {
            this.order_time = order_time;
        }

        public String getAccept_date() {
            return accept_date;
        }

        public void setAccept_date(String accept_date) {
            this.accept_date = accept_date;
        }

        public String getReject_date() {
            return reject_date;
        }

        public void setReject_date(String reject_date) {
            this.reject_date = reject_date;
        }

        public String getOwner_reject_date() {
            return owner_reject_date;
        }

        public void setOwner_reject_date(String owner_reject_date) {
            this.owner_reject_date = owner_reject_date;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public static class OwnerBeanX {


            private int id;
            private String username;
            private String password;
            private boolean is_superuser;
            private boolean is_provider;
            private String first_name;
            private String last_name;
            private String avatar;
            private String email;
            private String gender;
            private String language;
            private int invited;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public boolean isIs_superuser() {
                return is_superuser;
            }

            public void setIs_superuser(boolean is_superuser) {
                this.is_superuser = is_superuser;
            }

            public boolean isIs_provider() {
                return is_provider;
            }

            public void setIs_provider(boolean is_provider) {
                this.is_provider = is_provider;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getLanguage() {
                return language;
            }

            public void setLanguage(String language) {
                this.language = language;
            }

            public int getInvited() {
                return invited;
            }

            public void setInvited(int invited) {
                this.invited = invited;
            }
        }

        public static class ProviderBeanX {


            private int id;
            private UserBeanX user;
            private ServiceBeanX service;
            private int price;
            private int discount;
            private boolean is_virtual;
            private boolean is_publish;
            private String description;
            private int job_done_time;
            private List<?> locations;
            private List<?> languages;
            private List<CybersBean> cybers;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public UserBeanX getUser() {
                return user;
            }

            public void setUser(UserBeanX user) {
                this.user = user;
            }

            public ServiceBeanX getService() {
                return service;
            }

            public void setService(ServiceBeanX service) {
                this.service = service;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public int getDiscount() {
                return discount;
            }

            public void setDiscount(int discount) {
                this.discount = discount;
            }

            public boolean isIs_virtual() {
                return is_virtual;
            }

            public void setIs_virtual(boolean is_virtual) {
                this.is_virtual = is_virtual;
            }

            public boolean isIs_publish() {
                return is_publish;
            }

            public void setIs_publish(boolean is_publish) {
                this.is_publish = is_publish;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public int getJob_done_time() {
                return job_done_time;
            }

            public void setJob_done_time(int job_done_time) {
                this.job_done_time = job_done_time;
            }

            public List<?> getLocations() {
                return locations;
            }

            public void setLocations(List<?> locations) {
                this.locations = locations;
            }

            public List<?> getLanguages() {
                return languages;
            }

            public void setLanguages(List<?> languages) {
                this.languages = languages;
            }

            public List<CybersBean> getCybers() {
                return cybers;
            }

            public void setCybers(List<CybersBean> cybers) {
                this.cybers = cybers;
            }

            public static class UserBeanX {


                private int id;
                private String username;
                private String password;
                private boolean is_superuser;
                private boolean is_provider;
                private String first_name;
                private String last_name;
                private String avatar;
                private String email;
                private String gender;
                private String language;
                private int invited;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getPassword() {
                    return password;
                }

                public void setPassword(String password) {
                    this.password = password;
                }

                public boolean isIs_superuser() {
                    return is_superuser;
                }

                public void setIs_superuser(boolean is_superuser) {
                    this.is_superuser = is_superuser;
                }

                public boolean isIs_provider() {
                    return is_provider;
                }

                public void setIs_provider(boolean is_provider) {
                    this.is_provider = is_provider;
                }

                public String getFirst_name() {
                    return first_name;
                }

                public void setFirst_name(String first_name) {
                    this.first_name = first_name;
                }

                public String getLast_name() {
                    return last_name;
                }

                public void setLast_name(String last_name) {
                    this.last_name = last_name;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLanguage() {
                    return language;
                }

                public void setLanguage(String language) {
                    this.language = language;
                }

                public int getInvited() {
                    return invited;
                }

                public void setInvited(int invited) {
                    this.invited = invited;
                }
            }

            public static class ServiceBeanX {


                private int id;
                private String title;
                private String icon;
                private String parent;
                private boolean is_publish;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getIcon() {
                    return icon;
                }

                public void setIcon(String icon) {
                    this.icon = icon;
                }

                public String getParent() {
                    return parent;
                }

                public void setParent(String parent) {
                    this.parent = parent;
                }

                public boolean isIs_publish() {
                    return is_publish;
                }

                public void setIs_publish(boolean is_publish) {
                    this.is_publish = is_publish;
                }
            }

            public static class CybersBean {
                /**
                 * id : 42
                 * kind : i
                 * address : asdads
                 */

                private int id;
                private String kind;
                private String address;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getKind() {
                    return kind;
                }

                public void setKind(String kind) {
                    this.kind = kind;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }
            }
        }
    }

    public static class RequestBean {

        private int id;
        private OwnerBeanXX owner;
        private ProviderBeanXX provider;
        private int price;
        private int latitude;
        private int longitude;
        private String request_date;
        private String order_date;
        private int order_time;
        private String accept_date;
        private String reject_date;
        private String owner_reject_date;
        private int status;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public OwnerBeanXX getOwner() {
            return owner;
        }

        public void setOwner(OwnerBeanXX owner) {
            this.owner = owner;
        }

        public ProviderBeanXX getProvider() {
            return provider;
        }

        public void setProvider(ProviderBeanXX provider) {
            this.provider = provider;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public int getLatitude() {
            return latitude;
        }

        public void setLatitude(int latitude) {
            this.latitude = latitude;
        }

        public int getLongitude() {
            return longitude;
        }

        public void setLongitude(int longitude) {
            this.longitude = longitude;
        }

        public String getRequest_date() {
            return request_date;
        }

        public void setRequest_date(String request_date) {
            this.request_date = request_date;
        }

        public String getOrder_date() {
            return order_date;
        }

        public void setOrder_date(String order_date) {
            this.order_date = order_date;
        }

        public int getOrder_time() {
            return order_time;
        }

        public void setOrder_time(int order_time) {
            this.order_time = order_time;
        }

        public String getAccept_date() {
            return accept_date;
        }

        public void setAccept_date(String accept_date) {
            this.accept_date = accept_date;
        }

        public String getReject_date() {
            return reject_date;
        }

        public void setReject_date(String reject_date) {
            this.reject_date = reject_date;
        }

        public String getOwner_reject_date() {
            return owner_reject_date;
        }

        public void setOwner_reject_date(String owner_reject_date) {
            this.owner_reject_date = owner_reject_date;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public static class OwnerBeanXX {


            private int id;
            private String username;
            private String password;
            private boolean is_superuser;
            private boolean is_provider;
            private String first_name;
            private String last_name;
            private String avatar;
            private String email;
            private String gender;
            private String language;
            private int invited;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public boolean isIs_superuser() {
                return is_superuser;
            }

            public void setIs_superuser(boolean is_superuser) {
                this.is_superuser = is_superuser;
            }

            public boolean isIs_provider() {
                return is_provider;
            }

            public void setIs_provider(boolean is_provider) {
                this.is_provider = is_provider;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getLanguage() {
                return language;
            }

            public void setLanguage(String language) {
                this.language = language;
            }

            public int getInvited() {
                return invited;
            }

            public void setInvited(int invited) {
                this.invited = invited;
            }
        }

        public static class ProviderBeanXX {


            private int id;
            private UserBeanXX user;
            private ServiceBeanXX service;
            private int price;
            private int discount;
            private boolean is_virtual;
            private boolean is_publish;
            private String description;
            private int job_done_time;
            private List<?> locations;
            private List<?> languages;
            private List<CybersBeanX> cybers;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public UserBeanXX getUser() {
                return user;
            }

            public void setUser(UserBeanXX user) {
                this.user = user;
            }

            public ServiceBeanXX getService() {
                return service;
            }

            public void setService(ServiceBeanXX service) {
                this.service = service;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public int getDiscount() {
                return discount;
            }

            public void setDiscount(int discount) {
                this.discount = discount;
            }

            public boolean isIs_virtual() {
                return is_virtual;
            }

            public void setIs_virtual(boolean is_virtual) {
                this.is_virtual = is_virtual;
            }

            public boolean isIs_publish() {
                return is_publish;
            }

            public void setIs_publish(boolean is_publish) {
                this.is_publish = is_publish;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public int getJob_done_time() {
                return job_done_time;
            }

            public void setJob_done_time(int job_done_time) {
                this.job_done_time = job_done_time;
            }

            public List<?> getLocations() {
                return locations;
            }

            public void setLocations(List<?> locations) {
                this.locations = locations;
            }

            public List<?> getLanguages() {
                return languages;
            }

            public void setLanguages(List<?> languages) {
                this.languages = languages;
            }

            public List<CybersBeanX> getCybers() {
                return cybers;
            }

            public void setCybers(List<CybersBeanX> cybers) {
                this.cybers = cybers;
            }

            public static class UserBeanXX {

                private int id;
                private String username;
                private String password;
                private boolean is_superuser;
                private boolean is_provider;
                private String first_name;
                private String last_name;
                private String avatar;
                private String email;
                private String gender;
                private String language;
                private int invited;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getPassword() {
                    return password;
                }

                public void setPassword(String password) {
                    this.password = password;
                }

                public boolean isIs_superuser() {
                    return is_superuser;
                }

                public void setIs_superuser(boolean is_superuser) {
                    this.is_superuser = is_superuser;
                }

                public boolean isIs_provider() {
                    return is_provider;
                }

                public void setIs_provider(boolean is_provider) {
                    this.is_provider = is_provider;
                }

                public String getFirst_name() {
                    return first_name;
                }

                public void setFirst_name(String first_name) {
                    this.first_name = first_name;
                }

                public String getLast_name() {
                    return last_name;
                }

                public void setLast_name(String last_name) {
                    this.last_name = last_name;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLanguage() {
                    return language;
                }

                public void setLanguage(String language) {
                    this.language = language;
                }

                public int getInvited() {
                    return invited;
                }

                public void setInvited(int invited) {
                    this.invited = invited;
                }
            }

            public static class ServiceBeanXX {

                private int id;
                private String title;
                private String icon;
                private String parent;
                private boolean is_publish;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getIcon() {
                    return icon;
                }

                public void setIcon(String icon) {
                    this.icon = icon;
                }

                public String getParent() {
                    return parent;
                }

                public void setParent(String parent) {
                    this.parent = parent;
                }

                public boolean isIs_publish() {
                    return is_publish;
                }

                public void setIs_publish(boolean is_publish) {
                    this.is_publish = is_publish;
                }
            }

            public static class CybersBeanX {
                /**
                 * id : 42
                 * kind : i
                 * address : asdads
                 */

                private int id;
                private String kind;
                private String address;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getKind() {
                    return kind;
                }

                public void setKind(String kind) {
                    this.kind = kind;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }
            }
        }
    }
}
