package com.knock.knocked.rest;

import com.knock.knocked.models.AllChatsOutData;
import com.knock.knocked.rest.models.ChatListDataOut;

import retrofit2.Call;
import retrofit2.http.GET;

public class RChatList extends RetrofitHandler {


    interface IModel {

        @GET("chat/message/users/")
        Call<ChatListDataOut> getlist();
    }


    public  static void Fire(apiResponseHandler<ChatListDataOut>  responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RChatList.IModel.class,context).getlist();
            }
        }, responseHandler);
    }
}




