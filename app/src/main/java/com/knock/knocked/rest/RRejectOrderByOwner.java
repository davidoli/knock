package com.knock.knocked.rest;

import com.knock.knocked.rest.models.RejectData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PATCH;

public class RRejectOrderByOwner extends RetrofitHandler {



    interface IModel {

        @PATCH("order/owner_reject/")
        Call<ResponseBody> reject(@Body RejectData rejectData);
    }


    public  static void Fire(final RejectData rejectData, apiResponseHandler<ResponseBody> responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RRejectOrderByOwner.IModel.class,context).reject(rejectData);
            }
        }, responseHandler);
    }
}
