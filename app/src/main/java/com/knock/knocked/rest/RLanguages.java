package com.knock.knocked.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.knock.knocked.models.LanguageData;
import com.knock.knocked.rest.models.ServicesData;

import retrofit2.Call;
import retrofit2.http.GET;

public class RLanguages extends RetrofitHandler {


    interface IModel {

        @GET("language/")
        Call<LanguageData> getServices();
    }


    public  static void Fire(apiResponseHandler<LanguageData>  responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RLanguages.IModel.class,context).getServices();
            }
        }, responseHandler);
    }
}