package com.knock.knocked.rest;

import com.knock.knocked.models.LanguageData;
import com.knock.knocked.models.MyInfoData;

import retrofit2.Call;
import retrofit2.http.GET;

public class RMyInfo extends RetrofitHandler {


    interface IModel {

        @GET("user/me/")
        Call<MyInfoData> getInfo();
    }


    public  static void Fire(apiResponseHandler<MyInfoData>  responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RMyInfo.IModel.class,context).getInfo();
            }
        }, responseHandler);
    }
}