package com.knock.knocked.rest;

import com.knock.knocked.models.LoginData;
import com.knock.knocked.models.LoginOutData;
import com.knock.knocked.models.ResendData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class RInviteFriend extends RetrofitHandler {


    interface IModel {
        @POST("user_friend/")
        Call<ResponseBody> invite(@Body ResendData data);
    }

    public static void Fire(ResendData data, apiResponseHandler<ResponseBody> responseHandler) {
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RInviteFriend.IModel.class, context).invite(data);
            }
        }, responseHandler);
    }
}
