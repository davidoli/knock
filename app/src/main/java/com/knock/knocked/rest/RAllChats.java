package com.knock.knocked.rest;

import com.knock.knocked.models.AllChatsOutData;
import com.knock.knocked.models.MyInfoData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.Path;

public class RAllChats extends RetrofitHandler {


    interface IModel {

        @GET("chat/message/{id}/")
        Call<AllChatsOutData> getChats(@Path("id") int id);
    }


    public  static void Fire(final int id,apiResponseHandler<AllChatsOutData>  responseHandler){
        Request(new fireApi() {
            @Override
            public Call Fire() {
                return getClient(RAllChats.IModel.class,context).getChats(id);
            }
        }, responseHandler);
    }
}


