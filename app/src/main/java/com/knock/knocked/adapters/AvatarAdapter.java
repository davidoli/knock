package com.knock.knocked.adapters;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.knock.knocked.R;
import com.knock.knocked.activities.CompleteUserInfoActivity;
import com.knock.knocked.listeners.AvatarRecyclerViewListener;
import com.knock.knocked.listeners.WorkingHoursRecyclerViewListener;
import com.knock.knocked.models.AvatarModel;
import com.knock.knocked.models.WorkingHourModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AvatarAdapter extends RecyclerView.Adapter<AvatarAdapter.MyViewHolder> {
    private List<AvatarModel> avatarList;
    private Context context;
    private int total;
    private AvatarRecyclerViewListener mListener;
    private final int TYPE_ITEM = 1;
    private final int TYPE_FOOTER = 2;
    private final int NO_ITEM = 0;
    private int totalPages;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView ivAvatar;
        private AvatarRecyclerViewListener mListener;


        public MyViewHolder(View view, AvatarRecyclerViewListener listener) {
            super(view);

            ivAvatar = (ImageView) view.findViewById(R.id.iv_avatar);


            mListener = listener;
            view.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                default:
                    try {
                        mListener.onClick(v, getAdapterPosition(), avatarList.get(getAdapterPosition()),"plus");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }

        }
    }
// in this adaper constructor we add the list of messages as a parameter so that
// we will passe  it when making an instance of the adapter object in our activity


    public AvatarAdapter(List<AvatarModel> MessagesList, Context context, AvatarRecyclerViewListener listener,int totalc) {

        this.avatarList = MessagesList;
        this.context = context;
        this.mListener = listener;
        this.totalPages = totalc;


    }

    @Override
    public int getItemCount() {
//        return avatarList.size()+1;
        return avatarList.size();
    }

    @Override
    public AvatarAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;
//        if (viewType == TYPE_ITEM) {
        if (true) {

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_avatar, parent, false);
            return new AvatarAdapter.MyViewHolder(itemView, mListener);
        }



        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");

//        return new AvatarAdapter.MyViewHolder(itemView, mListener);
    }

    @Override
    public void onBindViewHolder(final AvatarAdapter.MyViewHolder holder, final int position) {

        //binding the data from our ArrayList of object to the item.xml using the viewholder


        if (holder instanceof AvatarAdapter.FooterViewHolder) {

        }
        else {
            switch (position)
            {
                case 0:
                    Glide.with(context)
                            .load(R.drawable.avatar1)  //http://karmento.ir      http://karmento.ir
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                            .fitCenter()//this method help to fit image into center of your ImageView
                            .into(holder.ivAvatar);
                    break;
                case 1:
                    Glide.with(context)
                            .load(R.drawable.avatar2)  //http://karmento.ir      http://karmento.ir
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                            .fitCenter()//this method help to fit image into center of your ImageView
                            .into(holder.ivAvatar);
                    break;
                case 2:
                    Glide.with(context)
                            .load(R.drawable.avatar3)  //http://karmento.ir      http://karmento.ir
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                            .fitCenter()//this method help to fit image into center of your ImageView
                            .into(holder.ivAvatar);
                    break;
                case 3:
                    Glide.with(context)
                            .load(R.drawable.avatar4)  //http://karmento.ir      http://karmento.ir
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                            .fitCenter()//this method help to fit image into center of your ImageView
                            .into(holder.ivAvatar);
                    break;
                case 4:
                    Glide.with(context)
                            .load(R.drawable.avatar5)  //http://karmento.ir      http://karmento.ir
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                            .fitCenter()//this method help to fit image into center of your ImageView
                            .into(holder.ivAvatar);
                    break;
                case 5:
                    Glide.with(context)
                            .load(R.drawable.avatar6)  //http://karmento.ir      http://karmento.ir
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                            .fitCenter()//this method help to fit image into center of your ImageView
                            .into(holder.ivAvatar);
                    break;
                case 6:
                    Glide.with(context)
                            .load(R.drawable.avatar7)  //http://karmento.ir      http://karmento.ir
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                            .fitCenter()//this method help to fit image into center of your ImageView
                            .into(holder.ivAvatar);
                    break;
                case 7:
                    Glide.with(context)
                            .load(R.drawable.avatar8)  //http://karmento.ir      http://karmento.ir
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                            .fitCenter()//this method help to fit image into center of your ImageView
                            .into(holder.ivAvatar);
                    break;
                case 8:
                    Glide.with(context)
                            .load(R.drawable.avatar9)  //http://karmento.ir      http://karmento.ir
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                            .fitCenter()//this method help to fit image into center of your ImageView
                            .into(holder.ivAvatar);
                    break;
                case 9:
                    Glide.with(context)
                            .load(R.drawable.avatar10)  //http://karmento.ir      http://karmento.ir
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                            .fitCenter()//this method help to fit image into center of your ImageView
                            .into(holder.ivAvatar);
                    break;
                case 10:
                    Glide.with(context)
                            .load(R.drawable.avatar11)  //http://karmento.ir      http://karmento.ir
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                            .fitCenter()//this method help to fit image into center of your ImageView
                            .into(holder.ivAvatar);
                    break;
                case 11:
                    Glide.with(context)
                            .load(R.drawable.avatar12)  //http://karmento.ir      http://karmento.ir
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                            .fitCenter()//this method help to fit image into center of your ImageView
                            .into(holder.ivAvatar);
                    break;

            }

        }

    }

    @Override
    public int getItemViewType(int position) {


//        if (isEndOfList(position))
//            return NO_ITEM;
//        else if (isPositionFooter(position)) {
//            return TYPE_FOOTER;
//        } else
        return TYPE_ITEM;
    }


    public class FooterViewHolder extends AvatarAdapter.MyViewHolder {
        public android.view.View View;

        public FooterViewHolder(android.view.View v) {
            super(v, mListener);
            View = v;
            // Add your UI Components here
        }

    }


    private boolean isPositionFooter(int position) {
        return position > avatarList.size()-1 ;
    }

    private boolean isEndOfList(int position) {


        if (avatarList.size() == 0)
            return true;

        if (avatarList.size() % 10 == 0) {
            if (position == totalPages * 10)
                return true;

        } else if (totalPages == 1) {
            if (position == avatarList.size())
                return true;
            else
                return false;
        } else {
            if (avatarList.size() % 10 != 0)
                if (position == avatarList.size())
                    return true;
                else
                    return totalPages * 10 == position;
        }
        return false;
    }


    private void startAnimation(View view,int pos,int type)
    {
        if(type == 1) {
            if (pos % 2 != 0) {
                ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", -30f);
                animation.setDuration(500);
                animation.start();

                ObjectAnimator animation2 = ObjectAnimator.ofFloat(view, "translationY", -30f);
                animation2.setDuration(500);
                animation2.start();

                ViewCompat.setElevation(view, 20);
            } else {
                ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", 30f);
                animation.setDuration(500);
                animation.start();

                ObjectAnimator animation2 = ObjectAnimator.ofFloat(view, "translationY", -30f);
                animation2.setDuration(500);
                animation2.start();

                ViewCompat.setElevation(view, 20);
            }
        }
        else
        {
            if (pos % 2 != 0) {
                ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", 0f);
                animation.setDuration(500);
                animation.start();

                ObjectAnimator animation2 = ObjectAnimator.ofFloat(view, "translationY", 0f);
                animation2.setDuration(500);
                animation2.start();

                ViewCompat.setElevation(view, 0);
            } else {
                ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", 0f);
                animation.setDuration(500);
                animation.start();

                ObjectAnimator animation2 = ObjectAnimator.ofFloat(view, "translationY", 0f);
                animation2.setDuration(500);
                animation2.start();

                ViewCompat.setElevation(view, 0);
            }
        }

    }

}