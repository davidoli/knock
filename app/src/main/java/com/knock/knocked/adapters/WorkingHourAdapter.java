package com.knock.knocked.adapters;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.knock.knocked.R;
import com.knock.knocked.listeners.WorkingHoursRecyclerViewListener;
import com.knock.knocked.models.WorkingHourModel;
import java.util.List;

public class WorkingHourAdapter extends RecyclerView.Adapter<WorkingHourAdapter.MyViewHolder> {
    private List<WorkingHourModel> workingHoursList;
    private Context context;
    private int total;
    private WorkingHoursRecyclerViewListener mListener;
    private final int TYPE_ITEM = 1;
    private final int TYPE_FOOTER = 2;
    private final int NO_ITEM = 0;
    private int totalPages;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTime;
        public TextView tvStatus;
        public int status;
        private WorkingHoursRecyclerViewListener mListener;


        public MyViewHolder(View view, WorkingHoursRecyclerViewListener listener) {
            super(view);

            tvTime = (TextView) view.findViewById(R.id.tv_time);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);


            mListener = listener;
            view.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                default:
                    mListener.onClick(v, getAdapterPosition(), workingHoursList.get(getAdapterPosition()),"plus");
                    break;
            }

        }
    }
// in this adaper constructor we add the list of messages as a parameter so that
// we will passe  it when making an instance of the adapter object in our activity


    public WorkingHourAdapter(List<WorkingHourModel> MessagesList, Context context, WorkingHoursRecyclerViewListener listener,int totalc) {

        this.workingHoursList = MessagesList;
        this.context = context;
        this.mListener = listener;
        this.totalPages = totalc;


    }

    @Override
    public int getItemCount() {
//        return workingHoursList.size()+1;
        return workingHoursList.size();
    }

    @Override
    public WorkingHourAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;
//        if (viewType == TYPE_ITEM) {
        if (true) {

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_working_hour, parent, false);
            return new WorkingHourAdapter.MyViewHolder(itemView, mListener);
        }

//        else if (viewType == TYPE_FOOTER) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_loading,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        } else if (viewType == NO_ITEM) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_item,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");

//        return new WorkingHourAdapter.MyViewHolder(itemView, mListener);
    }

    @Override
    public void onBindViewHolder(final WorkingHourAdapter.MyViewHolder holder, final int position) {

        //binding the data from our ArrayList of object to the item.xml using the viewholder


        if (holder instanceof WorkingHourAdapter.FooterViewHolder) {

        }
        else {
            holder.tvTime.setText(workingHoursList.get(position).getTime());
            holder.tvStatus.setText(workingHoursList.get(position).getStatus());
            if(workingHoursList.get(position).getStat() == 0)
                startAnimation(holder.itemView,position,0);
            else
                startAnimation(holder.itemView,position,1);
        }

    }

    @Override
    public int getItemViewType(int position) {


//        if (isEndOfList(position))
//            return NO_ITEM;
//        else if (isPositionFooter(position)) {
//            return TYPE_FOOTER;
//        } else
        return TYPE_ITEM;
    }


    public class FooterViewHolder extends WorkingHourAdapter.MyViewHolder {
        public android.view.View View;

        public FooterViewHolder(android.view.View v) {
            super(v, mListener);
            View = v;
            // Add your UI Components here
        }

    }


    private boolean isPositionFooter(int position) {
        return position > workingHoursList.size()-1 ;
    }

    private boolean isEndOfList(int position) {


        if (workingHoursList.size() == 0)
            return true;

        if (workingHoursList.size() % 10 == 0) {
            if (position == totalPages * 10)
                return true;

        } else if (totalPages == 1) {
            if (position == workingHoursList.size())
                return true;
            else
                return false;
        } else {
            if (workingHoursList.size() % 10 != 0)
                if (position == workingHoursList.size())
                    return true;
                else
                    return totalPages * 10 == position;
        }
        return false;
    }


    private void startAnimation(View view,int pos,int type)
    {
        if(type == 1) {
            if (pos % 2 != 0) {
                ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", -30f);
                animation.setDuration(500);
                animation.start();

                ObjectAnimator animation2 = ObjectAnimator.ofFloat(view, "translationY", -30f);
                animation2.setDuration(500);
                animation2.start();

                ViewCompat.setElevation(view, 20);
            } else {
                ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", 30f);
                animation.setDuration(500);
                animation.start();

                ObjectAnimator animation2 = ObjectAnimator.ofFloat(view, "translationY", -30f);
                animation2.setDuration(500);
                animation2.start();

                ViewCompat.setElevation(view, 20);
            }
        }
        else
        {
            if (pos % 2 != 0) {
                ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", 0f);
                animation.setDuration(500);
                animation.start();

                ObjectAnimator animation2 = ObjectAnimator.ofFloat(view, "translationY", 0f);
                animation2.setDuration(500);
                animation2.start();

                ViewCompat.setElevation(view, 0);
            } else {
                ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", 0f);
                animation.setDuration(500);
                animation.start();

                ObjectAnimator animation2 = ObjectAnimator.ofFloat(view, "translationY", 0f);
                animation2.setDuration(500);
                animation2.start();

                ViewCompat.setElevation(view, 0);
            }
        }

    }

}