package com.knock.knocked.adapters;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.knock.knocked.R;
import com.knock.knocked.activities.CalendarActivity;
import com.knock.knocked.listeners.VisitRecyclerViewListener;
import com.knock.knocked.listeners.WorkingHoursRecyclerViewListener;
import com.knock.knocked.models.CalendarVisitModel;
import com.knock.knocked.models.WorkingHourModel;

import java.util.ArrayList;
import java.util.List;

public class CalendarVisitAdapter extends RecyclerView.Adapter<CalendarVisitAdapter.MyViewHolder> {
    private List<CalendarVisitModel> workingHoursList;
    private Context context;
    private int total;
    private VisitRecyclerViewListener mListener;
    private final int TYPE_ITEM = 1;
    private final int TYPE_FOOTER = 2;
    private final int NO_ITEM = 0;
    private int totalPages;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTime;
        public TextView tvLocation;
        public TextView tvName;
        public ImageView ivProfile;
        public int status;
        private VisitRecyclerViewListener mListener;


        public MyViewHolder(View view, VisitRecyclerViewListener listener) {
            super(view);

            tvTime = (TextView) view.findViewById(R.id.tv_time);
            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvLocation = (TextView) view.findViewById(R.id.tv_location);
            ivProfile = (ImageView) view.findViewById(R.id.civ_prof);

            mListener = listener;
            view.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                default:
                    mListener.onClick(v, getAdapterPosition(), workingHoursList.get(getAdapterPosition()),"plus");
                    break;
            }

        }
    }
// in this adaper constructor we add the list of messages as a parameter so that
// we will passe  it when making an instance of the adapter object in our activity


    public CalendarVisitAdapter(List<CalendarVisitModel> MessagesList, Context context, VisitRecyclerViewListener listener, int totalc) {

        this.workingHoursList = MessagesList;
        this.context = context;
        this.mListener = listener;
        this.totalPages = totalc;


    }

    @Override
    public int getItemCount() {
//        return workingHoursList.size()+1;
        return workingHoursList.size();
    }

    @Override
    public CalendarVisitAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;
//        if (viewType == TYPE_ITEM) {
        if (true) {

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_calendar_visit, parent, false);
            return new CalendarVisitAdapter.MyViewHolder(itemView, mListener);
        }

//        else if (viewType == TYPE_FOOTER) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_loading,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        } else if (viewType == NO_ITEM) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_item,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");

//        return new CalendarVisitAdapter.MyViewHolder(itemView, mListener);
    }

    @Override
    public void onBindViewHolder(final CalendarVisitAdapter.MyViewHolder holder, final int position) {

        //binding the data from our ArrayList of object to the item.xml using the viewholder


        if (holder instanceof CalendarVisitAdapter.FooterViewHolder) {

        }
        else {
            holder.tvName.setText(workingHoursList.get(position).getName());
//            holder.tvLocation.setText(","+workingHoursList.get(position).getLocation());

            Glide.with(context)
                    .load(workingHoursList.get(position).getPic())  //http://karmento.ir      http://karmento.ir
                    .placeholder(R.mipmap.profile)
                    .dontAnimate()
                    .error(R.mipmap.profile)//in case of any glide exception or not able to download then this image will be appear . if you won't mention this error() then nothing to worry placeHolder image would be remain as it is.
                    .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                    .fitCenter()//this method help to fit image into center of your ImageView
                    .into(holder.ivProfile);


            switch (workingHoursList.get(position).getDate())
            {
                case "1":
                    holder.tvTime.setText("0 - 2 AM");
                    break;
                case "2":
                    holder.tvTime.setText("2 - 4 AM");
                    break;
                case "3":
                    holder.tvTime.setText("4 - 6 AM");
                    break;
                case "4":
                    holder.tvTime.setText("6 - 8 AM");
                    break;
                case "5":
                    holder.tvTime.setText("8 - 10 AM");
                    break;
                case "6":
                    holder.tvTime.setText("10 - 12 PM");
                    break;
                case "7":
                    holder.tvTime.setText("12 - 14 PM");
                    break;
                case "8":
                    holder.tvTime.setText("14 - 16 PM");
                    break;
                case "9":
                    holder.tvTime.setText("16 - 18 PM");
                    break;
                case "10":
                    holder.tvTime.setText("18 - 20 PM");
                    break;
                case "11":
                    holder.tvTime.setText("20 - 22 PM");
                    break;
                case "12":
                    holder.tvTime.setText("22 - 24 PM");
                    break;

            }
        }

    }

    @Override
    public int getItemViewType(int position) {


//        if (isEndOfList(position))
//            return NO_ITEM;
//        else if (isPositionFooter(position)) {
//            return TYPE_FOOTER;
//        } else
        return TYPE_ITEM;
    }


    public class FooterViewHolder extends CalendarVisitAdapter.MyViewHolder {
        public android.view.View View;

        public FooterViewHolder(android.view.View v) {
            super(v, mListener);
            View = v;
            // Add your UI Components here
        }

    }


    private boolean isPositionFooter(int position) {
        return position > workingHoursList.size()-1 ;
    }

    private boolean isEndOfList(int position) {


        if (workingHoursList.size() == 0)
            return true;

        if (workingHoursList.size() % 10 == 0) {
            if (position == totalPages * 10)
                return true;

        } else if (totalPages == 1) {
            if (position == workingHoursList.size())
                return true;
            else
                return false;
        } else {
            if (workingHoursList.size() % 10 != 0)
                if (position == workingHoursList.size())
                    return true;
                else
                    return totalPages * 10 == position;
        }
        return false;
    }


}