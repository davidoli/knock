package com.knock.knocked.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.knock.knocked.R;
import com.knock.knocked.listeners.ContactRecyclerViewListener;
import com.knock.knocked.models.ContactModel;

import java.util.List;


public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {
    private List<ContactModel> MessageList;
    private Context context;
    private ContactRecyclerViewListener mListener;
    private final int TYPE_ITEM = 1;
    private final int TYPE_2 = 2;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvName,tvPhone,tvInvite;
        public ImageView ivRemove,ivAdd;
        private ContactRecyclerViewListener mListener;


        public MyViewHolder(View view, ContactRecyclerViewListener listener) {
            super(view);



            ivAdd = (ImageView) view.findViewById(R.id.iv_add);
            ivRemove = (ImageView) view.findViewById(R.id.iv_remove);
            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvInvite = (TextView) view.findViewById(R.id.tv_invite);
            tvPhone = (TextView) view.findViewById(R.id.tv_phone);


            mListener = listener;
            ivAdd.setOnClickListener(this);
            ivRemove.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iv_add:
                    mListener.onClick(v, getAdapterPosition(), MessageList.get(getAdapterPosition()), "add");
                    break;
                case R.id.iv_remove:
                    mListener.onClick(v, getAdapterPosition(), MessageList.get(getAdapterPosition()), "remove");
                    break;

            }

        }
    }
// in this adaper constructor we add the list of messages as a parameter so that
// we will passe  it when making an instance of the adapter object in our activity


    public ContactAdapter(List<ContactModel> MessagesList, Context context, ContactRecyclerViewListener listener) {

        this.MessageList = MessagesList;
        this.context = context;
        this.mListener = listener;


    }

    @Override
    public int getItemCount() {
        return MessageList.size();
    }

    @Override
    public ContactAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = null;
        if (viewType == TYPE_ITEM) {

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_contact1, parent, false);
            return new ContactAdapter.MyViewHolder(itemView, mListener);
        } else if (viewType == TYPE_2) {



        }


        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(final ContactAdapter.MyViewHolder holder, final int position) {


        if (holder instanceof ContactAdapter.FooterViewHolder) {
            ((FooterViewHolder) holder).tvChar.setText(MessageList.get(position).getName());

            //your code here
        } else if (holder instanceof ContactAdapter.MyViewHolder) {
            holder.tvPhone.setText(MessageList.get(position).getPhone());
            holder.tvName.setText(MessageList.get(position).getName());
            if(MessageList.get(position).isSelected()) {
                holder.ivAdd.setVisibility(View.GONE);
                holder.ivRemove.setVisibility(View.VISIBLE);
            }
            else {
                holder.ivAdd.setVisibility(View.VISIBLE);
                holder.ivRemove.setVisibility(View.GONE);
            }


        }

    }

    @Override
    public int getItemViewType(int position) {

         if (isType2(position)) {
            return TYPE_2;
        } else
            return TYPE_ITEM;
    }


    private boolean isType2(int position) {
//        if(MessageList.get(position).getPhone().equals("-777"))
//            return true;
//        else
            return false;
    }




    public class FooterViewHolder extends ContactAdapter.MyViewHolder implements View.OnClickListener {
        public TextView tvChar;
        private ContactRecyclerViewListener mListener;


        public FooterViewHolder(View view, ContactRecyclerViewListener listener) {
            super(view,listener);



            mListener = listener;


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                default:
                    mListener.onClick(v, getAdapterPosition(), MessageList.get(getAdapterPosition()), "tick");
                    break;
            }

        }
    }


}