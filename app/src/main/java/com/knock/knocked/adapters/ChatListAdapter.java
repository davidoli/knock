package com.knock.knocked.adapters;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daasuu.ei.Ease;
import com.daasuu.ei.EasingInterpolator;
import com.knock.knocked.R;
import com.knock.knocked.activities.ChatListActivity;
import com.knock.knocked.listeners.ChatListRecyclerViewListener;
import com.knock.knocked.listeners.ExpertRecyclerViewListener;
import com.knock.knocked.models.ChatListModel;
import com.knock.knocked.models.ExpertModel;

import java.util.ArrayList;
import java.util.List;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.MyViewHolder> {
    private List<ChatListModel> chatList;
    private Context context;
    private int total;
    private ChatListRecyclerViewListener mListener;
    private final int TYPE_ITEM = 1;
    private final int TYPE_FOOTER = 2;
    private final int NO_ITEM = 0;
    private int totalPages;
    private int lastPosition = -1;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvName;
        public TextView tvTime;
        public TextView tvLastMessage;
        public ImageView ivProfile;
        private ChatListRecyclerViewListener mListener;
        private RelativeLayout layout;


        public MyViewHolder(View view, ChatListRecyclerViewListener listener) {
            super(view);

            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
            tvLastMessage = (TextView) view.findViewById(R.id.tv_message);
            ivProfile = (ImageView) view.findViewById(R.id.iv_prof);
            layout = (RelativeLayout) view.findViewById(R.id.layout);

            mListener = listener;
            view.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                default:
                    mListener.onClick(v, getAdapterPosition(), chatList.get(getAdapterPosition()), "plus");
                    break;
            }

        }
    }
// in this adaper constructor we add the list of messages as a parameter so that
// we will passe  it when making an instance of the adapter object in our activity


    public ChatListAdapter(List<ChatListModel> MessagesList, Context context, ChatListRecyclerViewListener listener, int totalc) {

        this.chatList = MessagesList;
        this.context = context;
        this.mListener = listener;
        this.totalPages = totalc;


    }

    @Override
    public int getItemCount() {
//        return chatList.size()+1;
        return chatList.size();
    }

    @Override
    public ChatListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;
//        if (viewType == TYPE_ITEM) {
        if (viewType == TYPE_FOOTER) {

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_chat_list_knock, parent, false);
            return new ChatListAdapter.MyViewHolder(itemView, mListener);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_chat_list, parent, false);
            return new ChatListAdapter.MyViewHolder(itemView, mListener);
        }

//        else if (viewType == TYPE_FOOTER) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_loading,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        } else if (viewType == NO_ITEM) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_item,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        }


//        return new ChatListAdapter.MyViewHolder(itemView, mListener);
    }

    @Override
    public void onBindViewHolder(final ChatListAdapter.MyViewHolder holder, final int position) {

        //binding the data from our ArrayList of object to the item.xml using the viewholder


        if (holder instanceof ChatListAdapter.FooterViewHolder) {


        } else {
            holder.tvName.setText(chatList.get(position).getName());
            holder.tvTime.setText(chatList.get(position).getTime());
            holder.tvLastMessage.setText(chatList.get(position).getLastMessage());
            Glide.with(context)
                    .load(chatList.get(position).getPic())  //http://karmento.ir      http://karmento.ir
                    .placeholder(R.drawable.avatar2)
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                    .fitCenter()
                    .into(holder.ivProfile);//this method help

        }


    }

    @Override
    public int getItemViewType(int position) {


        if (knocked(position))
            return TYPE_FOOTER;
        else if (true)
            return TYPE_ITEM;
//        } else
        return TYPE_ITEM;
    }


    public class FooterViewHolder extends ChatListAdapter.MyViewHolder {
        public android.view.View View;

        public FooterViewHolder(android.view.View v) {
            super(v, mListener);
            View = v;
            // Add your UI Components here
        }

    }


    private boolean isPositionFooter(int position) {
        return position > chatList.size() - 1;
    }

    private boolean isEndOfList(int position) {


        if (chatList.size() == 0)
            return true;

        if (chatList.size() % 10 == 0) {
            if (position == totalPages * 10)
                return true;

        } else if (totalPages == 1) {
            if (position == chatList.size())
                return true;
            else
                return false;
        } else {
            if (chatList.size() % 10 != 0)
                if (position == chatList.size())
                    return true;
                else
                    return totalPages * 10 == position;
        }
        return false;
    }


    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.bonus_anim);
            viewToAnimate.startAnimation(animation);


            doBounceAnimation(viewToAnimate);

            ValueAnimator valueAnimator = new ValueAnimator();
            valueAnimator.setInterpolator(new EasingInterpolator(Ease.ELASTIC_IN));
            valueAnimator.start();

            lastPosition = position;
        }
    }

    private void doBounceAnimation(View targetView) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(targetView, "translationX", 0, 200, 0);
        animator.setInterpolator(new EasingInterpolator(Ease.BOUNCE_IN_OUT));
        animator.setStartDelay(400);
        animator.setDuration(1200);
        animator.start();
    }

    private boolean knocked(int pos) {
        if (chatList.get(pos).isKnocked())
            return true;
        return false;
    }


}