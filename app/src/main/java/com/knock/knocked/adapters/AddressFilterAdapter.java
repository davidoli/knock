package com.knock.knocked.adapters;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.daasuu.ei.Ease;
import com.daasuu.ei.EasingInterpolator;
import com.knock.knocked.R;
import com.knock.knocked.activities.AddressFilterActivity;
import com.knock.knocked.listeners.AddressFilterRecyclerViewListener;
import com.knock.knocked.listeners.ExpertRecyclerViewListener;
import com.knock.knocked.models.AddressFilterModel;
import com.knock.knocked.models.ExpertModel;

import java.util.ArrayList;
import java.util.List;

public class AddressFilterAdapter extends RecyclerView.Adapter<AddressFilterAdapter.MyViewHolder> {
    private List<AddressFilterModel> addressList;
    private Context context;
    private int total;
    private AddressFilterRecyclerViewListener mListener;
    private final int TYPE_ITEM = 1;
    private final int TYPE_FOOTER = 2;
    private final int NO_ITEM = 0;
    private int totalPages;
    private int lastPosition = -1;
    public RelativeLayout yourView;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTitle;
        public ImageView ivRemove;
        public RelativeLayout layout;
        private AddressFilterRecyclerViewListener mListener;



        public MyViewHolder(View view, AddressFilterRecyclerViewListener listener) {
            super(view);

            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            ivRemove = (ImageView) view.findViewById(R.id.iv_remove);
            layout = (RelativeLayout) view.findViewById(R.id.rl_layout);

            mListener = listener;
            layout.setOnClickListener(this);
            ivRemove.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.rl_layout:
                    mListener.onClick(v, getAdapterPosition(), addressList.get(getAdapterPosition()), "layout");
                    break;
                default:
                    mListener.onClick(v, getAdapterPosition(), addressList.get(getAdapterPosition()), "remove");
                    break;
            }

        }
    }
// in this adaper constructor we add the list of messages as a parameter so that
// we will passe  it when making an instance of the adapter object in our activity


    public AddressFilterAdapter(List<AddressFilterModel> MessagesList, Context context, AddressFilterRecyclerViewListener listener, int totalc) {

        this.addressList = MessagesList;
        this.context = context;
        this.mListener = listener;
        this.totalPages = totalc;


    }

    @Override
    public int getItemCount() {
//        return addressList.size()+1;
        return addressList.size();
    }

    @Override
    public AddressFilterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;
//        if (viewType == TYPE_ITEM) {
        if (true) {

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_filter_address_list, parent, false);
            return new AddressFilterAdapter.MyViewHolder(itemView, mListener);
        }

//        else if (viewType == TYPE_FOOTER) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_loading,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        } else if (viewType == NO_ITEM) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_item,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");

//        return new AddressFilterAdapter.MyViewHolder(itemView, mListener);
    }

    @Override
    public void onBindViewHolder(final AddressFilterAdapter.MyViewHolder holder, final int position) {

        //binding the data from our ArrayList of object to the item.xml using the viewholder


        if (holder instanceof AddressFilterAdapter.FooterViewHolder) {


        } else {

            holder.tvTitle.setText(addressList.get(position).getAddress());

            if(addressList.get(position).isClicked())
                holder.tvTitle.setTextColor(Color.parseColor("#202020"));
            else
                holder.tvTitle.setTextColor(Color.parseColor("#969AB4"));

        }

    }

    @Override
    public int getItemViewType(int position) {


//        if (isEndOfList(position))
//            return NO_ITEM;
//        else if (isPositionFooter(position)) {
//            return TYPE_FOOTER;
//        } else
        return TYPE_ITEM;
    }


    public class FooterViewHolder extends AddressFilterAdapter.MyViewHolder {
        public android.view.View View;

        public FooterViewHolder(android.view.View v) {
            super(v, mListener);
            View = v;
            // Add your UI Components here
        }

    }


    private boolean isPositionFooter(int position) {
        return position > addressList.size() - 1;
    }

    private boolean isEndOfList(int position) {


        if (addressList.size() == 0)
            return true;

        if (addressList.size() % 10 == 0) {
            if (position == totalPages * 10)
                return true;

        } else if (totalPages == 1) {
            if (position == addressList.size())
                return true;
            else
                return false;
        } else {
            if (addressList.size() % 10 != 0)
                if (position == addressList.size())
                    return true;
                else
                    return totalPages * 10 == position;
        }
        return false;
    }


    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
//            Animation animation = AnimationUtils.loadAnimation(context, R.anim.bonus_anim);
//            viewToAnimate.startAnimation(animation);


//            doBounceAnimation(viewToAnimate);

//            ValueAnimator valueAnimator = new ValueAnimator();
//            valueAnimator.setInterpolator(new EasingInterpolator(Ease.ELASTIC_IN));
//            valueAnimator.start();

            lastPosition = position;
        }
    }


    private void doBounceAnimation(View targetView) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(targetView, "translationX", 0, 200, 0);
        animator.setInterpolator(new EasingInterpolator(Ease.BOUNCE_IN_OUT));
        animator.setStartDelay(400);
        animator.setDuration(1200);
        animator.start();
    }


}