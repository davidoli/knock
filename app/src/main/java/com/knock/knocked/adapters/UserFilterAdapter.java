package com.knock.knocked.adapters;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.daasuu.ei.Ease;
import com.daasuu.ei.EasingInterpolator;
import com.knock.knocked.R;
import com.knock.knocked.activities.UserFilterActivity;
import com.knock.knocked.listeners.AddressFilterRecyclerViewListener;
import com.knock.knocked.listeners.UserFilterRecyclerViewListener;
import com.knock.knocked.models.AddressFilterModel;
import com.knock.knocked.models.UserFilterModel;

import java.util.ArrayList;
import java.util.List;

public class UserFilterAdapter extends RecyclerView.Adapter<UserFilterAdapter.MyViewHolder> {
    private List<UserFilterModel> userList;
    private Context context;
    private int total;
    private UserFilterRecyclerViewListener mListener;
    private final int TYPE_ITEM = 1;
    private final int TYPE_FOOTER = 2;
    private final int NO_ITEM = 0;
    private int totalPages;
    private int lastPosition = -1;
    public RelativeLayout yourView;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView langCheck;
        public RelativeLayout layout;
        private UserFilterRecyclerViewListener mListener;



        public MyViewHolder(View view, UserFilterRecyclerViewListener listener) {
            super(view);

            langCheck = (TextView) view.findViewById(R.id.tv_language);
            layout = (RelativeLayout) view.findViewById(R.id.rl_layout);

            mListener = listener;
            langCheck.setOnClickListener(this);
            layout.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                default:
                    mListener.onClick(v, getAdapterPosition(), userList.get(getAdapterPosition()), "plus");
                    break;
            }

        }
    }
// in this adaper constructor we add the list of messages as a parameter so that
// we will passe  it when making an instance of the adapter object in our activity


    public UserFilterAdapter(List<UserFilterModel> MessagesList, Context context, UserFilterRecyclerViewListener listener, int totalc) {

        this.userList = MessagesList;
        this.context = context;
        this.mListener = listener;
        this.totalPages = totalc;


    }

    @Override
    public int getItemCount() {
//        return userList.size()+1;
        return userList.size();
    }

    @Override
    public UserFilterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;
//        if (viewType == TYPE_ITEM) {
        if (true) {

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_filter_user_list, parent, false);
            return new UserFilterAdapter.MyViewHolder(itemView, mListener);
        }

//        else if (viewType == TYPE_FOOTER) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_loading,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        } else if (viewType == NO_ITEM) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_item,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");

//        return new UserFilterAdapter.MyViewHolder(itemView, mListener);
    }

    @Override
    public void onBindViewHolder(final UserFilterAdapter.MyViewHolder holder, final int position) {

        //binding the data from our ArrayList of object to the item.xml using the viewholder


        if (holder instanceof UserFilterAdapter.FooterViewHolder) {


        } else {
            holder.langCheck.setText(userList.get(position).getLanguage());
            Typeface tf = Typeface.createFromAsset(context.getAssets(),"fonts/montserrat.otf");
            if(userList.get(position).isClicked())
                holder.langCheck.setTypeface(tf, Typeface.BOLD);
            else
                holder.langCheck.setTypeface(tf, Typeface.NORMAL);
        }


    }

    @Override
    public int getItemViewType(int position) {


//        if (isEndOfList(position))
//            return NO_ITEM;
//        else if (isPositionFooter(position)) {
//            return TYPE_FOOTER;
//        } else
        return TYPE_ITEM;
    }


    public class FooterViewHolder extends UserFilterAdapter.MyViewHolder {
        public android.view.View View;

        public FooterViewHolder(android.view.View v) {
            super(v, mListener);
            View = v;
            // Add your UI Components here
        }

    }


    private boolean isPositionFooter(int position) {
        return position > userList.size() - 1;
    }

    private boolean isEndOfList(int position) {


        if (userList.size() == 0)
            return true;

        if (userList.size() % 10 == 0) {
            if (position == totalPages * 10)
                return true;

        } else if (totalPages == 1) {
            if (position == userList.size())
                return true;
            else
                return false;
        } else {
            if (userList.size() % 10 != 0)
                if (position == userList.size())
                    return true;
                else
                    return totalPages * 10 == position;
        }
        return false;
    }


    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.bonus_anim);
            viewToAnimate.startAnimation(animation);


            doBounceAnimation(viewToAnimate);

            ValueAnimator valueAnimator = new ValueAnimator();
            valueAnimator.setInterpolator(new EasingInterpolator(Ease.ELASTIC_IN));
            valueAnimator.start();

            lastPosition = position;
        }
    }


    private void doBounceAnimation(View targetView) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(targetView, "translationX", 0, 200, 0);
        animator.setInterpolator(new EasingInterpolator(Ease.BOUNCE_IN_OUT));
        animator.setStartDelay(400);
        animator.setDuration(1200);
        animator.start();
    }


}