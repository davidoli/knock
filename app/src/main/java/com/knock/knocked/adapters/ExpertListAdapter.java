package com.knock.knocked.adapters;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daasuu.ei.Ease;
import com.daasuu.ei.EasingInterpolator;
import com.knock.knocked.R;
import com.knock.knocked.listeners.ExpertRecyclerViewListener;
import com.knock.knocked.models.ExpertModel;

import java.util.List;

public class ExpertListAdapter extends RecyclerView.Adapter<ExpertListAdapter.MyViewHolder> {
    private List<ExpertModel> expertList;
    private Context context;
    private int total;
    private ExpertRecyclerViewListener mListener;
    private final int TYPE_ITEM = 1;
    private final int TYPE_FOOTER = 2;
    private final int NO_ITEM = 0;
    private int totalPages;
    private int lastPosition = -1;
    public RelativeLayout yourView;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvName;
        public TextView tvTime;
        public TextView tvRate;
        public TextView tvPrice;
        public TextView tvDistance;
        public ImageView ivProfile;
        public RelativeLayout layout;
        private ExpertRecyclerViewListener mListener;



        public MyViewHolder(View view, ExpertRecyclerViewListener listener) {
            super(view);

            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
            tvRate = (TextView) view.findViewById(R.id.tv_rate);
            tvPrice = (TextView) view.findViewById(R.id.tv_price);
            tvDistance = (TextView) view.findViewById(R.id.tv_distance);
            ivProfile = (ImageView) view.findViewById(R.id.iv_prof);
            layout = (RelativeLayout) view.findViewById(R.id.layout);

            mListener = listener;
            view.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                default:
                    mListener.onClick(ivProfile, getAdapterPosition(), expertList.get(getAdapterPosition()), "plus");
                    break;
            }

        }
    }
// in this adaper constructor we add the list of messages as a parameter so that
// we will passe  it when making an instance of the adapter object in our activity


    public ExpertListAdapter(List<ExpertModel> MessagesList, Context context, ExpertRecyclerViewListener listener, int totalc) {

        this.expertList = MessagesList;
        this.context = context;
        this.mListener = listener;
        this.totalPages = totalc;


    }

    @Override
    public int getItemCount() {
//        return expertList.size()+1;
        return expertList.size();
    }

    @Override
    public ExpertListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;
//        if (viewType == TYPE_ITEM) {
        if (true) {

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_expert_list, parent, false);
            return new MyViewHolder(itemView, mListener);
        }

//        else if (viewType == TYPE_FOOTER) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_loading,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        } else if (viewType == NO_ITEM) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_item,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");

//        return new ExpertListAdapter.MyViewHolder(itemView, mListener);
    }

    @Override
    public void onBindViewHolder(final ExpertListAdapter.MyViewHolder holder, final int position) {

        //binding the data from our ArrayList of object to the item.xml using the viewholder


        if (holder instanceof FooterViewHolder) {


        } else {
            holder.tvName.setText(expertList.get(position).getName());
            holder.tvTime.setText(expertList.get(position).getTime());
            holder.tvPrice.setText(expertList.get(position).getPrice() + " Customers");
            holder.tvRate.setText(expertList.get(position).getRate()+"");


            Glide.with(context)
                    .load(expertList.get(position).getPic())  //http://karmento.ir      http://karmento.ir
                    .placeholder(R.mipmap.profile)
                    .dontAnimate()
                    .error(R.mipmap.profile)//in case of any glide exception or not able to download then this image will be appear . if you won't mention this error() then nothing to worry placeHolder image would be remain as it is.
                    .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                    .fitCenter()//this method help to fit image into center of your ImageView
                    .into(holder.ivProfile);

            setAnimation(holder.layout, position);
        }

    }

    @Override
    public int getItemViewType(int position) {


//        if (isEndOfList(position))
//            return NO_ITEM;
//        else if (isPositionFooter(position)) {
//            return TYPE_FOOTER;
//        } else
        return TYPE_ITEM;
    }


    public class FooterViewHolder extends ExpertListAdapter.MyViewHolder {
        public android.view.View View;

        public FooterViewHolder(android.view.View v) {
            super(v, mListener);
            View = v;
            // Add your UI Components here
        }

    }


    private boolean isPositionFooter(int position) {
        return position > expertList.size() - 1;
    }

    private boolean isEndOfList(int position) {


        if (expertList.size() == 0)
            return true;

        if (expertList.size() % 10 == 0) {
            if (position == totalPages * 10)
                return true;

        } else if (totalPages == 1) {
            if (position == expertList.size())
                return true;
            else
                return false;
        } else {
            if (expertList.size() % 10 != 0)
                if (position == expertList.size())
                    return true;
                else
                    return totalPages * 10 == position;
        }
        return false;
    }


    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
//            Animation animation = AnimationUtils.loadAnimation(context, R.anim.bonus_anim);
//            viewToAnimate.startAnimation(animation);


//            doBounceAnimation(viewToAnimate);

//            ValueAnimator valueAnimator = new ValueAnimator();
//            valueAnimator.setInterpolator(new EasingInterpolator(Ease.ELASTIC_IN));
//            valueAnimator.start();

            lastPosition = position;
        }
    }


    private void doBounceAnimation(View targetView) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(targetView, "translationX", 0, 200, 0);
        animator.setInterpolator(new EasingInterpolator(Ease.BOUNCE_IN_OUT));
        animator.setStartDelay(400);
        animator.setDuration(1200);
        animator.start();
    }






}