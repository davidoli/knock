package com.knock.knocked.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.knock.knocked.R;
import com.knock.knocked.listeners.ProfessionsRecyclerViewListener;
import com.knock.knocked.models.ProfessionModel;

import java.util.List;

public class ProfessionListAdapter extends RecyclerView.Adapter<ProfessionListAdapter.MyViewHolder> {
    private List<ProfessionModel> professionList;
    private Context context;
    private int total;
    private ProfessionsRecyclerViewListener mListener;
    private final int TYPE_ITEM = 1;
    private final int TYPE_FOOTER = 2;
    private final int NO_ITEM = 0;
    private int totalPages;
    private LinearLayout layout;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTitle;
        public LinearLayout layout;

        private ProfessionsRecyclerViewListener mListener;


        public MyViewHolder(View view, ProfessionsRecyclerViewListener listener) {
            super(view);

            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            layout= (LinearLayout) view.findViewById(R.id.layout);
            mListener = listener;
            view.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                default:
                    mListener.onClick(v, getAdapterPosition(), professionList.get(getAdapterPosition()),"plus",layout);
                    break;
            }

        }
    }
// in this adaper constructor we add the list of messages as a parameter so that
// we will passe  it when making an instance of the adapter object in our activity


    public ProfessionListAdapter(List<ProfessionModel> MessagesList, Context context, ProfessionsRecyclerViewListener listener,int totalc) {

        this.professionList = MessagesList;
        this.context = context;
        this.mListener = listener;
        this.totalPages = totalc;


    }

    @Override
    public int getItemCount() {
//        return professionList.size()+1;
        return professionList.size();
    }

    @Override
    public ProfessionListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;
//        if (viewType == TYPE_ITEM) {
        if (true) {

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_profession_list, parent, false);
            return new ProfessionListAdapter.MyViewHolder(itemView, mListener);
        }

//        else if (viewType == TYPE_FOOTER) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_loading,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        } else if (viewType == NO_ITEM) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_item,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");

//        return new ProfessionListAdapter.MyViewHolder(itemView, mListener);
    }

    @Override
    public void onBindViewHolder(final ProfessionListAdapter.MyViewHolder holder, final int position) {

        //binding the data from our ArrayList of object to the item.xml using the viewholder


        if (holder instanceof ProfessionListAdapter.FooterViewHolder) {

        }
        else {
            holder.tvTitle.setText(professionList.get(position).getJobTitle());
            if(professionList.get(position).isClicked())
                holder.tvTitle.setTextColor(Color.parseColor("#000000"));
            else
                holder.tvTitle.setTextColor(Color.parseColor("#969AB3"));


        }

    }

    @Override
    public int getItemViewType(int position) {


//        if (isEndOfList(position))
//            return NO_ITEM;
//        else if (isPositionFooter(position)) {
//            return TYPE_FOOTER;
//        } else
        return TYPE_ITEM;
    }


    public class FooterViewHolder extends ProfessionListAdapter.MyViewHolder {
        public android.view.View View;

        public FooterViewHolder(android.view.View v) {
            super(v, mListener);
            View = v;
            // Add your UI Components here
        }

    }


    private boolean isPositionFooter(int position) {
        return position > professionList.size()-1 ;
    }

    private boolean isEndOfList(int position) {


        if (professionList.size() == 0)
            return true;

        if (professionList.size() % 10 == 0) {
            if (position == totalPages * 10)
                return true;

        } else if (totalPages == 1) {
            if (position == professionList.size())
                return true;
            else
                return false;
        } else {
            if (professionList.size() % 10 != 0)
                if (position == professionList.size())
                    return true;
                else
                    return totalPages * 10 == position;
        }
        return false;
    }





}