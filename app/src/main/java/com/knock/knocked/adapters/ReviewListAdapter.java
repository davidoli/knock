package com.knock.knocked.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.knock.knocked.R;
import com.knock.knocked.activities.ReviewActivity;
import com.knock.knocked.listeners.ChatListRecyclerViewListener;
import com.knock.knocked.listeners.ReviewListRecyclerListener;
import com.knock.knocked.models.ChatListModel;
import com.knock.knocked.models.ReviewModel;

import java.util.ArrayList;
import java.util.List;

public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.MyViewHolder> {
    private List<ReviewModel> chatList;
    private Context context;
    private int total;
    private ReviewListRecyclerListener mListener;
    private final int TYPE_ITEM = 1;
    private final int TYPE_ITEM2 = 3;
    private final int TYPE_FOOTER = 2;
    private final int NO_ITEM = 0;
    private int totalPages;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvName;
        public TextView tvTime;
        public TextView tvDes;
        private ReviewListRecyclerListener mListener;


        public MyViewHolder(View view, ReviewListRecyclerListener listener) {
            super(view);

            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvTime = (TextView) view.findViewById(R.id.tv_date);
            tvDes = (TextView) view.findViewById(R.id.tv_des);

            mListener = listener;
            view.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                default:
                    mListener.onClick(v, getAdapterPosition(), chatList.get(getAdapterPosition()),"plus");
                    break;
            }

        }
    }
// in this adaper constructor we add the list of messages as a parameter so that
// we will passe  it when making an instance of the adapter object in our activity


    public ReviewListAdapter(List<ReviewModel> MessagesList, Context context, ReviewListRecyclerListener listener, int totalc) {

        this.chatList = MessagesList;
        this.context = context;
        this.mListener = listener;
        this.totalPages = totalc;


    }

    @Override
    public int getItemCount() {
//        return chatList.size()+1;
        return chatList.size();
    }

    @Override
    public ReviewListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;
//        if (viewType == TYPE_ITEM) {
        if (viewType == TYPE_ITEM) {

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_review_list1, parent, false);
            return new ReviewListAdapter.MyViewHolder(itemView, mListener);
        }

        else if(viewType == TYPE_ITEM2)
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_review_list2, parent, false);
            return new ReviewListAdapter.MyViewHolder(itemView, mListener);
        }
//        else if (viewType == TYPE_FOOTER) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_loading,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        } else if (viewType == NO_ITEM) {
//
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_item,
//                    parent, false);
//            return new FooterViewHolder(view);
//
//        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");

//        return new ReviewListAdapter.MyViewHolder(itemView, mListener);
    }

    @Override
    public void onBindViewHolder(final ReviewListAdapter.MyViewHolder holder, final int position) {

        //binding the data from our ArrayList of object to the item.xml using the viewholder


        if (holder instanceof ReviewListAdapter.FooterViewHolder) {



        }
        else {
            holder.tvName.setText(chatList.get(position).getName());
            holder.tvDes.setText(chatList.get(position).getDes());
            holder.tvTime.setText(chatList.get(position).getDate());

            Typeface tf = Typeface.createFromAsset(context.getAssets(),"fonts/monstserratlight.otf");
            holder.tvDes.setTypeface(tf);

        }

    }

    @Override
    public int getItemViewType(int position) {


        if(position %2 == 0)
            return TYPE_ITEM;
        else
            return TYPE_ITEM2;
//        if (isEndOfList(position))
//            return NO_ITEM;
//        else if (isPositionFooter(position)) {
//            return TYPE_FOOTER;
//        } else
    }


    public class FooterViewHolder extends ReviewListAdapter.MyViewHolder {
        public android.view.View View;

        public FooterViewHolder(android.view.View v) {
            super(v, mListener);
            View = v;
            // Add your UI Components here
        }

    }


    private boolean isPositionFooter(int position) {
        return position > chatList.size()-1 ;
    }

    private boolean isEndOfList(int position) {


        if (chatList.size() == 0)
            return true;

        if (chatList.size() % 10 == 0) {
            if (position == totalPages * 10)
                return true;

        } else if (totalPages == 1) {
            if (position == chatList.size())
                return true;
            else
                return false;
        } else {
            if (chatList.size() % 10 != 0)
                if (position == chatList.size())
                    return true;
                else
                    return totalPages * 10 == position;
        }
        return false;
    }


}