package com.knock.knocked;


import com.knock.knocked.models.AvatarOut;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by seven system on 7/5/2017.
 */

public interface ApiInterface {

    @Multipart
    @PATCH("user/avatar/")
    Call<AvatarOut> postFile(@Part MultipartBody.Part file);

}
