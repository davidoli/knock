package com.knock.knocked.models;

import java.util.ArrayList;

public class ProviderInfoInData {
    private int user,service;
    private String service_kind,description,instagram,web;
    private boolean is_virtual;
    private double job_done_time;

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public double getJob_done_time() {
        return job_done_time;
    }

    public void setJob_done_time(double job_done_time) {
        this.job_done_time = job_done_time;
    }


    public int getService() {
        return service;
    }

    public void setService(int service) {
        this.service = service;
    }



    public String getService_kind() {
        return service_kind;
    }

    public void setService_kind(String service_kind) {
        this.service_kind = service_kind;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public boolean isIs_virtual() {
        return is_virtual;
    }

    public void setIs_virtual(boolean is_virtual) {
        this.is_virtual = is_virtual;
    }



}
