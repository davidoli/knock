package com.knock.knocked.models;

public class LoginOutData {

    /**
     * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyX2lkIjozMywidXNlcl9uYW1lIjoiMDkzNTQ2MjMxNTciLCJ1c2VyX2FnZW50IjoiTW96aWxsYS81LjAgKFdpbmRvd3MgTlQgMTAuMDsgV2luNjQ7IHg2NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzc5LjAuMzk0NS4xMzAgU2FmYXJpLzUzNy4zNiIsInJlbW90ZV9hZGRyIjoiMTcyLjE4LjAuMiIsInBsYXllcl9pZCI6Il9fMTcyLjE4LjAuMl9fTW96aWxsYS81LjAgKFdpbmRvd3MgTlQgMTAuMDsgV2luNjQ7IHg2NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzc5LjAuMzk0NS4xMzAgU2FmYXJpLzUzNy4zNiIsImRhdGFfam9pbmQiOiJKYW51YXJ5IDI3IDIwMjAgLSAwNzozODowNyJ9._6RBAsqu4v1I_-3k6wl9Yl3EjE0nU5_1dXNCV885MDeJSZmoX84QAfNuo3N5CEXErGEN58XAk1ZYaOLUgx1Uww
     * user : {"cellphone":"09354623157","verification_code":"","id":33,"first_name":"Mohsen","last_name":"Asgari","username":"09354623157","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg","email":null,"is_active":true,"is_superuser":false,"is_staff":null}
     */

    private String token;
    private UserBean user;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public static class UserBean {
        /**
         * cellphone : 09354623157
         * verification_code :
         * id : 33
         * first_name : Mohsen
         * last_name : Asgari
         * username : 09354623157
         * avatar : http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg
         * email : null
         * is_active : true
         * is_superuser : false
         * is_staff : null
         */

        private String cellphone;
        private String verification_code;
        private int id;
        private String first_name;
        private String last_name;
        private String username;
        private String avatar;
        private Object email;
        private boolean is_active;
        private boolean is_superuser;
        private Object is_staff;

        public String getCellphone() {
            return cellphone;
        }

        public void setCellphone(String cellphone) {
            this.cellphone = cellphone;
        }

        public String getVerification_code() {
            return verification_code;
        }

        public void setVerification_code(String verification_code) {
            this.verification_code = verification_code;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public Object getEmail() {
            return email;
        }

        public void setEmail(Object email) {
            this.email = email;
        }

        public boolean isIs_active() {
            return is_active;
        }

        public void setIs_active(boolean is_active) {
            this.is_active = is_active;
        }

        public boolean isIs_superuser() {
            return is_superuser;
        }

        public void setIs_superuser(boolean is_superuser) {
            this.is_superuser = is_superuser;
        }

        public Object getIs_staff() {
            return is_staff;
        }

        public void setIs_staff(Object is_staff) {
            this.is_staff = is_staff;
        }
    }
}
