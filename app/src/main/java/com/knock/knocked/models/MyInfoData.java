package com.knock.knocked.models;

import java.util.List;

public class MyInfoData {

    private int id;
    private String avatar;
    private String username;
    private String email;
    private String cellphone;
    private String verification_code;
    private boolean is_superuser;
    private boolean is_provider;
    private String language;
    private String first_name,last_name;
    private int invited;
    private List<FavoritesBean> favorites;
    private List<FriendsBean> friends;


    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getVerification_code() {
        return verification_code;
    }

    public void setVerification_code(String verification_code) {
        this.verification_code = verification_code;
    }

    public boolean isIs_superuser() {
        return is_superuser;
    }

    public void setIs_superuser(boolean is_superuser) {
        this.is_superuser = is_superuser;
    }

    public boolean isIs_provider() {
        return is_provider;
    }

    public void setIs_provider(boolean is_provider) {
        this.is_provider = is_provider;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getInvited() {
        return invited;
    }

    public void setInvited(int invited) {
        this.invited = invited;
    }

    public List<FavoritesBean> getFavorites() {
        return favorites;
    }

    public void setFavorites(List<FavoritesBean> favorites) {
        this.favorites = favorites;
    }

    public List<FriendsBean> getFriends() {
        return friends;
    }

    public void setFriends(List<FriendsBean> friends) {
        this.friends = friends;
    }

    public static class FavoritesBean {

        private int id;
        private String name;
        private double latitude;
        private double longitude;
        private OwnerBean owner;
        private String address;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public OwnerBean getOwner() {
            return owner;
        }

        public void setOwner(OwnerBean owner) {
            this.owner = owner;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public static class OwnerBean {

            private int id;
            private String username;
            private String password;
            private boolean is_superuser;
            private boolean is_provider;
            private String first_name;
            private String last_name;
            private String avatar;
            private String email;
            private String gender;
            private String language;
            private int invited;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public boolean isIs_superuser() {
                return is_superuser;
            }

            public void setIs_superuser(boolean is_superuser) {
                this.is_superuser = is_superuser;
            }

            public boolean isIs_provider() {
                return is_provider;
            }

            public void setIs_provider(boolean is_provider) {
                this.is_provider = is_provider;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getLanguage() {
                return language;
            }

            public void setLanguage(String language) {
                this.language = language;
            }

            public int getInvited() {
                return invited;
            }

            public void setInvited(int invited) {
                this.invited = invited;
            }
        }
    }

    public static class FriendsBean {

        private int id;
        private String cellphone;
        private OwnerBeanX owner;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCellphone() {
            return cellphone;
        }

        public void setCellphone(String cellphone) {
            this.cellphone = cellphone;
        }

        public OwnerBeanX getOwner() {
            return owner;
        }

        public void setOwner(OwnerBeanX owner) {
            this.owner = owner;
        }

        public static class OwnerBeanX {

            private int id;
            private String username;
            private String password;
            private boolean is_superuser;
            private boolean is_provider;
            private String first_name;
            private String last_name;
            private String avatar;
            private String email;
            private String gender;
            private String language;
            private int invited;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public boolean isIs_superuser() {
                return is_superuser;
            }

            public void setIs_superuser(boolean is_superuser) {
                this.is_superuser = is_superuser;
            }

            public boolean isIs_provider() {
                return is_provider;
            }

            public void setIs_provider(boolean is_provider) {
                this.is_provider = is_provider;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getLanguage() {
                return language;
            }

            public void setLanguage(String language) {
                this.language = language;
            }

            public int getInvited() {
                return invited;
            }

            public void setInvited(int invited) {
                this.invited = invited;
            }
        }
    }
}
