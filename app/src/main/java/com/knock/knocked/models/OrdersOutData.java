package com.knock.knocked.models;

import java.util.List;

public class OrdersOutData {

    /**
     * list : [{"id":1,"owner":{"id":41,"username":"09128693497","password":"pbkdf2_sha256$150000$1dLqijiNTip6$Rp6ph3+rCoevUq0RCsoDi8RQ13tDdRHZ4jdVCQ+bQP4=","is_superuser":false,"is_provider":false,"first_name":"محمدعلی","last_name":"مینوچهر","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":0},"provider":{"id":111,"user":{"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$I4a197Ox15Rm$TOEFf3Oyw3qrIe1V8AvGjwhesBXeauHeQU4h6zQ/pxM=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":5},"service":{"id":10,"title":"Fitness Trainer","icon":"http://dev.hoonamapps.com/telemarket/media/icons/no-image.jpg","parent":null,"is_publish":true},"price":50000,"discount":3000,"is_virtual":false,"is_publish":true,"locations":[{"latitude":31.254,"longitude":57.659}],"languages":[{"id":3,"title":"Armenian","direction":"ltr"}],"description":"I believe sometime you were suffering this problem when refresh a recycle-view adapter when you come back from another activity. Typically we are doing restart the activity if there have a change in second activity","cybers":[]},"price":1000,"latitude":31.57,"longitude":57.3,"request_date":"2020-02-01T20:55:19Z","accept_date":null,"reject_date":null,"owner_reject_date":null},{"id":3,"owner":{"id":6,"username":"ali4","password":"pbkdf2_sha256$150000$BHcAeN9Cd6fE$csQiYzfkikbLVx1TCU/p0u/CZE3henWoqAoY+PIzHGw=","is_superuser":false,"is_provider":false,"first_name":"","last_name":"","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":0},"provider":{"id":111,"user":{"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$I4a197Ox15Rm$TOEFf3Oyw3qrIe1V8AvGjwhesBXeauHeQU4h6zQ/pxM=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":5},"service":{"id":10,"title":"Fitness Trainer","icon":"http://dev.hoonamapps.com/telemarket/media/icons/no-image.jpg","parent":null,"is_publish":true},"price":50000,"discount":3000,"is_virtual":false,"is_publish":true,"locations":[{"latitude":31.254,"longitude":57.659}],"languages":[{"id":3,"title":"Armenian","direction":"ltr"}],"description":"I believe sometime you were suffering this problem when refresh a recycle-view adapter when you come back from another activity. Typically we are doing restart the activity if there have a change in second activity","cybers":[]},"price":10000,"latitude":31.33,"longitude":57.333,"request_date":"2020-02-01T20:56:36Z","accept_date":null,"reject_date":null,"owner_reject_date":null},{"id":5,"owner":{"id":43,"username":"09121212121","password":"pbkdf2_sha256$150000$yYg4qWBEfnEg$gH4qlN/uYos861V3hEPzrQfIboJYl2AwgHvRw+tVNWk=","is_superuser":false,"is_provider":false,"first_name":"Hohhot","last_name":"jhkhjk","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":0},"provider":{"id":111,"user":{"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$I4a197Ox15Rm$TOEFf3Oyw3qrIe1V8AvGjwhesBXeauHeQU4h6zQ/pxM=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":5},"service":{"id":10,"title":"Fitness Trainer","icon":"http://dev.hoonamapps.com/telemarket/media/icons/no-image.jpg","parent":null,"is_publish":true},"price":50000,"discount":3000,"is_virtual":false,"is_publish":true,"locations":[{"latitude":31.254,"longitude":57.659}],"languages":[{"id":3,"title":"Armenian","direction":"ltr"}],"description":"I believe sometime you were suffering this problem when refresh a recycle-view adapter when you come back from another activity. Typically we are doing restart the activity if there have a change in second activity","cybers":[]},"price":10000,"latitude":31.5,"longitude":57.111,"request_date":"2020-02-01T20:57:39Z","accept_date":null,"reject_date":null,"owner_reject_date":null}]
     * total_page_num : 1
     * total_row : 3
     */

    private int total_page_num;
    private int total_row;
    private List<ListBean> list;

    public int getTotal_page_num() {
        return total_page_num;
    }

    public void setTotal_page_num(int total_page_num) {
        this.total_page_num = total_page_num;
    }

    public int getTotal_row() {
        return total_row;
    }

    public void setTotal_row(int total_row) {
        this.total_row = total_row;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 1
         * owner : {"id":41,"username":"09128693497","password":"pbkdf2_sha256$150000$1dLqijiNTip6$Rp6ph3+rCoevUq0RCsoDi8RQ13tDdRHZ4jdVCQ+bQP4=","is_superuser":false,"is_provider":false,"first_name":"محمدعلی","last_name":"مینوچهر","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":0}
         * provider : {"id":111,"user":{"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$I4a197Ox15Rm$TOEFf3Oyw3qrIe1V8AvGjwhesBXeauHeQU4h6zQ/pxM=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":5},"service":{"id":10,"title":"Fitness Trainer","icon":"http://dev.hoonamapps.com/telemarket/media/icons/no-image.jpg","parent":null,"is_publish":true},"price":50000,"discount":3000,"is_virtual":false,"is_publish":true,"locations":[{"latitude":31.254,"longitude":57.659}],"languages":[{"id":3,"title":"Armenian","direction":"ltr"}],"description":"I believe sometime you were suffering this problem when refresh a recycle-view adapter when you come back from another activity. Typically we are doing restart the activity if there have a change in second activity","cybers":[]}
         * price : 1000
         * latitude : 31.57
         * longitude : 57.3
         * request_date : 2020-02-01T20:55:19Z
         * accept_date : null
         * reject_date : null
         * owner_reject_date : null
         */

        private int id;
        private OwnerBean owner;
        private ProviderBean provider;
        private int price;
        private double latitude;
        private double longitude;
        private String request_date;
        private Object accept_date;
        private Object reject_date;
        private Object owner_reject_date;
        private String order_date;
        private String order_time;

        public String getOrder_date() {
            return order_date;
        }

        public void setOrder_date(String order_date) {
            this.order_date = order_date;
        }

        public String getOrder_time() {
            return order_time;
        }

        public void setOrder_time(String order_time) {
            this.order_time = order_time;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public OwnerBean getOwner() {
            return owner;
        }

        public void setOwner(OwnerBean owner) {
            this.owner = owner;
        }

        public ProviderBean getProvider() {
            return provider;
        }

        public void setProvider(ProviderBean provider) {
            this.provider = provider;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public String getRequest_date() {
            return request_date;
        }

        public void setRequest_date(String request_date) {
            this.request_date = request_date;
        }

        public Object getAccept_date() {
            return accept_date;
        }

        public void setAccept_date(Object accept_date) {
            this.accept_date = accept_date;
        }

        public Object getReject_date() {
            return reject_date;
        }

        public void setReject_date(Object reject_date) {
            this.reject_date = reject_date;
        }

        public Object getOwner_reject_date() {
            return owner_reject_date;
        }

        public void setOwner_reject_date(Object owner_reject_date) {
            this.owner_reject_date = owner_reject_date;
        }

        public static class OwnerBean {
            /**
             * id : 41
             * username : 09128693497
             * password : pbkdf2_sha256$150000$1dLqijiNTip6$Rp6ph3+rCoevUq0RCsoDi8RQ13tDdRHZ4jdVCQ+bQP4=
             * is_superuser : false
             * is_provider : false
             * first_name : محمدعلی
             * last_name : مینوچهر
             * avatar : http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg
             * email : null
             * gender : m
             * language : null
             * invited : 0
             */

            private int id;
            private String username;
            private String password;
            private boolean is_superuser;
            private boolean is_provider;
            private String first_name;
            private String last_name;
            private String avatar;
            private Object email;
            private String gender;
            private Object language;
            private int invited;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public boolean isIs_superuser() {
                return is_superuser;
            }

            public void setIs_superuser(boolean is_superuser) {
                this.is_superuser = is_superuser;
            }

            public boolean isIs_provider() {
                return is_provider;
            }

            public void setIs_provider(boolean is_provider) {
                this.is_provider = is_provider;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public Object getEmail() {
                return email;
            }

            public void setEmail(Object email) {
                this.email = email;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public Object getLanguage() {
                return language;
            }

            public void setLanguage(Object language) {
                this.language = language;
            }

            public int getInvited() {
                return invited;
            }

            public void setInvited(int invited) {
                this.invited = invited;
            }
        }

        public static class ProviderBean {
            /**
             * id : 111
             * user : {"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$I4a197Ox15Rm$TOEFf3Oyw3qrIe1V8AvGjwhesBXeauHeQU4h6zQ/pxM=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":5}
             * service : {"id":10,"title":"Fitness Trainer","icon":"http://dev.hoonamapps.com/telemarket/media/icons/no-image.jpg","parent":null,"is_publish":true}
             * price : 50000
             * discount : 3000
             * is_virtual : false
             * is_publish : true
             * locations : [{"latitude":31.254,"longitude":57.659}]
             * languages : [{"id":3,"title":"Armenian","direction":"ltr"}]
             * description : I believe sometime you were suffering this problem when refresh a recycle-view adapter when you come back from another activity. Typically we are doing restart the activity if there have a change in second activity
             * cybers : []
             */

            private int id;
            private UserBean user;
            private ServiceBean service;
            private int price;
            private int discount;
            private boolean is_virtual;
            private boolean is_publish;
            private String description;
            private List<LocationsBean> locations;
            private List<LanguagesBean> languages;
            private List<?> cybers;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public UserBean getUser() {
                return user;
            }

            public void setUser(UserBean user) {
                this.user = user;
            }

            public ServiceBean getService() {
                return service;
            }

            public void setService(ServiceBean service) {
                this.service = service;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public int getDiscount() {
                return discount;
            }

            public void setDiscount(int discount) {
                this.discount = discount;
            }

            public boolean isIs_virtual() {
                return is_virtual;
            }

            public void setIs_virtual(boolean is_virtual) {
                this.is_virtual = is_virtual;
            }

            public boolean isIs_publish() {
                return is_publish;
            }

            public void setIs_publish(boolean is_publish) {
                this.is_publish = is_publish;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public List<LocationsBean> getLocations() {
                return locations;
            }

            public void setLocations(List<LocationsBean> locations) {
                this.locations = locations;
            }

            public List<LanguagesBean> getLanguages() {
                return languages;
            }

            public void setLanguages(List<LanguagesBean> languages) {
                this.languages = languages;
            }

            public List<?> getCybers() {
                return cybers;
            }

            public void setCybers(List<?> cybers) {
                this.cybers = cybers;
            }

            public static class UserBean {
                /**
                 * id : 40
                 * username : 09198048741
                 * password : pbkdf2_sha256$150000$I4a197Ox15Rm$TOEFf3Oyw3qrIe1V8AvGjwhesBXeauHeQU4h6zQ/pxM=
                 * is_superuser : false
                 * is_provider : false
                 * first_name : David
                 * last_name : Kalami
                 * avatar : http://dev.hoonamapps.com/telemarket/media/avatars/no-image.jpg
                 * email : null
                 * gender : m
                 * language : null
                 * invited : 5
                 */

                private int id;
                private String username;
                private String password;
                private boolean is_superuser;
                private boolean is_provider;
                private String first_name;
                private String last_name;
                private String avatar;
                private Object email;
                private String gender;
                private Object language;
                private int invited;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getPassword() {
                    return password;
                }

                public void setPassword(String password) {
                    this.password = password;
                }

                public boolean isIs_superuser() {
                    return is_superuser;
                }

                public void setIs_superuser(boolean is_superuser) {
                    this.is_superuser = is_superuser;
                }

                public boolean isIs_provider() {
                    return is_provider;
                }

                public void setIs_provider(boolean is_provider) {
                    this.is_provider = is_provider;
                }

                public String getFirst_name() {
                    return first_name;
                }

                public void setFirst_name(String first_name) {
                    this.first_name = first_name;
                }

                public String getLast_name() {
                    return last_name;
                }

                public void setLast_name(String last_name) {
                    this.last_name = last_name;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }

                public Object getEmail() {
                    return email;
                }

                public void setEmail(Object email) {
                    this.email = email;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public Object getLanguage() {
                    return language;
                }

                public void setLanguage(Object language) {
                    this.language = language;
                }

                public int getInvited() {
                    return invited;
                }

                public void setInvited(int invited) {
                    this.invited = invited;
                }
            }

            public static class ServiceBean {
                /**
                 * id : 10
                 * title : Fitness Trainer
                 * icon : http://dev.hoonamapps.com/telemarket/media/icons/no-image.jpg
                 * parent : null
                 * is_publish : true
                 */

                private int id;
                private String title;
                private String icon;
                private Object parent;
                private boolean is_publish;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getIcon() {
                    return icon;
                }

                public void setIcon(String icon) {
                    this.icon = icon;
                }

                public Object getParent() {
                    return parent;
                }

                public void setParent(Object parent) {
                    this.parent = parent;
                }

                public boolean isIs_publish() {
                    return is_publish;
                }

                public void setIs_publish(boolean is_publish) {
                    this.is_publish = is_publish;
                }
            }

            public static class LocationsBean {
                /**
                 * latitude : 31.254
                 * longitude : 57.659
                 */

                private double latitude;
                private double longitude;

                public double getLatitude() {
                    return latitude;
                }

                public void setLatitude(double latitude) {
                    this.latitude = latitude;
                }

                public double getLongitude() {
                    return longitude;
                }

                public void setLongitude(double longitude) {
                    this.longitude = longitude;
                }
            }

            public static class LanguagesBean {
                /**
                 * id : 3
                 * title : Armenian
                 * direction : ltr
                 */

                private int id;
                private String title;
                private String direction;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getDirection() {
                    return direction;
                }

                public void setDirection(String direction) {
                    this.direction = direction;
                }
            }
        }
    }
}
