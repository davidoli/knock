package com.knock.knocked.models;

import java.util.List;

public class AllChatsOutData {

    /**
     * list : [{"source":{"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$I4a197Ox15Rm$TOEFf3Oyw3qrIe1V8AvGjwhesBXeauHeQU4h6zQ/pxM=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":5},"destination":{"id":41,"username":"09128693497","password":"pbkdf2_sha256$150000$1dLqijiNTip6$Rp6ph3+rCoevUq0RCsoDi8RQ13tDdRHZ4jdVCQ+bQP4=","is_superuser":false,"is_provider":false,"first_name":"محمدعلی","last_name":"مینوچهر","avatar":"/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":0},"provider":null,"message":"test","file":null,"file_type":null,"file_size":0,"file_name":null,"seen":false,"sent":false,"received":false,"date":"1398/11/13","time":"02:14:07"}]
     * total_page_num : 1
     * total_row : 1
     */

    private int total_page_num;
    private int total_row;
    private List<ListBean> list;

    public int getTotal_page_num() {
        return total_page_num;
    }

    public void setTotal_page_num(int total_page_num) {
        this.total_page_num = total_page_num;
    }

    public int getTotal_row() {
        return total_row;
    }

    public void setTotal_row(int total_row) {
        this.total_row = total_row;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * source : {"id":40,"username":"09198048741","password":"pbkdf2_sha256$150000$I4a197Ox15Rm$TOEFf3Oyw3qrIe1V8AvGjwhesBXeauHeQU4h6zQ/pxM=","is_superuser":false,"is_provider":false,"first_name":"David","last_name":"Kalami","avatar":"/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":5}
         * destination : {"id":41,"username":"09128693497","password":"pbkdf2_sha256$150000$1dLqijiNTip6$Rp6ph3+rCoevUq0RCsoDi8RQ13tDdRHZ4jdVCQ+bQP4=","is_superuser":false,"is_provider":false,"first_name":"محمدعلی","last_name":"مینوچهر","avatar":"/telemarket/media/avatars/no-image.jpg","email":null,"gender":"m","language":null,"invited":0}
         * provider : null
         * message : test
         * file : null
         * file_type : null
         * file_size : 0
         * file_name : null
         * seen : false
         * sent : false
         * received : false
         * date : 1398/11/13
         * time : 02:14:07
         */

        private SourceBean source;
        private DestinationBean destination;
        private String provider;
        private String message;
        private Object file;
        private Object file_type;
        private int file_size;
        private Object file_name;
        private boolean seen;
        private boolean sent;
        private boolean received;
        private String date;
        private String time;

        public SourceBean getSource() {
            return source;
        }

        public void setSource(SourceBean source) {
            this.source = source;
        }

        public DestinationBean getDestination() {
            return destination;
        }

        public void setDestination(DestinationBean destination) {
            this.destination = destination;
        }

        public Object getProvider() {
            return provider;
        }

        public void setProvider(String provider) {
            this.provider = provider;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Object getFile() {
            return file;
        }

        public void setFile(Object file) {
            this.file = file;
        }

        public Object getFile_type() {
            return file_type;
        }

        public void setFile_type(Object file_type) {
            this.file_type = file_type;
        }

        public int getFile_size() {
            return file_size;
        }

        public void setFile_size(int file_size) {
            this.file_size = file_size;
        }

        public Object getFile_name() {
            return file_name;
        }

        public void setFile_name(Object file_name) {
            this.file_name = file_name;
        }

        public boolean isSeen() {
            return seen;
        }

        public void setSeen(boolean seen) {
            this.seen = seen;
        }

        public boolean isSent() {
            return sent;
        }

        public void setSent(boolean sent) {
            this.sent = sent;
        }

        public boolean isReceived() {
            return received;
        }

        public void setReceived(boolean received) {
            this.received = received;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public static class SourceBean {
            /**
             * id : 40
             * username : 09198048741
             * password : pbkdf2_sha256$150000$I4a197Ox15Rm$TOEFf3Oyw3qrIe1V8AvGjwhesBXeauHeQU4h6zQ/pxM=
             * is_superuser : false
             * is_provider : false
             * first_name : David
             * last_name : Kalami
             * avatar : /telemarket/media/avatars/no-image.jpg
             * email : null
             * gender : m
             * language : null
             * invited : 5
             */

            private int id;
            private String username;
            private String password;
            private boolean is_superuser;
            private boolean is_provider;
            private String first_name;
            private String last_name;
            private String avatar;
            private Object email;
            private String gender;
            private Object language;
            private int invited;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public boolean isIs_superuser() {
                return is_superuser;
            }

            public void setIs_superuser(boolean is_superuser) {
                this.is_superuser = is_superuser;
            }

            public boolean isIs_provider() {
                return is_provider;
            }

            public void setIs_provider(boolean is_provider) {
                this.is_provider = is_provider;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public Object getEmail() {
                return email;
            }

            public void setEmail(Object email) {
                this.email = email;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public Object getLanguage() {
                return language;
            }

            public void setLanguage(Object language) {
                this.language = language;
            }

            public int getInvited() {
                return invited;
            }

            public void setInvited(int invited) {
                this.invited = invited;
            }
        }

        public static class DestinationBean {
            /**
             * id : 41
             * username : 09128693497
             * password : pbkdf2_sha256$150000$1dLqijiNTip6$Rp6ph3+rCoevUq0RCsoDi8RQ13tDdRHZ4jdVCQ+bQP4=
             * is_superuser : false
             * is_provider : false
             * first_name : محمدعلی
             * last_name : مینوچهر
             * avatar : /telemarket/media/avatars/no-image.jpg
             * email : null
             * gender : m
             * language : null
             * invited : 0
             */

            private int id;
            private String username;
            private String password;
            private boolean is_superuser;
            private boolean is_provider;
            private String first_name;
            private String last_name;
            private String avatar;
            private Object email;
            private String gender;
            private Object language;
            private int invited;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public boolean isIs_superuser() {
                return is_superuser;
            }

            public void setIs_superuser(boolean is_superuser) {
                this.is_superuser = is_superuser;
            }

            public boolean isIs_provider() {
                return is_provider;
            }

            public void setIs_provider(boolean is_provider) {
                this.is_provider = is_provider;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public Object getEmail() {
                return email;
            }

            public void setEmail(Object email) {
                this.email = email;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public Object getLanguage() {
                return language;
            }

            public void setLanguage(Object language) {
                this.language = language;
            }

            public int getInvited() {
                return invited;
            }

            public void setInvited(int invited) {
                this.invited = invited;
            }
        }
    }
}
