package com.knock.knocked;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by seven system on 7/5/2017.
 */

public class ApiClient {
//    private static Context context = Application.context;
//    public static final String BASE_URL = context.getResources().getString(R.string.mystring);//"https://core.zizmart.com/";
    private static final String CONTENT_TYPE = "application/json";
    private static Retrofit retrofit = null;
    private Context context;

    private static OkHttpClient okClient(final Context context) {
        SharedPreferences settings = context.getSharedPreferences("me",
                Context.MODE_PRIVATE);
        final String iht = settings.getString("iht","0");

            return new OkHttpClient.Builder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request request = chain.request().newBuilder()
                                    .addHeader("Content-Type", CONTENT_TYPE)
                                    .addHeader("Authorization", getServerKey(context))
//                                    .addHeader("ht", "abcdefghijklmn")
//                                .addHeader("iht", iht)
//                                .addHeader("at", "P")
                                    .build();
                            return chain.proceed(request);
                        }
                    })
                    .build();

    }

    private static String getServerKey(Context context) {

        SharedPreferences settings = G.getContext().getSharedPreferences("me",
                Context.MODE_PRIVATE);
        String token = settings.getString("token", "0");
        String result="";
        if (token.equals("0"))
            result = "Bearer " + "";
        else
            result = "Bearer " + token;
        return result;


    }


    public static Retrofit getClient(Context context) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://dev.hoonamapps.com/telemarket/api/v0/")
                    .client(okClient(context))
                    .addConverterFactory(GsonConverterFactory.create(gson))//
                    .build();
        }
        return retrofit;
    }

    public void setContext(Context context) {
        this.context = context;
    }


}
